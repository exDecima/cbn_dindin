-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2016 at 03:05 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_cbn`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_background`
--

CREATE TABLE IF NOT EXISTS `t_background` (
  `kode_bg` int(6) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(100) NOT NULL,
  `warna` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_bg`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `t_background`
--

INSERT INTO `t_background` (`kode_bg`, `gambar`, `warna`) VALUES
(1, '177569274-20150806-071331.jpg', '#000000');

-- --------------------------------------------------------

--
-- Table structure for table `t_grup`
--

CREATE TABLE IF NOT EXISTS `t_grup` (
  `kode_grup` int(6) NOT NULL AUTO_INCREMENT,
  `grup` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_grup`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `t_grup`
--

INSERT INTO `t_grup` (`kode_grup`, `grup`) VALUES
(1, 'Customer'),
(2, 'Petugas1'),
(3, 'Petugas2'),
(4, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `t_kendaraan`
--

CREATE TABLE IF NOT EXISTS `t_kendaraan` (
  `kode_kendaraan` int(5) NOT NULL AUTO_INCREMENT,
  `kendaraan` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_kendaraan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `t_kendaraan`
--

INSERT INTO `t_kendaraan` (`kode_kendaraan`, `kendaraan`) VALUES
(1, 'Motor'),
(2, 'Mobil');

-- --------------------------------------------------------

--
-- Table structure for table `t_kota`
--

CREATE TABLE IF NOT EXISTS `t_kota` (
  `kode_kota` int(5) NOT NULL AUTO_INCREMENT,
  `kota` varchar(100) NOT NULL,
  `kode_negara` int(5) NOT NULL,
  PRIMARY KEY (`kode_kota`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `t_kota`
--

INSERT INTO `t_kota` (`kode_kota`, `kota`, `kode_negara`) VALUES
(1, 'Berlin', 1),
(2, 'Mexico D.F', 2),
(3, 'London', 3),
(4, 'Lulea', 4),
(5, 'Manheim', 1),
(6, 'Strasbourg', 5),
(7, 'Madrid', 11),
(8, 'Marseille', 5),
(9, 'Tsawwasen', 6),
(10, 'Buenos Aires', 7),
(11, 'Bern', 8),
(12, 'Sao Paolo', 9);

-- --------------------------------------------------------

--
-- Table structure for table `t_merek`
--

CREATE TABLE IF NOT EXISTS `t_merek` (
  `kode_merek` int(11) NOT NULL AUTO_INCREMENT,
  `merek` varchar(100) NOT NULL,
  `kode_kendaraan` int(5) NOT NULL,
  PRIMARY KEY (`kode_merek`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_merek`
--

INSERT INTO `t_merek` (`kode_merek`, `merek`, `kode_kendaraan`) VALUES
(1, 'Honda Vario Techno', 1),
(2, 'Yamaha Mio FGM F1', 1),
(3, 'Honda Jazz', 2),
(4, 'Xenia', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_negara`
--

CREATE TABLE IF NOT EXISTS `t_negara` (
  `kode_negara` int(5) NOT NULL AUTO_INCREMENT,
  `negara` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_negara`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `t_negara`
--

INSERT INTO `t_negara` (`kode_negara`, `negara`) VALUES
(1, 'Germany'),
(2, 'Mexico'),
(3, 'United Kingdom'),
(4, 'Sweden'),
(5, 'France'),
(6, 'Canada'),
(7, 'Argentina'),
(8, 'Switzerland'),
(9, 'Brazil'),
(11, 'Spain');

-- --------------------------------------------------------

--
-- Table structure for table `t_pengajuan`
--

CREATE TABLE IF NOT EXISTS `t_pengajuan` (
  `kode_pengajuan` int(5) NOT NULL AUTO_INCREMENT,
  `kd_pengajuan` varchar(100) NOT NULL,
  `id_user` int(5) NOT NULL,
  `kode_kendaraan` int(5) NOT NULL,
  `kode_merek` int(5) NOT NULL,
  `tgl_pengajuan` datetime NOT NULL,
  `petugas1` varchar(100) NOT NULL,
  `stts_petugas1` varchar(100) NOT NULL,
  `alasan_petugas1` varchar(500) NOT NULL,
  `tgl_petugas1` datetime NOT NULL,
  `petugas2` varchar(100) NOT NULL,
  `stts_petugas2` varchar(100) NOT NULL,
  `alasan_petugas2` varchar(500) NOT NULL,
  `tgl_petugas2` datetime NOT NULL,
  `manager` varchar(100) NOT NULL,
  `stts_manager` varchar(100) NOT NULL,
  `alasan_manager` varchar(500) NOT NULL,
  `tgl_manager` datetime NOT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `t_pengajuan`
--

INSERT INTO `t_pengajuan` (`kode_pengajuan`, `kd_pengajuan`, `id_user`, `kode_kendaraan`, `kode_merek`, `tgl_pengajuan`, `petugas1`, `stts_petugas1`, `alasan_petugas1`, `tgl_petugas1`, `petugas2`, `stts_petugas2`, `alasan_petugas2`, `tgl_petugas2`, `manager`, `stts_manager`, `alasan_manager`, `tgl_manager`) VALUES
(1, 'PK00000001', 13, 1, 2, '2016-02-12 08:48:03', 'Dindin Zaenudin', 'Terima', '', '2016-02-12 09:40:40', 'Yani Yuliani', 'Terima', '', '2016-02-12 10:19:28', 'Ilyasviel Von Einzbern', 'Terima', '', '2016-02-12 09:43:12'),
(2, 'PK00000002', 14, 2, 4, '2016-02-12 10:11:46', 'Dindin Zaenudin', 'Terima', '', '2016-02-12 10:16:33', 'Yani Yuliani', 'Tolak', 'tidak layak', '2016-02-12 10:25:53', '', 'Tunggu', '', '0000-00-00 00:00:00'),
(3, 'PK00000003', 14, 1, 1, '2016-02-12 10:15:28', 'Dindin Zaenudin', 'Terima', '', '2016-02-12 10:49:13', 'Yani Yuliani', 'Terima', '', '2016-02-12 10:49:28', 'Ilyasviel Von Einzbern', 'Tolak', 'sangat tidak layak', '2016-02-12 10:49:55'),
(4, 'PK00000004', 13, 2, 3, '2016-02-12 10:15:48', 'Dindin Zaenudin', 'Terima', '', '2016-02-12 10:39:03', 'Yani Yuliani', 'Terima', '', '2016-02-12 10:39:28', 'Ilyasviel Von Einzbern', 'Tolak', 'coba', '2016-02-12 14:58:52');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `id_user` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL,
  `kode_grup` int(6) NOT NULL,
  `stts` int(2) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `status` int(2) NOT NULL,
  `kode_negara` int(5) NOT NULL,
  `kode_kota` int(5) NOT NULL,
  `penghasilan` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `kode_grup` (`kode_grup`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `stts`, `foto`, `status`, `kode_negara`, `kode_kota`, `penghasilan`, `email`) VALUES
(2, 'dindin', '788ad7091c4e500855fcf4e3cb1bda29', 'Dindin Zaenudin', 'petugas1', 2, 1, '257820495-20160212-041311.jpg', 0, 0, 0, '', 'zaen.dindin@gmail.com'),
(3, 'yani', '080840925a7e2087673145d83918c658', 'Yani Yuliani', 'petugas2', 3, 1, '40737790-20160212-003636.png', 0, 0, 0, '', ''),
(4, 'ilya', '67e21d45f7b1fab0a3c43dd4339ee8e9', 'Ilyasviel Von Einzbern', 'manager', 4, 1, '149408998-20160212-003802.jpg', 0, 0, 0, '', 'zaen.dindin@gmail.com'),
(13, 'djoeli', 'cb67a72295d23f0ba0e91275979a8a4a', 'Djoeliani Ani', 'customer', 1, 1, '257597884-20160212-054846.jpg', 0, 5, 6, 'Rp. 25.678.000,-', 'djoeli@nie.com'),
(14, 'alfred', '29cb2448018800ab65a9de297548b6e0', 'Alfreds Futterkiste', 'customer', 1, 1, '', 0, 1, 1, 'US 2.500', 'alfred@numberi.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
