<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo $title; ?> | Kredit CBN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="layout" content="main"/>
    <link rel="shortcut icon" href="<?php echo base_url('asset/images/favicon.ico')?>">
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>

    <script src="<?php echo base_url(); ?>asset/js/jquery/jquery-1.8.2.min.js" type="text/javascript" ></script>
    <link href="<?php echo base_url(); ?>asset/css/customize-template.css" type="text/css" media="screen, projection" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>asset/colorpicker/bootstrap-colorpicker.min.css" type="text/css" media="screen, projection" rel="stylesheet" />

    <script>
    	$(document).ready(function(){       
            var scroll_pos = 0;
            $(document).scroll(function() { 
                scroll_pos = $(this).scrollTop();
                if(scroll_pos > 70) {
                    $(".body-nav.body-nav-horizontal").css('background-color', 'rgba(31,58,77, 0.3)');
                    $(".body-nav.body-nav-horizontal .page-header small").css('color','yellow');
                } else {
                    $(".body-nav.body-nav-horizontal").css('background-color', 'rgba(31,58,77, 1)');
                    $(".body-nav.body-nav-horizontal .page-header small").css('color','#0bb8e4');
                }
            });
        });
    </script>
    <style>
    </style>
</head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button class="btn btn-navbar" data-toggle="collapse" data-target="#app-nav-top-bar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo base_url(); ?>" class="brand">Kredit CBN</a>
                    <div id="app-nav-top-bar" class="nav-collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <button class="btn btn-primary" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i><?php echo $this->session->userdata('nama_lengkap'); ?>
                                	<b class="caret hidden-phone"></b>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo base_url(); ?>app/ubah_password">Pengaturan Akun</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>app/logout">Logout</a>
                                    </li>
                                </ul>
                            </li>                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="body-container">
            <div id="body-content">
                
                    <div class="body-nav body-nav-horizontal body-nav-fixed">
                        <div class="container">
                            <header class="page-header">
                                <h3><?php echo $judul; ?><br/>
                                    <small><?php echo $pengguna; ?></small>
                                </h3>
                            </header>
                        </div>
                    </div>
                
                
        <section class="nav nav-page">
        <div class="container">
            <div class="row">
                <div class="page-nav-options">
                    <div class="span16">
                        <ul class="nav nav-tabs">
                            <li>
                                <a href="<?php echo base_url(); ?>"><i class="icon-home"></i>Beranda</a>
                            </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown">
                                        Master Data
                                    </a>
                                    <ul class="dropdown-menu" style="background-color: #048ae4;">
                                        <li><a style="padding-bottom: 10px;" href="<?php echo base_url(); ?>negara">Negara dan Kota</a></li>
                                        <li><a style="border-radius: 0 0 4px 4px; padding-bottom: 10px;" href="<?php echo base_url(); ?>kendaraan">Kendaraan</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown">
                                        Verifikasi
                                    </a>
                                    <ul class="dropdown-menu" style="background-color: #048ae4;">
                                        <li><a style="border-radius: 0 0 4px 4px; padding-bottom: 10px;" href="<?php echo base_url(); ?>pengajuan">Pengajuan Kredit</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>customer">
                                        Kelola Customer
                                    </a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
        