<?php $this->load->view('petugas1/v_header'); ?>

    <section class="page container">
        <div class="row">
            <div class="span12">
                <div class="box pattern pattern-sandstone">
                    <div class="box-header">
                        <i class="icon-list"></i>
                        <h5>Selayang Pandang</h5>
                        <button class="btn btn-box-right" data-toggle="collapse" data-target=".box-list">
                            <i class="icon-reorder"></i>
                        </button>
                    </div>
                    <div class="box-content box-list collapse in">
                        <ul>
                            <li>
                                <div>
                                    <a class="news-item-title"> &nbsp; </a>
                                    <p class="news-item-preview" style="text-indent:50px; text-align:justify;">Dalam melaksanakan tugas pokok dan fungsinya CBN menghadapi tantangan yang tidak mudah, khususnya dalam hal melayani penempatan pengajuan kredit. 
                                    Hal yang perlu menjadi perhatian dan harus dipersiapkan untuk mengurangi tantangan dalam pelaksanaan tugas dan fungsinya tersebut perlu diterapkan sarana <a>teknologi informasi berbasis web</a> dalam melaksanakan layanan pelayanan pengajuan kredit di CBN guna kepentingan pelayanan yang efektif, efesien, dan cepat.</p>
                                    </div>
                            </li>
                            <li>
                                <div>
                                    <a class="news-item-title">Apa itu Kredit CBN?</a>
                                    <p class="news-item-preview" style="text-align:justify;">Aplikasi Kredit CBN merupakan sebuah aplikasi untuk menunjang pengelolaan pengajuan kredit.</p>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <a class="news-item-title">Fungsi Kredit CBN?</a>
                                    <p class="news-item-preview" style="text-align:justify;">Fungsi Kredit CBN antara lain untuk menunjang dan mempercepat proses pengelolaan Pengajuan Kredit, membantu penyebaran informasi Kredit, serta memfasilitasi CBN dan Customer dalam mengelola dan memperoleh informasi mengenai kredit.</p>
                                </div>
                            </li>
                        </ul>
                        <div class="box-collapse">
                            <button class="btn btn-box" data-toggle="collapse" data-target=".more-list">
                                Show / Hide
                            </button>
                        </div>
                        <ul class="more-list collapse out">
                            <li>
                                <div>
                                    <a class="news-item-title">Siapa pengguna Kredit CBN?</a>
                                    <p class="news-item-preview" style="text-align:justify;">Pengguna Kredit CBN adalah pegawai CBN serta customer yang mempunyai Nomor Induk Pegawai dan account Kredit CBN yang diberikan oleh Administrator Kredit CBN.</p>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <a class="news-item-title">Cara masuk Kredit CBN?</a>
                                    <p class="news-item-preview" style="text-align:justify;">Untuk menggunakan Kredit CBN terlebih dahulu harus memiliki NIP dan account Kredit CBN. Untuk mendapatkan account Kredit CBN, silahkan datang ke kantor CBN dengan menunjukan NIP sebagai bukti terdaftar secara sah sebagai pegawai lalu minta kepada Administrator Kredit CBN untuk mendapatkan account Kredit CBN.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="blockoff-left">
                    <legend class="lead">
                        Selamat Datang
                    </legend>
                    <p>Anda telah memasuki Sistem Kredit CBN, silahkan menggunakan menu yang ada untuk melakukan pengelolaan administrasi kredit.</p>
                </div>
            </div>
        </div>

<?php $this->load->view('app/v_footer'); ?>