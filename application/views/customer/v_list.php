<?php if($this->session->userdata('level')=="manager")
{ 
    $this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
    $this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
    $this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
    $this->load->view('customer/v_header');
}
?>

    <section class="page container">
        <div class="container">
            <?php echo $this->session->flashdata("p");?>
        </div>
        <div class="row">
            <div class="span16">
                <div class="box pattern pattern-sandstone">
                    <div class="well">
                        <div class="navbar navbar-inverse">
                            <div class="navbar-inner">
                                <div class="container">
                                    <a class="brand">Data Customers</a>
                                    <div class="nav-collapse">
                                        <ul class="nav">
                                            <li><a href="<?php echo base_url(); ?>customer/tambah" class="small-box"><i class="icon-plus-sign icon-white"></i>Tambah Data</a></li>
                                        </ul>
                                    </div>
                                    <div class="span7 pull-right">
                                        <?php echo form_open('customer/cari','class="navbar-form pull-right"'); ?>
                                        <button type="reset" class="btn" onClick="window.location='<?php echo base_url().'customer'; ?>'"><i class="icon-refresh"></i></button>
                                        <input type="text" class="span4" name="cari" placeholder="Masukkan kata kunci pencarian">
                                        <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Cari Data</button>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div><!-- /navbar-inner -->
                        </div>
                        <div class="box-content box-table">
                            <table id="sample-table" class="table table-hover table-consended">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Lengkap</th>
                                        <th>Kota</th>
                                        <th>Negara</th>
                                        <th>Penghasilan</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th class="td-actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=$tot+1;
                                        foreach($data_customer->result_array() as $dm)
                                        {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo strtoupper($dm['nama_lengkap']); ?></td>
                                        <td><?php echo $dm['kota']; ?></td>
                                        <td><?php echo $dm['negara']; ?></td>
                                        <td><?php echo $dm['penghasilan']; ?></td>
                                        <td><?php echo $dm['email']; ?></td>
                                        <?php
                                                    if($dm['stts']==0)
                                                    {
                                                        $a = "label label-important";
                                                        ?>
                                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">Tidak Aktif</span></td>
                                                        <?php
                                                    }
                                                    else if($dm['stts']==1)
                                                    {
                                                        $a = "label label-success";
                                                        ?>
                                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">Aktif</span></td>
                                                        <?php
                                                    }
                                                ?>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-small small-box" href="<?php echo base_url(); ?>customer/detail/<?php echo $dm['id_user']; ?>"><i class="icon-ok-circle"></i> Lihat Detail</a>
                                                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo base_url(); ?>customer/edit/<?php echo $dm['id_user']; ?>" class="small-box"><i class="icon-pencil"></i> Edit Data</a></li>
                                                    <li><a href="<?php echo base_url(); ?>customer/hapus/<?php echo $dm['id_user']; ?>" onClick="return confirm('Anda yakin menghapus data?');"><i class="icon-trash"></i> Hapus Data</a></li>
                                                </ul>
                                            </div><!-- /btn-group -->
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination pagination-centered">
                        <ul>
                            <?php
                            echo $paginator;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    

<?php $this->load->view('app/v_footer'); ?>