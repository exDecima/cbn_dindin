<?php if($this->session->userdata('level')=="manager")
{ 
    $this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
    $this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
    $this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
    $this->load->view('customer/v_header');
}
?>

        <section id="my-account-security-form" class="page container">
            <form class="form-horizontal">

                    <div class="row">
                        <div class="span8">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label">Nama Lengkap<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="nama_lengkap" id="nama_lengkap" value="<?php echo $nama_lengkap; ?>">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label readonly="readonly" class="control-label">Negara<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="negara" id="negara" value="<?php echo $negara; ?>">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label readonly="readonly" class="control-label">Kota<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="kota" id="kota" value="<?php echo $kota; ?>">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Penghasilan Perbulan<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="penghasilan" id="penghasilan" value="<?php echo $penghasilan; ?>">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Email<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="email" id="email" value="<?php echo $email; ?>">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Foto<span class="required"></span></label>
                                    <div class="controls">
                                        <?php $ft = $foto; if($ft==""){$ft="no-img.jpg";}?>
                                        <img src="<?php echo base_url(); ?>asset/images/user/thumb/<?php echo $ft; ?>" />
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="span7">
                            <fieldset>
                                <legend>Data Login</legend>
                            </fieldset>
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" style="width:90px;">Username<span class="required"></span></label>
                                    <div class="controls" style="margin-left:110px;">
                                        <input readonly="readonly" type="text" class="span4" name="username" id="username" value="<?php echo $username; ?>">
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <footer id="submit-actions" class="form-actions">
                        <button type="reset" class="btn" name="action" value="CANCEL" onclick="window.location.href='<?php echo base_url(); ?>customer/kembali'">Kembali</button>
                    </footer>
                </div>
            </form>

<?php $this->load->view('app/v_footer'); ?>