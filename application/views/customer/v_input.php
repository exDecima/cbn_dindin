<?php if($this->session->userdata('level')=="manager")
{ 
    $this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
    $this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
    $this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
    $this->load->view('customer/v_header');
}
?>

        <section id="my-account-security-form" class="page container">
            <?php echo form_open_multipart('customer/simpan','class="form-horizontal"'); ?>
                <div class="container">
                <?php if(validation_errors()) { ?>
                    <div class="alert alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <h4>Mohon Maaf Terjadi Kesalahan.</h4>
                    </div>
                <?php } ?>

                    <div class="row">
                        <div class="span8">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label">Nama Lengkap<span class="required"></span></label>
                                    <div class="controls">
                                        <input type="text" class="span5" name="nama_lengkap" id="nama_lengkap" value="<?php echo set_value('nama_lengkap'); ?>">
                                        <br><?php echo form_error('nama_lengkap','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Negara<span class="required"></span></label>
                                    <div class="controls">
                                        <?php
                                            $style_negara='class="chosen span5" id="kode_negara" onChange="tampil_kota()"';
                                            echo form_dropdown('kode_negara',$negara,'',$style_negara);
                                        ?>
                                        <br><?php echo form_error('kode_negara','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Kota<span class="required"></span></label>
                                    <div class="controls">
                                        <?php
                                            $style_kota='class="span5" id="kd_kota"';
                                            echo form_dropdown('kode_kota',array('Pilih Kota'=>'Pilih Kota'),'',$style_kota);
                                        ?>
                                        <br><?php echo form_error('kode_kota','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Penghasilan Perbulan<span class="required"></span></label>
                                    <div class="controls">
                                        <input type="text" class="span5" name="penghasilan" id="penghasilan" value="<?php echo set_value('penghasilan'); ?>">
                                        <br><?php echo form_error('penghasilan','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Email<span class="required"></span></label>
                                    <div class="controls">
                                        <input type="email" class="span5" name="email" id="email" value="<?php echo set_value('email'); ?>">
                                        <br><?php echo form_error('email','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Foto<span class="required"></span></label>
                                    <div class="controls">
                                        <input type="file" class="span5" name="foto" id="foto" value="<?php echo set_value('foto'); ?>">
                                        <br><?php echo form_error('foto','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="span7">
                            <fieldset>
                                <legend>Data Login</legend>
                            </fieldset>
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" style="width:90px;">Username<span class="required"></span></label>
                                    <div class="controls" style="margin-left:110px;">
                                        <input type="text" class="span4" name="username" id="username" value="<?php echo set_value('username'); ?>">
                                        <br><?php echo form_error('username','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" style="width:90px;">Password<span class="required"></span></label>
                                    <div class="controls" style="margin-left:110px;">
                                        <input type="password" class="span4" name="password" id="password" value="<?php echo set_value('password'); ?>">
                                        <br><?php echo form_error('password','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                        

                    <input type="hidden" name="id_user" value="<?php echo $id_user; ?>">

                    <footer id="submit-actions" class="form-actions">
                        <button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM">Simpan</button>
                        <button type="reset" class="btn" name="action" value="CANCEL" onclick="window.location.href='<?php echo base_url(); ?>customer/kembali'">Batal</button>
                    </footer>
                </div>
            <?php echo form_close(); ?>

<?php $this->load->view('app/v_footer'); ?>

<script type="text/javascript">
    function tampil_kota()
    {
        kdnegara = document.getElementById("kode_negara").value;
        $.ajax({
        url:"<?php echo base_url();?>customer/get_kota/"+kdnegara+"",
            success: function(response){
            $("#kd_kota").html(response);
            },
            dataType:"html"
        });
        return false;
    }
</script>