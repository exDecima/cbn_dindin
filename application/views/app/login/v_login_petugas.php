<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Login | CBN</title>
		<link href="<?php echo base_url(); ?>asset/login/normalize.css" rel="stylesheet" type="text/css">
		<script async src="<?php echo base_url(); ?>asset/login/cloudflare.min.js"></script>
		<link rel="shortcut icon" href="<?php echo base_url('asset/images/favicon.png')?>">
		<script type="text/javascript">
			try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:1425736675,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"5b40aefb76f8a130da56c705074bef4f",petok:"31d1761b0c160e82694344db2b5373bcb4ba9676-1426555000-1800",zone:"easy-development.com",rocket:"0",apps:{"abetterbrowser":{"ie":"7"}}}];CloudFlare.push({"apps":{"ape":"0b5f8e69f1d622d8a0db705748c9e1ba"}});!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=919620257c/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
		</script>
		<script src="<?php echo base_url();?>asset/login/jquery-2.1.1.js"></script>
		<script src="<?php echo base_url(); ?>assets/login/jquery-1.4.1.js"></script>
		<script src="<?php echo base_url(); ?>asset/login/jquery.js"></script>
		<link href="<?php echo base_url(); ?>asset/login/animate.css" rel="stylesheet" type="text/css">
		<script src="<?php echo base_url(); ?>asset/login/jquery-ultimate-fancy-form.min.js"></script>
		<link href="<?php echo base_url(); ?>asset/login/jquery-ultimate-fancy-form.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>asset/login/bootstrap.min.css" rel="stylesheet" type="text/css">
		<script src="<?php echo base_url(); ?>asset/login/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>asset/login/bootstrap.js"></script>
		<link href="<?php echo base_url(); ?>asset/login/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>asset/login/custom.css" rel="stylesheet" type="text/css">
		<style type="text/css">.cf-hidden { display: none; } .cf-invisible { visibility: hidden; }</style>
	</head>
	<body style="background-image: url(<?php echo base_url();?>asset/foto_barang/<?php echo $gambar;?>);">
		<div class="navbar-inner">
			<div class="header">
				<div class="brand"><img src="<?php echo base_url(); ?>asset/images/logo.png" /> Kredit CBN</div>
			</div>
		</div>
		<div class="container row">
			<div class="nav-control">
				<ul>
					<li><a href="<?php echo base_url(); ?>">CUSTOMER</a></li>
					<li><a class="active" href="<?php echo base_url(); ?>app/login_petugas">PETUGAS</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="form-container">
				<?php echo form_open ('app/login_petugas','class="form form-horizontal component-uff dispatched"','onkeypress="return event.keyCode != 13;"','role="form"'); ?>
				<div class="row">
					<div class="col-md-11">
						<div data-step="" data-step-number="0">
							<div class="form-group">
								<label class="col-sm-3 control-label sb-effect-displayed animated fadeInRight" data-sb="fadeInRight" style="opacity: 1; color:<?php echo $warna;?>">Username</label>
								<div class="col-sm-9">
									<input placeholder=" Username" data-sb="fadeInLeft" type="text" class="form-control sb-effect-displayed animated fadeInLeft" data-validation-required="#first_name_error_required" name="username" style="opacity: 1;">
									<div class="clearfix"></div>
									<?php echo form_error('username','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label sb-effect-displayed animated fadeInRight" data-sb="fadeInRight" style="opacity: 1; color:<?php echo $warna;?>">Password</label>
								<div class="col-sm-9">
									<input placeholder=" Password" data-sb="fadeInLeft" type="password" class="form-control sb-effect-displayed animated fadeInLeft" data-validation-required="#last_name_error_required" name="password" style="opacity: 1;">
									<div class="clearfix"></div>
									<?php echo form_error('password','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label sb-effect-displayed animated fadeInRight" data-sb="fadeInRight" style="opacity: 1; color:<?php echo $warna;?>">Grup</label>
								<div class="col-sm-9">
									<?php
										$style_grup='class="form-control sb-effect-displayed animated fadeInLeft" data-sb="fadeInLeft" style="opacity: 1;" id="kode_grup" onChange="tampil_instansi()"';
										echo form_dropdown('kode_grup',$grup,'',$style_grup);
									?>
									<?php if($this->session->flashdata('result_login')) { ?>
										<span class="error"><?php echo $this->session->flashdata('result_login'); ?><button type="button" class="close" data-dismiss="alert">×</button></span>
									<?php } ?>
								</div>
							</div>
							<div class="pager">
								<button type="submit" class="right sb-effect-displayed animated bounceInLeft" data-sb="bounceInLeft" style="opacity: 1;"><a class="next" data-step-next="">Login »</a></button>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
		<div class="footer">
            <p>Copyright &copy; Dindin Zaenudin<br>CBN </a>2016</p>
		</div>
	</body>
</html>
