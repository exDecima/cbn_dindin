<?php if($this->session->userdata('level')=="manager")
{ 
	$this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
	$this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
	$this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
	$this->load->view('customer/v_header');
}
?>

		<section id="my-account-security-form" class="page container">
		<?php if($this->session->flashdata('pass')) { ?>
            <?php echo $this->session->flashdata('pass'); ?>
       	<?php } ?>
            <div class="well">
                <?php if(validation_errors()) { ?>
                <div class="alert alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h4>Mohon Maaf Terjadi Kesalahan.</h4>
                </div>
                <?php } ?>
            
	            <div class="tabbable tabs-left">
				<?php
					if($this->session->userdata("tab_a")=="" && $this->session->userdata("tab_b")=="" && $this->session->userdata("tab_c")=="" && $this->session->userdata("tab_d")=="")
					{
						$set['tab_a'] = "active";
						$this->session->set_userdata($set);
					}
					$a = $this->session->userdata("tab_a");
					$b = $this->session->userdata("tab_b");
					$c = $this->session->userdata("tab_c");
					$d = $this->session->userdata("tab_d");
					?>
				  	<ul class="nav nav-tabs">
						<li class="<?php echo $a; ?>"><a href="#lA" data-toggle="tab">Pengaturan Password</a></li>
						<li class="<?php echo $b; ?>"><a href="#lB" data-toggle="tab">Pengaturan Nama Pengguna</a></li>
						<li class="<?php echo $c; ?>"><a href="#lC" data-toggle="tab">Pengaturan Foto Profil</a></li>
						<li class="<?php echo $d; ?>"><a href="#lD" data-toggle="tab">Pengaturan Email</a></li>
				  	</ul>
				  	<div class="tab-content">
						<div class="tab-pane <?php echo $a; ?>" id="lA">
							<div class="span8">
	                            <fieldset>
	                                <legend>Pengaturan Password</legend><br>
	                            </fieldset>
                        	</div>
                        	<div class="span8">
								<?php echo form_open_multipart('app/simpan_password','class="form-horizontal"'); ?>
								<div class="control-group">
									<label class="control-label">Username</label>
									<div class="controls">
									  	<input type="text" value="<?php echo $username; ?>"  class="span4" name="username" id="username" readonly="true">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="pass_lama">Password Lama</label>
									<div class="controls">
										<input type="password" class="span4" name="pass_lama" id="pass_lama">
										<br><?php echo form_error('pass_lama','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="pass_lama">Password Baru</label>
									<div class="controls">
									  	<input type="password" class="span4" name="pass_baru" id="pass_baru">
										<br><?php echo form_error('pass_baru','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="pass_lama">Ulangi Password Baru</label>
									<div class="controls">
									  	<input type="password" class="span4" name="ulangi_pass_baru" id="ulangi_pass_baru">
										<br><?php echo form_error('ulangi_pass_baru','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
									</div>
								</div>
								<footer id="submit-actions" class="form-actions">
									<div class="controls">
									  	<button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM">Simpan</button>
									</div>
						  		</footer>
								<?php echo form_close(); ?>
							</div>
						</div>
						<div class="tab-pane <?php echo $b; ?>" id="lB">
                        	<div class="span8">
	                            <fieldset>
	                                <legend>Pengaturan Nama Pengguna</legend><br>
	                            </fieldset>
                        	</div>
                        	<div class="span8">
								<?php echo form_open_multipart('app/simpan_nama','class="form-horizontal"'); ?>
								<div class="control-group">
									<label class="control-label">Username</label>
									<div class="controls">
									  	<input type="text" value="<?php echo $username; ?>"  class="span4" name="username" id="username" readonly="true">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="pass_lama">Nama Pengguna</label>
									<div class="controls">
										<input type="text" value="<?php echo $nama_lengkap; ?>" class="span4" name="nama_lengkap" id="nama_lengkap">
										<br><?php echo form_error('nama_lengkap','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
									</div>
								</div>
								<footer id="submit-actions" class="form-actions">
									<div class="controls">
									  	<button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM">Simpan</button>
									</div>
							  	</footer>
								<?php echo form_close(); ?>
							</div>
						</div>
						<div class="tab-pane <?php echo $c; ?>" id="lC">
							<div class="span8">
	                            <fieldset>
	                                <legend>Pengaturan Foto</legend><br>
	                            </fieldset>
                        	</div>
                        	<div class="span8">
								<?php echo form_open_multipart('app/simpan_foto','class="form-horizontal"'); ?>
								<div class="control-group">
									<label class="control-label">Foto</label>
									<div class="controls">
									  	<input type="file" value="<?php echo $foto; ?>"  class="span4" name="foto" id="foto">
									  	<br>
                                        <?php $ft = $foto; if($ft==""){$ft="no-img.jpg";}?>
                                        <img src="<?php echo base_url(); ?>asset/images/user/thumb/<?php echo $ft; ?>" />
									</div>
								</div>
								<input class="form-control" type="hidden" name="username" id="username" value="<?php echo $username; ?>">
								<footer id="submit-actions" class="form-actions">
									<div class="controls">
									  	<button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM">Simpan</button>
									</div>
						  		</footer>
								<?php echo form_close(); ?>
							</div>
						</div>
						<div class="tab-pane <?php echo $d; ?>" id="lD">
                        	<div class="span8">
	                            <fieldset>
	                                <legend>Pengaturan Email</legend><br>
	                            </fieldset>
                        	</div>
                        	<div class="span8">
								<?php echo form_open_multipart('app/simpan_email','class="form-horizontal"'); ?>
								<div class="control-group">
									<label class="control-label">Username</label>
									<div class="controls">
									  	<input type="text" value="<?php echo $username; ?>"  class="span4" name="username" id="username" readonly="true">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="pass_lama">Email</label>
									<div class="controls">
										<input type="email" value="<?php echo $email; ?>" class="span4" name="email" id="email">
										<br><?php echo form_error('email','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
									</div>
								</div>
								<footer id="submit-actions" class="form-actions">
									<div class="controls">
									  	<button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM">Simpan</button>
									</div>
							  	</footer>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
        </section>
}
<?php $this->load->view('app/v_footer'); ?>