<?php $this->load->view('admin/v_header'); ?>
	
		<section class="page container">
            <div class="container">
            	<?php echo $error;?>
                <?php echo $this->session->flashdata("p");?>
            </div>
            <h3>Backup Database</h3>
            <table class="table table-striped">
			<tr>
				<td><br>
					 <button type="submit" class="btn btn-primary" name="action"><a style="text-decoration:none; color:white;" href="<?php echo base_url(); ?>app/backup"><i class="icon-download"></i> Backup Database</a></button>
				</td>
			</tr>
			</table>
            <h3>Restore Database</h3>
            <?php echo form_open_multipart('app/restore');?>
			<table class="table table-striped">
			<tr>
				<td>
				Pilih File<br>
				<input type="file" class="span4" name="resfile" id="resfile" required>
				</td>
			</tr>
			<tr>
				<td><br>
					<button type="submit" name="submit" class="btn btn-primary"><i class="icon-upload"></i> Restore Database</button>
				</td>
			</tr>
			</table>
			<?php echo form_close();?>
			<h3>Ubah Background</h3>
			<?php echo form_open_multipart('app/ubah_bg');?>
			<table class="table table-striped">
			<tr>
				<td>
				Pilih File<br>
				<input type="file" class="span5" name="gambar" id="gambar" value="<?php echo $gambar; ?>">
				</td>
				<td>
				Pilih Warna
					<input type="text" name="warna" id="warna" class="form-control my-colorpicker" value="<?php echo set_value('warna'); ?>">
				</td>
			<tr>
				<td>
					<button type="submit" name="submit" class="btn btn-primary"> Set Background</button>
				</td>
			</tr>
				<td><img src="<?php echo base_url(); ?>asset/foto_barang/medium/<?php echo $gambar; ?>" /></td>
				<input type="hidden" name="kode_bg" id="kode_bg">
			</tr>
			</table>
			<?php echo form_close();?>

<?php $this->load->view('app/v_footer'); ?>

<script type="text/javascript">
    $(function () {
		$(".my-colorpicker").colorpicker();
	});
</script>