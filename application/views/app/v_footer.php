    <div id="spinner" class="spinner" style="display:none;">
            Loading&hellip;
        </div>

        <footer class="application-footer">
            <div class="container">
                <p>Sistem Kredit CBN</p>
                <p>Telp./Fax: 022-099213 <i class="icon-envelope"></i><a> kredit.cbn.com</a></p>
                <div class="disclaimer">
                    <p>Copyright &copy; 2016 Dindin Zaenudin - CBN</p>
                    <p>Waktu Eksekusi : {elapsed_time}, Penggunaan Memori : {memory_usage}</p>
                </div>
            </div>
        </footer>
        
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-transition.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-alert.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-modal.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-dropdown.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-scrollspy.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-tab.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-tooltip.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-popover.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-button.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-collapse.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-carousel.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-typeahead.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-affix.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap/bootstrap-datepicker.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/jquery/jquery-tablesorter.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/jquery/jquery-chosen.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/js/jquery/virtual-tour.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>asset/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript" ></script>
        <script type="text/javascript">
        $(function() {
            $('#sample-table').tablesorter();
            $('#datepicker').datepicker();
            $(".chosen").chosen();
        });
        </script>

	</body>
</html>