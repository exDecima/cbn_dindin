<!-- Modal -->
<div id="modal_tolak" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Keterangan Penolakan</h4>
    </div>
    <div class="modal-body">
        <?php echo form_open_multipart('pengajuan/simpan_tolak','class="form-horizontal"','id="frm"','name="frm"'); ?>
            <div class="control-group">
                <label class="control-label" style="width:100px;">Keterangan</label>
                <div class="controls" style="margin-left:120px;">
                    <textarea cols="9" rows="9" class="span5" name="alasan" id="alasan" value="<?php echo set_value('alasan'); ?>"></textarea>
                    <br><?php echo form_error('alasan','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                </div>
            </div>
            
            <input type="hidden" name="kode_pengajuan" id="kode_pengajuan" value="">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-primary" id="add" name="add">Simpan</button>
            </div>
    <?php echo form_close(); ?>
    </div>
</div>

