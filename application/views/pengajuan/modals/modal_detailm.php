<!-- Modal -->
<div id="modal_detailm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Keterangan Penolakan</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label" style="width:110px;">Verifikasi Oleh</label>
                <div class="controls" style="margin-left:130px;">
                    <input type="text" class="span5" readonly="readonly" name="manager" id="manager" value="">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="width:110px;">Tanggal Verifikasi</label>
                <div class="controls" style="margin-left:130px;">
                    <input type="text" class="span5" readonly="readonly" name="tgl_manager" id="tgl_manager" value="">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="width:110px;">Keterangan</label>
                <div class="controls" style="margin-left:130px;">
                    <textarea readonly="readonly" cols="7" rows="9" class="span5" name="alasan_manager" id="alasan_manager" value=""></textarea>
                </div>
            </div>
            
            <input type="hidden" name="kode_pengajuan" id="kode_pengajuan" value="">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </form>
    </div>
</div>

