<?php if($this->session->userdata('level')=="manager")
{ 
    $this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
    $this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
    $this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
    $this->load->view('customer/v_header');
}
?>
        <section id="my-account-security-form" class="page container">
            <?php echo form_open_multipart('pengajuan/simpan','class="form-horizontal"'); ?>
                <div class="container">
                <?php if(validation_errors()) { ?>
                    <div class="alert alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <h4>Mohon Maaf Terjadi Kesalahan.</h4>
                    </div>
                <?php } ?>

                    <div class="row">
                        <div class="span8">
                            <fieldset>
                                <div class="control-group ">
                                    <label class="control-label">Kode Pengajuan<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="kd_pengajuan" id="kd_pengajuan" value="<?php echo $kd_pengajuan; ?>">
                                        <br><?php echo form_error('kd_pengajuan','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Jenis Kendaraan<span class="required"></span></label>
                                    <div class="controls">
                                        <?php
                                            $style_kendaraan='class="chosen span5" id="kode_kendaraan" onChange="tampil_merek()"';
                                            echo form_dropdown('kode_kendaraan',$kendaraan,'',$style_kendaraan);
                                        ?>
                                        <br><?php echo form_error('kode_kendaraan','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Merek<span class="required"></span></label>
                                    <div class="controls">
                                        <?php
                                            $style_merek='class="span5" id="kd_merek"';
                                            echo form_dropdown('kode_merek',array('Pilih Merek'=>'Pilih Merek'),'',$style_merek);
                                        ?>
                                        <br><?php echo form_error('kode_merek','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    
                    <input type="hidden" name="kode_pengajuan" value="<?php echo $kode_pengajuan; ?>">

                    <footer id="submit-actions" class="form-actions">
                        <button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM">Kirim</button>
                        <button type="reset" class="btn" name="action" value="CANCEL" onclick="window.location.href='<?php echo base_url(); ?>pengajuan/back'">Batal</button>
                    </footer>
                </div>
            <?php echo form_close(); ?>

<?php $this->load->view('app/v_footer'); ?>

<script type="text/javascript">
    function tampil_merek()
    {
        kdkendaraan = document.getElementById("kode_kendaraan").value;
        $.ajax({
        url:"<?php echo base_url();?>pengajuan/get_merek/"+kdkendaraan+"",
            success: function(response){
            $("#kd_merek").html(response);
            },
            dataType:"html"
        });
        return false;
    }
</script>