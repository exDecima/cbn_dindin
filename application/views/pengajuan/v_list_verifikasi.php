<?php if($this->session->userdata('level')=="manager")
{ 
    $this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
    $this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
    $this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
    $this->load->view('customer/v_header');
}
?>

    <section class="page container">
        <div class="container">
            <?php echo $this->session->flashdata("p");?>
            <?php if(validation_errors()) { ?>
                <div class="alert alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h4>Mohon Maaf Terjadi Kesalahan.</h4>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="span16">
                <div class="box pattern pattern-sandstone">
                    <div class="well">
                        <div class="navbar navbar-inverse">
                            <div class="navbar-inner">
                                <div class="container">
                                    <a class="brand">Verifikasi Pengajuan Kredit Tahap 1</a>
                                    <div class="span7 pull-right">
                                        <?php echo form_open('pengajuan/cari','class="navbar-form pull-right"'); ?>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div><!-- /navbar-inner -->
                        </div>
                        <div class="box-content box-table">
                            <table id="sample-table" class="table table-hover table-consended">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Pengajuan</th>
                                        <th>Customer</th>
                                        <th>Tgl Pengajuan</th>
                                        <th>Merek Kendaraan</th>
                                        <th>Petugas 1</th>
                                        <th>Petugas 2</th>
                                        <th>Manager</th>
                                        <th class="td-actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=$tot+1;
                                        foreach($data_verifikasi->result_array() as $dm)
                                        {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo strtoupper($dm['kd_pengajuan']); ?></td>
                                        <td><?php echo strtoupper($dm['nama_lengkap']); ?></td>
                                        <td><?php echo date("d M Y",strtotime($dm['tgl_pengajuan'])); ?></td>
                                        <td><?php echo strtoupper($dm['merek']); ?></td>
                                        <?php
                                            if($dm['stts_petugas1']=="Tunggu")
                                            {
                                                $a = "label label-warning";
                                                $b = "#";
                                                $c = "#";
                                                $d = "#";
                                            }
                                            else if($dm['stts_petugas1']=="Terima")
                                            {
                                                $a = "label label-success";
                                                $b = "modal";
                                                $c = "success1";
                                                $d = "#modal_success1";
                                            }
                                            else if($dm['stts_petugas1']=="Tolak")
                                            {
                                                $a = "label label-important";
                                                $b = "modal";
                                                $c = "detail1";
                                                $d = "#modal_detail1";
                                            }
                                        ?>
                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">
                                        <a style="text-decoration:none; color:white;" id="<?php echo $c; ?>" data-toggle="<?php echo $b; ?>" data-kode="<?php echo $dm['kode_pengajuan']; ?>" data-ket="<?php echo $dm['alasan_petugas1']; ?>" 
                                        data-oleh="<?php echo $dm['petugas1']; ?>" data-tgl="<?php echo $dm['tgl_petugas1']; ?>" href="<?php echo $d; ?>">
                                        <?php echo $dm['stts_petugas1']; ?></a></span></td>
                                        <?php
                                            if($dm['stts_petugas2']=="Tunggu")
                                            {
                                                $a = "label label-warning";
                                                $b = "#";
                                                $c = "#";
                                                $d = "#";
                                            }
                                            else if($dm['stts_petugas2']=="Terima")
                                            {
                                                $a = "label label-success";
                                                $b = "modal";
                                                $c = "success2";
                                                $d = "#modal_success2";
                                            }
                                            else if($dm['stts_petugas2']=="Tolak")
                                            {
                                                $a = "label label-important";
                                                $b = "modal";
                                                $c = "detail2";
                                                $d = "#modal_detail2";
                                            }
                                        ?>
                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">
                                        <a style="text-decoration:none; color:white;" id="<?php echo $c; ?>" data-toggle="<?php echo $b; ?>" data-kode="<?php echo $dm['kode_pengajuan']; ?>" data-ket="<?php echo $dm['alasan_petugas2']; ?>" 
                                        data-oleh="<?php echo $dm['petugas2']; ?>" data-tgl="<?php echo $dm['tgl_petugas2']; ?>" href="<?php echo $d; ?>">
                                        <?php echo $dm['stts_petugas2']; ?></a></span></td>
                                        <?php
                                            if($dm['stts_manager']=="Tunggu")
                                            {
                                                $a = "label label-warning";
                                                $b = "#";
                                                $c = "#";
                                                $d = "#";
                                            }
                                            else if($dm['stts_manager']=="Terima")
                                            {
                                                $a = "label label-success";
                                                $b = "modal";
                                                $c = "successm";
                                                $d = "#modal_successm";
                                            }
                                            else if($dm['stts_manager']=="Tolak")
                                            {
                                                $a = "label label-important";
                                                $b = "modal";
                                                $c = "detailm";
                                                $d = "#modal_detailm";
                                            }
                                        ?>
                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">
                                        <a style="text-decoration:none; color:white;" id="<?php echo $c; ?>" data-toggle="<?php echo $b; ?>" data-kode="<?php echo $dm['kode_pengajuan']; ?>" data-ket="<?php echo $dm['alasan_manager']; ?>" 
                                        data-oleh="<?php echo $dm['manager']; ?>" data-tgl="<?php echo $dm['tgl_manager']; ?>" href="<?php echo $d; ?>">
                                        <?php echo $dm['stts_manager']; ?></a></span></td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-small small-box" href="<?php echo base_url(); ?>pengajuan/detail/<?php echo $dm['kode_pengajuan']; ?>"><i class="icon-check"></i> Detail</a>
                                                <a class="btn btn-small small-box" href="<?php echo base_url(); ?>pengajuan/simpan_setuju/<?php echo $dm['kode_pengajuan']; ?>"><i class="icon-ok-circle"></i> Terima</a>
                                                <a class="btn btn-small small-box" href="#modal_tolak" id="tolak" data-toggle="modal" data-kode="<?php echo $dm['kode_pengajuan']; ?>"><i class="icon-remove-sign"></i> Tolak</a>
                                            </div><!-- /btn-group -->
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination pagination-centered">
                        <ul>
                            <?php
                            echo $paginator;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('pengajuan/modals/modal_tolak')?>
<?php $this->load->view('pengajuan/modals/modal_detail1')?>
<?php $this->load->view('pengajuan/modals/modal_success1')?>
<?php $this->load->view('pengajuan/modals/modal_detail2')?>
<?php $this->load->view('pengajuan/modals/modal_success2')?>
<?php $this->load->view('pengajuan/modals/modal_detailm')?>
<?php $this->load->view('pengajuan/modals/modal_successm')?>
<?php $this->load->view('app/v_footer'); ?>

<script type="text/javascript">
    $(document).on("click", "#tolak", function () {
        var kode_pengajuan = $(this).data('kode');
        $(".modal-body #kode_pengajuan").val( kode_pengajuan );
        $('#modal_tolak').modal('show');
    });
    $(document).on("click", "#detail1", function () {
        var kode_pengajuan = $(this).data('kode');
        var petugas1 = $(this).data('oleh');
        var alasan_petugas1 = $(this).data('ket');
        var tgl_petugas1 = $(this).data('tgl');
        $(".modal-body #kode_pengajuan").val( kode_pengajuan );
        $(".modal-body #petugas1").val( petugas1 );
        $(".modal-body #alasan_petugas1").val( alasan_petugas1 );
        $(".modal-body #tgl_petugas1").val( tgl_petugas1 );
        $('#modal_detail1').modal('show');
    });
    $(document).on("click", "#success1", function () {
        var kode_pengajuan = $(this).data('kode');
        var petugas1 = $(this).data('oleh');
        var tgl_petugas1 = $(this).data('tgl');
        $(".modal-body #kode_pengajuan").val( kode_pengajuan );
        $(".modal-body #petugas1").val( petugas1 );
        $(".modal-body #tgl_petugas1").val( tgl_petugas1 );
        $('#modal_success1').modal('show');
    });
    $(document).on("click", "#detail2", function () {
        var kode_pengajuan = $(this).data('kode');
        var petugas2 = $(this).data('oleh');
        var alasan_petugas2 = $(this).data('ket');
        var tgl_petugas2 = $(this).data('tgl');
        $(".modal-body #kode_pengajuan").val( kode_pengajuan );
        $(".modal-body #petugas2").val( petugas2 );
        $(".modal-body #alasan_petugas2").val( alasan_petugas2 );
        $(".modal-body #tgl_petugas2").val( tgl_petugas2 );
        $('#modal_detail2').modal('show');
    });
    $(document).on("click", "#success2", function () {
        var kode_pengajuan = $(this).data('kode');
        var petugas2 = $(this).data('oleh');
        var tgl_petugas2 = $(this).data('tgl');
        $(".modal-body #kode_pengajuan").val( kode_pengajuan );
        $(".modal-body #petugas2").val( petugas2 );
        $(".modal-body #tgl_petugas2").val( tgl_petugas2 );
        $('#modal_success2').modal('show');
    });
    $(document).on("click", "#detailm", function () {
        var kode_pengajuan = $(this).data('kode');
        var manager = $(this).data('oleh');
        var alasan_manager = $(this).data('ket');
        var tgl_manager = $(this).data('tgl');
        $(".modal-body #kode_pengajuan").val( kode_pengajuan );
        $(".modal-body #manager").val( manager );
        $(".modal-body #alasan_manager").val( alasan_manager );
        $(".modal-body #tgl_manager").val( tgl_manager );
        $('#modal_detailm').modal('show');
    });
    $(document).on("click", "#successm", function () {
        var kode_pengajuan = $(this).data('kode');
        var manager = $(this).data('oleh');
        var tgl_manager = $(this).data('tgl');
        $(".modal-body #kode_pengajuan").val( kode_pengajuan );
        $(".modal-body #manager").val( manager );
        $(".modal-body #tgl_manager").val( tgl_manager );
        $('#modal_successm').modal('show');
    });
</script>