<?php if($this->session->userdata('level')=="manager")
{ 
    $this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
    $this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
    $this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
    $this->load->view('customer/v_header');
}
?>
        <section id="my-account-security-form" class="page container">
            <form class="form-horizontal">
                <div class="container">
                    <div class="row">
                        <div class="span8">
                            <fieldset>
                                <div class="control-group ">
                                    <label class="control-label">Kode Pengajuan<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="kd_pengajuan" id="kd_pengajuan" value="<?php echo $kd_pengajuan; ?>">
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label class="control-label">Nama Customer<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="nama_lengkap" id="nama_lengkap" value="<?php echo $nama_lengkap; ?>">
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label class="control-label">Kota<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="kota" id="kota" value="<?php echo $kota; ?>">
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label class="control-label">Negara<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="negara" id="negara" value="<?php echo $negara; ?>">
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label class="control-label">Email<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="email" id="email" value="<?php echo $email; ?>">
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label class="control-label">Penghasilan Perbulan<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="penghasilan" id="penghasilan" value="<?php echo $penghasilan; ?>">
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="span8">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label">Jenis Kendaraan<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="kendaraan" id="kendaraan" value="<?php echo $kendaraan; ?>">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Merek<span class="required"></span></label>
                                    <div class="controls">
                                        <input readonly="readonly" type="text" class="span5" name="merek" id="merek" value="<?php echo $merek; ?>">
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <footer id="submit-actions" class="form-actions">
                        <button type="reset" class="btn" name="action" value="CANCEL" onclick="window.location.href='<?php echo base_url(); ?>pengajuan/back'">Kembali</button>
                    </footer>
                </div>
            </form>

<?php $this->load->view('app/v_footer'); ?>