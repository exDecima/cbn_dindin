                                <div class="box-content box-table">
                                    <table class="table table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Nama Lengkap</th>
                                                <th>Level</th>
                                                <th>Instansi</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no=1;
                                        foreach($data_pengguna->result_array() as $dp)
                                        {
                                        ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo strtoupper($dp['username']); ?></td>
                                            <td><?php echo strtoupper($dp['nama_lengkap']); ?></td>
                                            <td><?php echo strtoupper($dp['level']); ?></td>
                                            <td><?php $ft = $dp['foto']; if($ft==""){$ft="no-img.jpg";}?>
                                                <img src="<?php echo base_url(); ?>asset/images/user/thumb/<?php echo $ft; ?>" />
                                            </td>
                                            <?php
                                                    if($dp['stts']==0)
                                                    {
                                                        $a = "label label-important";
                                                        ?>
                                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">Tidak Aktif</span></td>
                                                        <?php
                                                    }
                                                    else if($dp['stts']==1)
                                                    {
                                                        $a = "label label-success";
                                                        ?>
                                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">Aktif</span></td>
                                                        <?php
                                                    }
                                            ?>
                                            <td><div class="btn-group">
                                                <a class="btn-small" title="Ubah Status" style="text-decoration:none;" href="<?php echo base_url(); ?>pengguna/ubah_status/<?php echo $dp['id_user']; ?>"><i class="icon-wrench icon-large icon-white"></i></a>
                                                <a class="btn-small" title="Edit" style="text-decoration:none;"  href="<?php echo base_url(); ?>pengguna/edit/<?php echo $dp['id_user']; ?>"><i class="icon-pencil icon-large icon-white"></i></a>
                                                <a class="btn-small" title="Hapus" style="text-decoration:none;" href="<?php echo base_url(); ?>pengguna/hapus/<?php echo $dp['id_user']; ?>" onClick="return confirm('Anda yakin menghapus data?');">
                                                <i class="icon-remove-sign icon-large icon-white"></i></a>
                                            </div></td>
                                        </tr>
                                        <?php
                                            $no++;
                                        }
                                        ?>    
                                        </tbody>
                                    </table>
                                </div>