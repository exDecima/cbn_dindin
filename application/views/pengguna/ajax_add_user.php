                    <div class="row">
                        <?php if($st=='tambah') {?>
                        <div class="span11">
                            <fieldset>
                                <legend>Tambah Data Pengguna</legend>
                            </fieldset>
                        </div>
                        <?php echo form_open_multipart('pengguna/simpan','class="form-horizontal"','id="frm"','name="frm"'); ?>
                        <div class="span5">
                                <div class="control-group ">
                                    <label class="control-label" style="width:110px;">Username<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input type="text" class="span4" name="username" id="username" value="<?php echo set_value('username'); ?>">
                                        <br><?php echo form_error('username','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                    <label class="control-label" style="width:110px;">Password<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input type="password" class="span4" name="password" id="password" value="<?php echo set_value('password'); ?>">
                                        <br><?php echo form_error('password','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                    <label class="control-label" style="width:110px;">Nama Lengkap<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input type="text" class="span4" name="nama_lengkap" id="nama_lengkap" value="<?php echo set_value('nama_lengkap'); ?>">
                                        <br><?php echo form_error('nama_lengkap','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                        </div>
                        <div class="span5">
                                <div class="control-group ">
                                    <label class="control-label" style="width:110px;">Email<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input type="email" class="span4" name="email" id="email" value="<?php echo set_value('email'); ?>">
                                        <br><?php echo form_error('email','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                    <label class="control-label" style="width:110px;">Grup<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <select class="chosen span4" name="kode_grup" id="kode_grup" data-placeholder="Pilih Grup Pengguna">
                                                <?php
                                                    foreach($mst_grup->result_array() as $db)
                                                    {
                                                ?>
                                                        <option value="<?php echo $db['kode_grup']; ?>"><?php echo $db['grup']; ?></option>
                                                <?php
                                                    }
                                                ?>
                                        </select>
                                        <br><?php echo form_error('kode_grup','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                    <label class="control-label" style="width:110px;">Foto<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input type="file" class="span4" name="foto" id="foto">
                                    </div>
                                </div>
                        </div>
                        <input type="hidden" id="id_user" name="id_user" value="<?php echo $id_user; ?>">

                        <footer id="submit-actions" class="form-actions pull-right">
                            <button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM">Simpan</button>
                            <button type="reset" class="btn" name="action" value="CANCEL"><a style="text-decoration:none;" href="<?php echo base_url(); ?>pengguna/kembali">Batal</a></button>
                        </footer>
                        <?php echo form_close(); ?>
                        <?php } 
                        else if($st=='edit') {?>
                        <div class="span11">
                            <fieldset>
                                <legend>Edit Data Pengguna</legend>
                            </fieldset>
                        </div>
                        <?php echo form_open_multipart('pengguna/simpan_edit','class="form-horizontal"','id="frm"','name="frm"'); ?>
                        <div class="span5">
                                <div class="control-group ">
                                    <label class="control-label" style="width:110px;">Username<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input readonly="readonly"  type="text" class="span4" name="username" id="username" value="<?php echo $username; ?>">
                                        <br><?php echo form_error('username','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                    <label class="control-label" style="width:110px;">Password<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input required="required" type="password" class="span4" name="password" id="password" value="<?php echo set_value('password'); ?>">
                                        <br><?php echo form_error('password','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                    <label class="control-label" style="width:110px;">Nama Lengkap<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input type="text" class="span4" name="nama_lengkap" id="nama_lengkap" value="<?php echo $nama_lengkap; ?>">
                                        <br><?php echo form_error('nama_lengkap','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                    <label class="control-label" style="width:110px;">Email<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input type="email" class="span4" name="email" id="email" value="<?php echo $email; ?>">
                                        <br><?php echo form_error('email','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                </div>
                        </div>
                        <div class="span5">
                                <div class="control-group ">
                                    <label class="control-label" style="width:110px;">Grup<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <select class="chosen span4" name="kode_grup" id="kode_grup" data-placeholder="Pilih Grup Pengguna">
                                                <?php
                                                    foreach($mst_grup->result_array() as $db)
                                                    {
                                                        if($db['kode_grup']==$kode_grup)
                                                        {
                                                ?>
                                                        <option value="<?php echo $db['kode_grup']; ?>" selected="selected"><?php echo $db['grup']; ?></option>
                                                <?php
                                                        }else{
                                                ?>
                                                        <option value="<?php echo $db['kode_grup']; ?>"><?php echo $db['grup']; ?></option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                        </select>
                                        <br><?php echo form_error('level','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                                    </div>
                                    <label class="control-label" style="width:110px;">Foto<span class="required"></span></label>
                                    <div class="controls" style="margin-left:130px;">
                                        <input type="file" class="span4" name="foto" id="foto" value="<?php echo $foto; ?>">
                                        <br>
                                        <?php $ft = $foto; if($ft==""){$ft="no-img.jpg";}?>
                                        <img src="<?php echo base_url(); ?>asset/images/user/thumb/<?php echo $ft; ?>" />
                                    </div>
                                </div>
                        </div>
                        <input type="hidden" id="id_user" name="id_user" value="<?php echo $id_user; ?>">

                        <footer id="submit-actions" class="form-actions pull-right">
                            <button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM">Simpan</button>
                            <button type="reset" class="btn" name="action" value="CANCEL"><a style="text-decoration:none;" href="<?php echo base_url(); ?>pengguna/kembali">Batal</a></button>
                        </footer>
                        <?php echo form_close(); ?>
                        <?php }?>
                    </div>