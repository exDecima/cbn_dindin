<?php $this->load->view('manager/v_header'); ?>

        <section class="page container">
            <div class="container">
                <?php echo $this->session->flashdata("p");?>
                <?php if(validation_errors()) { ?>
                    <div class="alert alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <h4>Mohon Maaf Terjadi Kesalahan.</h4>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="span5">
                    <div class="blockoff-right">
                        <ul id="person-list" class="nav nav-list">
                            <li class="nav-header">Grup</li>
                            <li class="active" id="view-all" value="0">
                                <a>
                                    <i class="icon-chevron-right pull-right"></i>
                                    <b>SEMUA</b>
                                </a>
                            </li>
                            <li value="1" name="kode_grup1" id="kode_grup1">
                                <a>
                                    <i class="icon-chevron-right pull-right"></i>
                                    CUSTOMER
                                </a>
                            </li>
                            <li value="2" name="kode_grup2" id="kode_grup2">
                                <a>
                                    <i class="icon-chevron-right pull-right"></i>
                                    PETUGAS 1
                                </a>
                            </li>
                            <li value="3" name="kode_grup3" id="kode_grup3">
                                <a>
                                    <i class="icon-chevron-right pull-right"></i>
                                    PETUGAS 2
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
               
                    <div class="span11">
                        <div class="box">
                            <div class="box-header">
                                <h5>Data Pengguna</h5>
                                <ul class="pull-right" style="list-style-type:none;">
                                    <li id="add_user" value="0"><a class="small-box" style="text-decoration:none;"><i class="icon-plus-sign icon-white"></i>Tambah Pengguna</a></li>
                                </ul>
                            </div>
                            <div id="all">
                                <div class="box-content box-table">
                                    <table class="table table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Nama Lengkap</th>
                                                <th>Level</th>
                                                <th>Foto</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no=1;
                                        foreach($data_pengguna->result_array() as $dp)
                                        {
                                        ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo strtoupper($dp['username']); ?></td>
                                                <td><?php echo strtoupper($dp['nama_lengkap']); ?></td>
                                                <td><?php echo strtoupper($dp['level']); ?></td>
                                                <td><?php $ft = $dp['foto']; if($ft==""){$ft="no-img.jpg";}?>
                                                    <img src="<?php echo base_url(); ?>asset/images/user/thumb/<?php echo $ft; ?>" />
                                                </td>
                                                <?php
                                                    if($dp['stts']==0)
                                                    {
                                                        $a = "label label-important";
                                                        ?>
                                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">Tidak Aktif</span></td>
                                                        <?php
                                                    }
                                                    else if($dp['stts']==1)
                                                    {
                                                        $a = "label label-success";
                                                        ?>
                                                        <td><span style="font-size:10px;" class="<?php echo $a; ?>">Aktif</span></td>
                                                        <?php
                                                    }
                                                ?>
                                                <td><div class="btn-group">
                                                    <a class="btn-small" title="Ubah Status" style="text-decoration:none;" href="<?php echo base_url(); ?>pengguna/ubah_status/<?php echo $dp['id_user']; ?>"><i class="icon-wrench icon-large icon-white"></i></a>
                                                    <a class="btn-small" title="Edit" style="text-decoration:none;" id="edit_user" data-id="<?php echo $dp['id_user']; ?>"><i class="icon-pencil icon-large icon-white"></i></a>
                                                    <a class="btn-small" title="Hapus" style="text-decoration:none;" href="<?php echo base_url(); ?>pengguna/hapus/<?php echo $dp['id_user']; ?>" onClick="return confirm('Anda yakin menghapus data?');">
                                                    <i class="icon-remove-sign icon-large icon-white"></i></a>
                                                </div></td>
                                            </tr>
                                        <?php
                                            $no++;
                                        }
                                        ?>    
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="detail_user" name="detail_user"></div>
                        </div>
                        <div id="detail_add"></div>
                    </div>
                
                
            </div>
        </section>

<?php $this->load->view('app/v_footer'); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).on("click", "#kode_grup1", function (){
            $("#all").remove();
            var kode_grup = $("#kode_grup1").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('pengguna/get_detail_user'); ?>",
                data: "kode_grup="+kode_grup,
                cache:false,
                success: function(msg){
                    $('#detail_user').html(msg);
                }
            });
        });

        $(document).on("click", "#kode_grup2", function (){
            $("#all").remove();
            var kode_grup = $("#kode_grup2").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('pengguna/get_detail_user'); ?>",
                data: "kode_grup="+kode_grup,
                cache:false,
                success: function(msg){
                    $('#detail_user').html(msg);
                }
            });
        });

        $(document).on("click", "#kode_grup3", function (){
            $("#all").remove();
            var kode_grup = $("#kode_grup3").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('pengguna/get_detail_user'); ?>",
                data: "kode_grup="+kode_grup,
                cache:false,
                success: function(msg){
                    $('#detail_user').html(msg);
                }
            });
        });

        $("#view-all").click(function(e){
            e.preventDefault();
            $("#all").remove();
            var kode_grup = $("#view-all").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('pengguna/get_all_user'); ?>",
                data: "kode_grup="+kode_grup,
                cache:false,
                success: function(msg){
                    $('#detail_user').html(msg);
                }
            });
        });

        $("#add_user").click(function(){
            var add_user = $("#add_user").val();
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('pengguna/get_add_user'); ?>",
                data: "add_user="+add_user,
                cache:false,
                success: function(msg){
                    $('#detail_add').html(msg);
                }
            });
        });

        $(document).on("click", "#edit_user", function () {
            var edit_user = $(this).data('id');
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('pengguna/get_edit_user'); ?>",
                data: "edit_user="+edit_user,
                cache:false,
                success: function(msg){
                    $('#detail_add').html(msg);
                }
            });
        });

        $('#person-list.nav > li > a').click(function(e){
                $('#person-list > li[class="active"]').removeClass('active');
                $(this).parent().addClass('active');
                e.preventDefault();
            });

            $(function(){
                $('table').tablesorter();
                $("[rel=tooltip]").tooltip();
            });
    })
</script>