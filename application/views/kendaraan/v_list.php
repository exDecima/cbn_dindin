<?php if($this->session->userdata('level')=="manager")
{ 
    $this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
    $this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
    $this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
    $this->load->view('customer/v_header');
}
?>

    <section class="page container">
        <div class="container">
            <?php echo $this->session->flashdata("p");?>
        </div>
        <div class="container">
            <?php if(validation_errors()) { ?>
                <div class="alert alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h4>Mohon Maaf Terjadi Kesalahan.</h4>
                </div>
            <?php } ?>
        <div class="row">
            <div class="span6">
                <div class="box pattern pattern-sandstone">
                    <div class="well">
                        <div class="navbar navbar-inverse">
                            <div class="navbar-inner">
                                <div class="container">
                                    <a class="brand">Data Kendaraan</a>
                                    <div class="nav-collapse">
                                        <ul class="nav">
                                            <li><a href="#tambah_kendaraan" class="small-box" style="text-decoration:none;" data-toggle="modal"><i class="icon-plus-sign icon-white"></i>Tambah Data</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- /navbar-inner -->
                        </div>
                        <div class="box-content box-table">
                            <table id="sample-table" class="table table-hover table-consended">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kendaraan</th>
                                        <th class="td-actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1;
                                        foreach($data_kendaraan->result_array() as $dp)
                                        {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo strtoupper($dp['kendaraan']); ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn detail-kendaraan" href="#detail_kendaraan" data-toggle="modal" data-kode="<?php echo $dp['kode_kendaraan']; ?>" 
                                                    data-nama="<?php echo $dp['kendaraan']; ?>"><i class="icon-ok-circle"></i> Lihat Detail</a>
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a class="edit-kendaraan" href="#edit_kendaraan" data-toggle="modal" data-kode="<?php echo $dp['kode_kendaraan']; ?>" 
                                                    data-nama="<?php echo $dp['kendaraan']; ?>"><i class="icon-pencil"></i> Edit Data</a></li>
                                                    <li><a href="<?php echo base_url(); ?>kendaraan/hapus/<?php echo $dp['kode_kendaraan']; ?>" onClick="return confirm('Anda yakin menghapus data?');"><i class="icon-trash"></i> Hapus Data</a></li>
                                                </ul>
                                            </div><!-- /btn-group -->
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span10">
                <div class="box pattern pattern-sandstone">
                    <div class="well">
                        <div class="navbar navbar-inverse">
                            <div class="navbar-inner">
                                <div class="container">
                                    <a class="brand">Data Merek</a>
                                    <div class="nav-collapse">
                                        <ul class="nav">
                                            <li><a href="#tambah_merek" class="small-box" style="text-decoration:none;" data-toggle="modal"><i class="icon-plus-sign icon-white"></i>Tambah Data</a></li>
                                        </ul>
                                    </div>
                                    <div class="span5 pull-right">
                                        <?php echo form_open('kendaraan/cari','class="navbar-form pull-right"'); ?>
                                        <button type="reset" class="btn" onClick="window.location='<?php echo base_url().'kendaraan'; ?>'"><i class="icon-refresh"></i></button>
                                        <select class="chosen span2" name="kode_kendaraan" id="kode_kendaraan" data-placeholder="Pilih kendaraan">
                                            <option value=""></option>
                                                <?php
                                                    foreach($mst_kendaraan->result_array() as $db)
                                                    {
                                                    ?>
                                                        <option value="<?php echo $db['kode_kendaraan']; ?>"><?php echo $db['kendaraan']; ?></option>
                                                    <?php
                                                    }
                                                ?>
                                        </select>
                                        <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Cari Data</button>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div><!-- /navbar-inner -->
                        </div>
                        <div class="box-content box-table">
                            <table id="sample-table" class="table table-hover table-consended">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Merek</th>
                                        <th>Kendaraan</th>
                                        <th class="td-actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=$tot+1;
                                        foreach($data_merek->result_array() as $di)
                                        {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo strtoupper($di['merek']); ?></td>
                                        <td><?php echo strtoupper($di['kendaraan']); ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn detail-merek" href="#detail_merek" data-toggle="modal" data-kode="<?php echo $di['kode_merek']; ?>" 
                                                    data-nama="<?php echo $di['merek']; ?>" data-kendaraan="<?php echo $di['kendaraan']; ?>"><i class="icon-ok-circle"></i> Lihat Detail</a>
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a class="edit-merek" href="#edit_merek" data-toggle="modal" data-kode="<?php echo $di['kode_merek']; ?>" 
                                                    data-nama="<?php echo $di['merek']; ?>" data-kendaraan="<?php echo $di['kendaraan']; ?>"><i class="icon-pencil"></i> Edit Data</a></li>
                                                    <li><a href="<?php echo base_url(); ?>kendaraan/hapus_merek/<?php echo $di['kode_merek']; ?>" onClick="return confirm('Anda yakin menghapus data?');"><i class="icon-trash"></i> Hapus Data</a></li>
                                                </ul>
                                            </div><!-- /btn-group -->
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination pagination-centered">
                        <ul>
                            <?php
                            echo $paginator;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('kendaraan/modals/tambah_kendaraan'); ?>

<?php $this->load->view('kendaraan/modals/edit_kendaraan'); ?>

<?php $this->load->view('kendaraan/modals/detail_kendaraan'); ?>

<?php $this->load->view('kendaraan/modals/tambah_merek'); ?>

<?php $this->load->view('kendaraan/modals/edit_merek'); ?>

<?php $this->load->view('kendaraan/modals/detail_merek'); ?>

<?php $this->load->view('app/v_footer'); ?>

<script type="text/javascript">
    $(document).on("click", ".detail-kendaraan", function() {
        var kode_kendaraan = $(this).data('kode');
        var kendaraan = $(this).data('nama');
        $(".modal-body #kode_kendaraan").val( kode_kendaraan );
        $(".modal-body #kendaraan").val( kendaraan );
        $('#detail_kendaraan').modal('show');
    });

    $(document).on("click", ".edit-kendaraan", function() {
        var kode_kendaraan = $(this).data('kode');
        var kendaraan = $(this).data('nama');
        $(".modal-body #kode_kendaraan").val( kode_kendaraan );
        $(".modal-body #kendaraan").val( kendaraan );
        $('#edit_kendaraan').modal('show');
    });

    $(document).on("click", ".detail-merek", function() {
        var kode_merek = $(this).data('kode');
        var merek = $(this).data('nama');
        var kendaraan = $(this).data('kendaraan');
        $(".modal-body #kode_merek").val( kode_merek );
        $(".modal-body #merek").val( merek );
        $(".modal-body #kendaraan").val( kendaraan );
        $('#detail_merek').modal('show');
    });

    $(document).on("click", ".edit-merek", function() {
        var kode_merek = $(this).data('kode');
        var merek = $(this).data('nama');
        var kendaraan = $(this).data('kendaraan');
        $(".modal-body #kode_merek").val( kode_merek );
        $(".modal-body #merek").val( merek );
        $(".modal-body #kendaraan").val( kendaraan );
        $('#edit_merek').modal('show');
    });
</script>