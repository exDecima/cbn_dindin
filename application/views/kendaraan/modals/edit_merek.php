<!-- Modal -->
<div id="edit_merek" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Edit Data Merek</h4>
    </div>
    <div class="modal-body">
        <?php echo form_open_multipart('kendaraan/simpan_edit_merek','class="form-horizontal"','id="frm"','name="frm"'); ?>
            <div class="control-group">
                <label class="control-label">Merek</label>
                <div class="controls">
                    <input type="text" class="span4" name="merek" id="merek">
                    <br><?php echo form_error('merek','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Kendaraan</label>
                <div class="controls">
                    <select class="span4" name="kode_kendaraan" id="kode_kendaraan" data-placeholder="Pilih kendaraan">
                        <?php
                        foreach($mst_kendaraan->result_array() as $db)
                        {
                            if($db['kode_kendaraan']==$kode_kendaraan)
                            {
                            ?>
                                <option value="<?php echo $db['kode_kendaraan']; ?>" selected="selected"><?php echo $db['kendaraan']; ?></option>
                            <?php
                            }
                            else
                            {
                            ?>
                                <option value="<?php echo $db['kode_kendaraan']; ?>"><?php echo $db['kendaraan']; ?></option>
                            <?php
                            }   
                        }
                        ?>
                    </select>
                </div>
            </div>
            <input type="hidden" name="kode_merek" id="kode_merek">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-primary" id="add" name="add">Simpan</button>
            </div>

        <?php echo form_close(); ?>
    </div>
</div>

