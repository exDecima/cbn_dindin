<!-- Modal -->
<div id="tambah_kendaraan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Tambah Kendaraan</h4>
    </div>
    <div class="modal-body">
        <?php echo form_open_multipart('kendaraan/simpan','class="form-horizontal"','id="frm"','name="frm"'); ?>
            <div class="control-group">
                <label class="control-label">Kendaraan</label>
                <div class="controls">
                    <input type="text" class="span4" name="kendaraan" id="kendaraan" value="<?php echo set_value('kendaraan'); ?>">
                    <br><?php echo form_error('kendaraan','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                </div>
            </div>
            <input type="hidden" name="kode_kendaraan" value="<?php echo $kode_kendaraan; ?>">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-primary" id="add" name="add">Simpan</button>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

