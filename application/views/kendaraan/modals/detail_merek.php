<!-- Modal -->
<div id="detail_merek" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Detail Data Merek</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Merek</label>
                <div class="controls">
                    <input type="text" readonly="readonly" class="span4" name="merek" id="merek" value="<?php echo $merek; ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Kendaraan</label>
                <div class="controls">
                    <input type="text" readonly="readonly" class="span4" name="kendaraan" id="kendaraan" value="<?php echo $kendaraan; ?>">
                </div>
            </div>
            <input type="hidden" name="kode_merek" value="<?php echo $kode_merek; ?>">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </form>
    </div>
</div>

