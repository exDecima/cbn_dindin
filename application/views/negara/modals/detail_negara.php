<!-- Modal -->
<div id="detail_negara" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Detail Data Negara</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Negara</label>
                <div class="controls">
                    <input type="text" readonly="readonly" class="span4" name="negara" id="negara" value="<?php echo $negara; ?>">
                </div>
            </div>
            <input type="hidden" name="kode_negara" value="<?php echo $kode_negara; ?>">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </form>
    </div>
</div>

