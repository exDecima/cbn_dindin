<!-- Modal -->
<div id="tambah_kota" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Tambah Kota</h4>
    </div>
    <div class="modal-body">
        <?php echo form_open_multipart('negara/simpan_kota','class="form-horizontal"','id="frm"','name="frm"'); ?>
            <div class="control-group">
                <label class="control-label">Kota</label>
                <div class="controls">
                    <input type="text" class="span4" name="kota" id="kota" value="<?php echo set_value('kota'); ?>">
                    <br><?php echo form_error('kota','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Negara</label>
                <div class="controls">
                    <select class="span4" name="kode_negara" id="kode_negara" data-placeholder="Pilih negara">
                        <option value=""></option>
                        <?php
                        foreach($mst_negara->result_array() as $db)
                        {
                            ?>
                                <option value="<?php echo $db['kode_negara']; ?>"><?php echo $db['negara']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <br><?php echo form_error('kode_negara','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                </div>
            </div>
            <input type="hidden" name="kode_kota" value="<?php echo $kode_kota; ?>">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-primary" id="add" name="add">Simpan</button>
            </div>
    <?php echo form_close(); ?>
    </div>
</div>

