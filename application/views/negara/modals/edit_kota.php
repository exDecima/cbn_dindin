<!-- Modal -->
<div id="edit_kota" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Edit Data Kota</h4>
    </div>
    <div class="modal-body">
        <?php echo form_open_multipart('negara/simpan_edit_kota','class="form-horizontal"','id="frm"','name="frm"'); ?>
            <div class="control-group">
                <label class="control-label">Kota</label>
                <div class="controls">
                    <input type="text" class="span4" name="kota" id="kota">
                    <br><?php echo form_error('kota','<span class="error"><button type="button" class="close" data-dismiss="alert">×</button>', '</span>'); ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Negara</label>
                <div class="controls">
                    <select class="span4" name="kode_negara" id="kode_negara" data-placeholder="Pilih negara">
                        <?php
                        foreach($mst_negara->result_array() as $db)
                        {
                            if($db['kode_negara']==$kode_negara)
                            {
                            ?>
                                <option value="<?php echo $db['kode_negara']; ?>" selected="selected"><?php echo $db['negara']; ?></option>
                            <?php
                            }
                            else
                            {
                            ?>
                                <option value="<?php echo $db['kode_negara']; ?>"><?php echo $db['negara']; ?></option>
                            <?php
                            }   
                        }
                        ?>
                    </select>
                </div>
            </div>
            <input type="hidden" name="kode_kota" id="kode_kota">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-primary" id="add" name="add">Simpan</button>
            </div>

        <?php echo form_close(); ?>
    </div>
</div>

