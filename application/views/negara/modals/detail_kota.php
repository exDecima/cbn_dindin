<!-- Modal -->
<div id="detail_kota" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Detail Data Kota</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Kota</label>
                <div class="controls">
                    <input type="text" readonly="readonly" class="span4" name="kota" id="kota" value="<?php echo $kota; ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Negara</label>
                <div class="controls">
                    <input type="text" readonly="readonly" class="span4" name="negara" id="negara" value="<?php echo $negara; ?>">
                </div>
            </div>
            <input type="hidden" name="kode_kota" value="<?php echo $kode_kota; ?>">
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </form>
    </div>
</div>

