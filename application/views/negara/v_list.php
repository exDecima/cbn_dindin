<?php if($this->session->userdata('level')=="manager")
{ 
    $this->load->view('manager/v_header'); 
}
else if($this->session->userdata('level')=="petugas1")
{
    $this->load->view('petugas1/v_header');
}
else if($this->session->userdata('level')=="petugas2")
{
    $this->load->view('petugas2/v_header');
}
else if($this->session->userdata('level')=="customer")
{
    $this->load->view('customer/v_header');
}
?>

    <section class="page container">
        <div class="container">
            <?php echo $this->session->flashdata("p");?>
        </div>
        <div class="container">
            <?php if(validation_errors()) { ?>
                <div class="alert alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h4>Mohon Maaf Terjadi Kesalahan.</h4>
                </div>
            <?php } ?>
        <div class="row">
            <div class="span6">
                <div class="box pattern pattern-sandstone">
                    <div class="well">
                        <div class="navbar navbar-inverse">
                            <div class="navbar-inner">
                                <div class="container">
                                    <a class="brand">Data Negara</a>
                                    <div class="nav-collapse">
                                        <ul class="nav">
                                            <li><a href="#tambah_negara" class="small-box" style="text-decoration:none;" data-toggle="modal"><i class="icon-plus-sign icon-white"></i>Tambah Data</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- /navbar-inner -->
                        </div>
                        <div class="box-content box-table">
                            <table id="sample-table" class="table table-hover table-consended">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Negara</th>
                                        <th class="td-actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1;
                                        foreach($data_negara->result_array() as $dp)
                                        {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo strtoupper($dp['negara']); ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn detail-negara" href="#detail_negara" data-toggle="modal" data-kode="<?php echo $dp['kode_negara']; ?>" 
                                                    data-nama="<?php echo $dp['negara']; ?>"><i class="icon-ok-circle"></i> Lihat Detail</a>
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a class="edit-negara" href="#edit_negara" data-toggle="modal" data-kode="<?php echo $dp['kode_negara']; ?>" 
                                                    data-nama="<?php echo $dp['negara']; ?>"><i class="icon-pencil"></i> Edit Data</a></li>
                                                    <li><a href="<?php echo base_url(); ?>negara/hapus/<?php echo $dp['kode_negara']; ?>" onClick="return confirm('Anda yakin menghapus data?');"><i class="icon-trash"></i> Hapus Data</a></li>
                                                </ul>
                                            </div><!-- /btn-group -->
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span10">
                <div class="box pattern pattern-sandstone">
                    <div class="well">
                        <div class="navbar navbar-inverse">
                            <div class="navbar-inner">
                                <div class="container">
                                    <a class="brand">Data Kota</a>
                                    <div class="nav-collapse">
                                        <ul class="nav">
                                            <li><a href="#tambah_kota" class="small-box" style="text-decoration:none;" data-toggle="modal"><i class="icon-plus-sign icon-white"></i>Tambah Data</a></li>
                                        </ul>
                                    </div>
                                    <div class="span5 pull-right">
                                        <?php echo form_open('negara/cari','class="navbar-form pull-right"'); ?>
                                        <button type="reset" class="btn" onClick="window.location='<?php echo base_url().'negara'; ?>'"><i class="icon-refresh"></i></button>
                                        <select class="chosen span2" name="kode_negara" id="kode_negara" data-placeholder="Pilih negara">
                                            <option value=""></option>
                                                <?php
                                                    foreach($mst_negara->result_array() as $db)
                                                    {
                                                    ?>
                                                        <option value="<?php echo $db['kode_negara']; ?>"><?php echo $db['negara']; ?></option>
                                                    <?php
                                                    }
                                                ?>
                                        </select>
                                        <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Cari Data</button>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div><!-- /navbar-inner -->
                        </div>
                        <div class="box-content box-table">
                            <table id="sample-table" class="table table-hover table-consended">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kota</th>
                                        <th>Negara</th>
                                        <th class="td-actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=$tot+1;
                                        foreach($data_kota->result_array() as $di)
                                        {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo strtoupper($di['kota']); ?></td>
                                        <td><?php echo strtoupper($di['negara']); ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn detail-kota" href="#detail_kota" data-toggle="modal" data-kode="<?php echo $di['kode_kota']; ?>" 
                                                    data-nama="<?php echo $di['kota']; ?>" data-negara="<?php echo $di['negara']; ?>"><i class="icon-ok-circle"></i> Lihat Detail</a>
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a class="edit-kota" href="#edit_kota" data-toggle="modal" data-kode="<?php echo $di['kode_kota']; ?>" 
                                                    data-nama="<?php echo $di['kota']; ?>" data-negara="<?php echo $di['negara']; ?>"><i class="icon-pencil"></i> Edit Data</a></li>
                                                    <li><a href="<?php echo base_url(); ?>negara/hapus_kota/<?php echo $di['kode_kota']; ?>" onClick="return confirm('Anda yakin menghapus data?');"><i class="icon-trash"></i> Hapus Data</a></li>
                                                </ul>
                                            </div><!-- /btn-group -->
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination pagination-centered">
                        <ul>
                            <?php
                            echo $paginator;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('negara/modals/tambah_negara'); ?>

<?php $this->load->view('negara/modals/edit_negara'); ?>

<?php $this->load->view('negara/modals/detail_negara'); ?>

<?php $this->load->view('negara/modals/tambah_kota'); ?>

<?php $this->load->view('negara/modals/edit_kota'); ?>

<?php $this->load->view('negara/modals/detail_kota'); ?>

<?php $this->load->view('app/v_footer'); ?>

<script type="text/javascript">
    $(document).on("click", ".detail-negara", function() {
        var kode_negara = $(this).data('kode');
        var negara = $(this).data('nama');
        $(".modal-body #kode_negara").val( kode_negara );
        $(".modal-body #negara").val( negara );
        $('#detail_negara').modal('show');
    });

    $(document).on("click", ".edit-negara", function() {
        var kode_negara = $(this).data('kode');
        var negara = $(this).data('nama');
        $(".modal-body #kode_negara").val( kode_negara );
        $(".modal-body #negara").val( negara );
        $('#edit_negara').modal('show');
    });

    $(document).on("click", ".detail-kota", function() {
        var kode_kota = $(this).data('kode');
        var kota = $(this).data('nama');
        var negara = $(this).data('negara');
        $(".modal-body #kode_kota").val( kode_kota );
        $(".modal-body #kota").val( kota );
        $(".modal-body #negara").val( negara );
        $('#detail_kota').modal('show');
    });

    $(document).on("click", ".edit-kota", function() {
        var kode_kota = $(this).data('kode');
        var kota = $(this).data('nama');
        var negara = $(this).data('negara');
        $(".modal-body #kode_kota").val( kode_kota );
        $(".modal-body #kota").val( kota );
        $(".modal-body #negara").val( negara );
        $('#edit_kota').modal('show');
    });
</script>