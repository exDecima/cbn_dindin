<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$d['title']='Kelola Data Pengguna';
			$d['judul']='Kelola Data Pengguna';
			$d['pengguna']='Manager Kredit CBN';

			$d['id_user'] = "";
			$d['username'] = ""; 
			$d['password'] = ""; 
			$d['nama_lengkap'] = ""; 
			$d['level'] = "";
			$d['email'] = "";

			$d['data_grup'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_grup b on a.kode_grup=b.kode_grup");
			$d['data_pengguna'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_grup b on a.kode_grup=b.kode_grup WHERE a.level!='manager'");
			$d['mst_grup'] = $this->app_model->getAllGrup();
			$this->load->view('pengguna/v_list',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function get_add_user()
	{
		$d['id_user'] = "";
		$d['username'] = ""; 
		$d['password'] = ""; 
		$d['nama_lengkap'] = ""; 
		$d['level'] = "";
		$d['email'] = "";
		$d['foto'] = "";
		$d['st'] = "tambah";
		$d['mst_grup']=$this->app_model->manualQuery("SELECT * from t_grup where grup='petugas1' or grup='petugas2'");
		$d['grup'] = $this->app_model->get_grup();
		$this->load->view('pengguna/ajax_add_user',$d);
	}

	public function get_edit_user()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$id['id_user'] = $this->input->post('edit_user');	
			$q = $this->app_model->getSelectedData("t_user",$id);
			foreach($q->result() as $dt)
			{
				$d['id_user'] = $dt->id_user;
				$d['username'] = $dt->username; 
				$d['password'] = $dt->password; 
				$d['nama_lengkap'] = $dt->nama_lengkap;
				$d['level'] = $dt->level; 
				$d['kode_grup'] = $dt->kode_grup;
				$d['foto'] = $dt->foto; 
				$d['email'] = $dt->email;
				$d['stts'] = $dt->stts;
			}
			$d['st'] = "edit";
			$d['grup'] = $this->app_model->get_grup();
			$d['mst_grup']=$this->app_model->manualQuery("SELECT * from t_grup where grup!='manager'");
			$this->load->view('pengguna/ajax_add_user',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function get_detail_user()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
	        $id = $this->input->post('kode_grup');

	        $d['data_pengguna'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_grup b on a.kode_grup=b.kode_grup WHERE a.kode_grup = '$id' and a.level!='manager'");
		    $this->load->view('pengguna/ajax_detail_user',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function get_all_user()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
	        $d['kode_grup'] = $this->input->post('kode_grup');
	        $d['data_pengguna'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_grup b on a.kode_grup=b.kode_grup WHERE a.level!='manager'");
		    $this->load->view('pengguna/ajax_detail_user',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function hapus()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$id['id_user'] = $this->uri->segment(3);
			$this->app_model->deleteData("t_user",$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil dihapus.</div>");
			header('location:'.base_url().'pengguna');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$this->form_validation->set_rules('username', '*Username', 'trim|required|is_unique[t_user.username]');
			$this->form_validation->set_rules('nama_lengkap', '*Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('password', '*Password', 'trim|required');
			$this->form_validation->set_rules('email', '*Email', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['id_user'] = $this->input->post("id_user");
			
			if ($this->form_validation->run() == FALSE)
			{
				$d['title']='Kelola Data Pengguna';
				$d['judul']='Kelola Data Pengguna';
				$d['pengguna']='Manager Kredit CBN';

				$d['id_user'] = "";
				$d['username'] = ""; 
				$d['password'] = ""; 
				$d['nama_lengkap'] = ""; 
				$d['level'] = "";
				$d['email'] = "";

				$d['data_grup'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_grup b on a.kode_grup=b.kode_grup");
				$d['data_pengguna'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_grup b on a.kode_grup=b.kode_grup WHERE a.level!='manager'");
				$d['mst_grup'] = $this->app_model->getAllGrup();
				$this->load->view('pengguna/v_list',$d);
			}
			else
			{
				$in['username'] = $this->input->post("username");
				$in['nama_lengkap'] = $this->input->post("nama_lengkap");
				$in['password'] = md5($this->input->post("password"));
				$in['kode_grup'] = $this->input->post("kode_grup");
				$in['email'] = $this->input->post("email");
				$in['status'] = 0;
				$in['stts'] = 0;

				$ix['kode_grup'] = $this->input->post("kode_grup");
				$lvl = $this->app_model->getSelectedData("t_grup",$ix);
				foreach($lvl->result() as $dt)
				{
					$in['level'] = strtolower($dt->grup);
				}

				if(!empty($_FILES['foto']['name']))
				{
					$acak=rand(00000000000,99999999999);
					$bersih=$_FILES['foto']['name'];
					$nm=str_replace(" ","_","$bersih");
					$pisah=explode(".",$nm);
					$nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
					$nama_murni=date('Ymd-His');
					$ekstensi_kotor = $pisah[1];
						
					$file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
					$file_type_baru = strtolower($file_type);
						
					$ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
					$n_baru = $ubah.'.'.$file_type_baru;
						
					$config['upload_path']	= "./asset/images/user/";
					$config['allowed_types']= 'png|jpg|jpeg|gif';
					$config['file_name'] = $n_baru;
					$config['max_size']     = 0;
					$config['max_width']  	= 0;
					$config['max_height']  	= 0;
				 
					$this->load->library('upload', $config);
				 
					if ($this->upload->do_upload("foto")) {
						$data	 	= $this->upload->data();
				 
						/* PATH */
						$source             = "./asset/images/user/".$data['file_name'] ;
						$destination_thumb	= "./asset/images/user/thumb/" ;
						$destination_medium	= "./asset/images/user/medium/" ;
				 
						// Permission Configuration
						chmod($source, 0777) ;
				 
						/* Resizing Processing */
						// Configuration Of Image Manipulation :: Static
						$this->load->library('image_lib') ;
						$img['image_library'] = 'GD2';
						$img['create_thumb']  = TRUE;
						$img['maintain_ratio']= TRUE;
				 
						/// Limit Width Resize
						$limit_medium   = 280 ;
						$limit_thumb    = 120 ;
				 
						// Size Image Limit was using (LIMIT TOP)
						$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
				 
						// Percentase Resize
						if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
							$percent_medium = $limit_medium/$limit_use ;
							$percent_thumb  = $limit_thumb/$limit_use ;
						}
				 
						//// Making THUMBNAIL ///////
						$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
						$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_thumb ;
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
				 
						////// Making MEDIUM /////////////
						$img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
						$img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_medium ;
							
						$in['foto'] = $data['file_name'];
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
					}
				}
				$this->app_model->insertData("t_user",$in);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil ditambahkan.</div>");
				header('location:'.base_url().'pengguna');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_edit()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$this->form_validation->set_rules('nama_lengkap', '*Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('password', '*Password', 'trim|required');
			$this->form_validation->set_rules('email', '*Email', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['id_user'] = $this->input->post("id_user");
			
			if ($this->form_validation->run() == FALSE)
			{
				$d['title']='Kelola Data Pengguna';
				$d['judul']='Kelola Data Pengguna';
				$d['pengguna']='Manager Kredit CBN';

				$d['id_user'] = "";
				$d['username'] = ""; 
				$d['password'] = ""; 
				$d['nama_lengkap'] = ""; 
				$d['level'] = "";
				$d['email'] = "";

				$d['data_grup'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_grup b on a.kode_grup=b.kode_grup");
				$d['data_pengguna'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_grup b on a.kode_grup=b.kode_grup WHERE a.level!='manager'");
				$d['mst_grup'] = $this->app_model->getAllGrup();
				$this->load->view('pengguna/v_list',$d);
			}
			else
			{
				$upd['username'] = $this->input->post("username");
				$upd['nama_lengkap'] = $this->input->post("nama_lengkap");
				$upd['password'] = md5($this->input->post("password"));
				$upd['kode_grup'] = $this->input->post("kode_grup");
				$upd['email'] = $this->input->post("email");
				$upd['status'] = 0;
				$upd['stts'] = 0;

				$ix['kode_grup'] = $this->input->post("kode_grup");
				$lvl = $this->app_model->getSelectedData("t_grup",$ix);
				foreach($lvl->result() as $dt)
				{
					$upd['level'] = strtolower($dt->grup);
				}

				if(!empty($_FILES['foto']['name']))
				{
					$acak=rand(00000000000,99999999999);
					$bersih=$_FILES['foto']['name'];
					$nm=str_replace(" ","_","$bersih");
					$pisah=explode(".",$nm);
					$nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
					$nama_murni=date('Ymd-His');
					$ekstensi_kotor = $pisah[1];
						
					$file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
					$file_type_baru = strtolower($file_type);
						
					$ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
					$n_baru = $ubah.'.'.$file_type_baru;
						
					$config['upload_path']	= "./asset/images/user/";
					$config['allowed_types']= 'png|jpg|jpeg|gif';
					$config['file_name'] = $n_baru;
					$config['max_size']     = 0;
					$config['max_width']  	= 0;
					$config['max_height']  	= 0;
				 
					$this->load->library('upload', $config);
				 
					if ($this->upload->do_upload("foto")) {
						$data	 	= $this->upload->data();
				 
						/* PATH */
						$source             = "./asset/images/user/".$data['file_name'] ;
						$destination_thumb	= "./asset/images/user/thumb/" ;
						$destination_medium	= "./asset/images/user/medium/" ;
				 
						// Permission Configuration
						chmod($source, 0777) ;
				 
						/* Resizing Processing */
						// Configuration Of Image Manipulation :: Static
						$this->load->library('image_lib') ;
						$img['image_library'] = 'GD2';
						$img['create_thumb']  = TRUE;
						$img['maintain_ratio']= TRUE;
				 
						/// Limit Width Resize
						$limit_medium   = 280 ;
						$limit_thumb    = 120 ;
				 
						// Size Image Limit was using (LIMIT TOP)
						$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
				 
						// Percentase Resize
						if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
							$percent_medium = $limit_medium/$limit_use ;
							$percent_thumb  = $limit_thumb/$limit_use ;
						}
				 
						//// Making THUMBNAIL ///////
						$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
						$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_thumb ;
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
				 
						////// Making MEDIUM /////////////
						$img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
						$img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_medium ;
							
						$upd['foto'] = $data['file_name'];
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
					}
				}
				$this->app_model->updateData("t_user",$upd,$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil diubah.</div>");
				header('location:'.base_url().'pengguna');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function ubah_status()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$id['id_user'] = $this->uri->segment(3);
			$q = $this->app_model->getSelectedData("t_user",$id);
			foreach($q->result() as $dt)
			{
				$d['id_user'] = $dt->id_user;
				$d['username'] = $dt->username; 
				$d['password'] = $dt->password;
				$d['nama_lengkap'] = $dt->nama_lengkap;
				$d['level'] = $dt->level; 
				$d['kode_grup'] = $dt->kode_grup;
				$d['stts'] = $dt->stts;
			}
			if($d['stts']==0){
				$upd['stts'] = 1;
				$this->app_model->updateData("t_user",$upd,$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Status Pengguna ".$d['username']." Aktif.</div>");
			}
			else if($d['stts']==1){
				$upd['stts'] = 0;
				$this->app_model->updateData("t_user",$upd,$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Status Pengguna ".$d['username']." Tidak Aktif.</div>");
			}
			header('location:'.base_url().'pengguna');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function kembali()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			header('location:'.base_url().'pengguna');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

}