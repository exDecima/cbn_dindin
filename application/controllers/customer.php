<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$page=$this->uri->segment(3);
			$limit=$this->config->item('limit_data');
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$d['pengguna']='Petugas 1 Kredit CBN';
			$d['title']='Kelola Data Customers';
			$d['judul']='Kelola Data Customers';
			
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->manualQuery("SELECT * from t_user where level='customer'");
			$config['base_url'] = base_url() . 'customer/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();

			$d['data_customer'] = $this->app_model->manualQuery("SELECT * from t_user a left join t_kota b on a.kode_kota=b.kode_kota left join t_negara c on a.kode_negara=c.kode_negara WHERE a.level='customer' ORDER BY a.nama_lengkap DESC limit ".$offset.",".$limit."");
			
			$this->load->view('customer/v_list',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function tambah()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$d['pengguna']='Petugas 1 Kredit CBN';
			$d['title']='Tambah Data Customer CBN';
			$d['judul']='Tambah Customer';

			$d['id_user'] = "";
			$d['mst_kota'] = $this->app_model->getAllKota();
			$d['negara']=$this->app_model->get_negara();
			$this->load->view('customer/v_input',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function edit()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$d['pengguna']='Petugas 1 Kredit CBN';
			$d['title']='Edit Data Customer CBN';
			$d['judul']='Edit Customer';

			$id['id_user'] = $this->uri->segment(3);	
			$q = $this->app_model->getSelectedData("t_user",$id);
			foreach($q->result() as $dt)
			{
				$d['id_user'] = $dt->id_user;
				$d['username'] = $dt->username; 
				$d['password'] = $dt->password; 
				$d['nama_lengkap'] = $dt->nama_lengkap;
				$d['level'] = $dt->level; 
				$d['kode_grup'] = $dt->kode_grup;
				$d['stts'] = $dt->stts;
				$d['foto'] = $dt->foto; 
				$d['status'] = $dt->status; 
				$d['kode_negara'] = $dt->kode_negara;
				$d['kode_kota'] = $dt->kode_kota; 
				$d['penghasilan'] = $dt->penghasilan;
				$d['email'] = $dt->email;
			}
			$d['mst_kota'] = $this->app_model->getAllKota();
			$d['negara']=$this->app_model->get_negara();
			$this->load->view('customer/v_edit',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function detail()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$d['pengguna']='Petugas 1 Kredit CBN';
			$d['title']='Detail Data Customer CBN';
			$d['judul']='Detail Customer';

			$id = $this->uri->segment(3);	
			$q = $this->app_model->manualQuery("SELECT * from t_user a left join t_kota b on a.kode_kota=b.kode_kota left join t_negara c on a.kode_negara=c.kode_negara WHERE a.id_user='$id'");
			foreach($q->result() as $dt)
			{
				$d['id_user'] = $dt->id_user;
				$d['username'] = $dt->username; 
				$d['password'] = $dt->password; 
				$d['nama_lengkap'] = $dt->nama_lengkap;
				$d['level'] = $dt->level; 
				$d['kode_grup'] = $dt->kode_grup;
				$d['stts'] = $dt->stts;
				$d['foto'] = $dt->foto; 
				$d['status'] = $dt->status; 
				$d['kode_negara'] = $dt->kode_negara;
				$d['kode_kota'] = $dt->kode_kota; 
				$d['penghasilan'] = $dt->penghasilan;
				$d['email'] = $dt->email;
				$d['negara'] = $dt->negara;
				$d['kota'] = $dt->kota;
			}
			$this->load->view('customer/v_detail',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function get_kota()
	{
		$data['kota']=$this->app_model->get_kota($this->uri->segment(3));
		$this->load->view('customer/ajax_get_kota',$data);
	}

	public function cari()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$d['pengguna']='Petugas 1 Kredit CBN';
			$d['title']='Kelola Data Customers';
			$d['judul']='Kelola Data Customers';
			
			$kata = $this->input->post("cari");
			
			$page=$this->uri->segment(3);
			$limit=$this->config->item('limit_data');
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$d['tot'] = $offset;
			$kata = str_replace("'", "''", $kata);
			$tot_hal = $this->app_model->manualQuery("select * from t_user a left join t_kota b on a.kode_kota=b.kode_kota left join t_negara c on a.kode_negara=c.kode_negara where nama_lengkap like '%".$kata."%' or kota like '%".$kata."%' or negara like '%".$kata."%' or penghasilan like '%".$kata."%'
				or email like '%".$kata."%' ");
			$config['base_url'] = base_url() . 'customer/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$d['data_customer'] = $this->app_model->manualQuery("select * from t_user a left join t_kota b on a.kode_kota=b.kode_kota left join t_negara c on a.kode_negara=c.kode_negara where nama_lengkap like '%".$kata."%' or kota like '%".$kata."%' or negara like '%".$kata."%' or penghasilan like '%".$kata."%'
				or email like '%".$kata."%' limit ".$offset.",".$limit."");
			
			$this->load->view('customer/v_list',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('email', '*Email', 'trim|required|is_unique[t_user.email]');
			$this->form_validation->set_rules('username', '*Username', 'trim|required|is_unique[t_user.username]');
			$this->form_validation->set_rules('nama_lengkap', '*Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('password', '*Password', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['id_user'] = $this->input->post("id_user");
			
			if ($this->form_validation->run() == FALSE)
			{
				$d['pengguna']='Petugas 1 Kredit CBN';
				$d['title']='Tambah Data Customer CBN';
				$d['judul']='Tambah Customer';

				$d['id_user'] = "";
				$d['mst_kota'] = $this->app_model->getAllKota();
				$d['negara']=$this->app_model->get_negara();
				$this->load->view('customer/v_input',$d);
			}
			else
			{
				$in['username'] = $this->input->post("username");
				$in['nama_lengkap'] = $this->input->post("nama_lengkap");
				$in['password'] = md5($this->input->post("password"));
				$in['kode_grup'] = 1;
				$in['email'] = $this->input->post("email");
				$in['status'] = 0;
				$in['stts'] = 0;
				$in['level'] = 'customer';
				$in['kode_kota'] = $this->input->post("kode_kota");
				$in['kode_negara'] = $this->input->post("kode_negara");
				$in['penghasilan'] = $this->input->post("penghasilan");
				if(!empty($_FILES['foto']['name']))
				{
					$acak=rand(00000000000,99999999999);
					$bersih=$_FILES['foto']['name'];
					$nm=str_replace(" ","_","$bersih");
					$pisah=explode(".",$nm);
					$nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
					$nama_murni=date('Ymd-His');
					$ekstensi_kotor = $pisah[1];
						
					$file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
					$file_type_baru = strtolower($file_type);
						
					$ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
					$n_baru = $ubah.'.'.$file_type_baru;
						
					$config['upload_path']	= "./asset/images/user/";
					$config['allowed_types']= 'png|jpg|jpeg|gif';
					$config['file_name'] = $n_baru;
					$config['max_size']     = 0;
					$config['max_width']  	= 0;
					$config['max_height']  	= 0;
				 
					$this->load->library('upload', $config);
				 
					if ($this->upload->do_upload("foto")) {
						$data	 	= $this->upload->data();
				 
						/* PATH */
						$source             = "./asset/images/user/".$data['file_name'] ;
						$destination_thumb	= "./asset/images/user/thumb/" ;
						$destination_medium	= "./asset/images/user/medium/" ;
				 
						// Permission Configuration
						chmod($source, 0777) ;
				 
						/* Resizing Processing */
						// Configuration Of Image Manipulation :: Static
						$this->load->library('image_lib') ;
						$img['image_library'] = 'GD2';
						$img['create_thumb']  = TRUE;
						$img['maintain_ratio']= TRUE;
				 
						/// Limit Width Resize
						$limit_medium   = 280 ;
						$limit_thumb    = 120 ;
				 
						// Size Image Limit was using (LIMIT TOP)
						$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
				 
						// Percentase Resize
						if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
							$percent_medium = $limit_medium/$limit_use ;
							$percent_thumb  = $limit_thumb/$limit_use ;
						}
				 
						//// Making THUMBNAIL ///////
						$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
						$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_thumb ;
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
				 
						////// Making MEDIUM /////////////
						$img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
						$img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_medium ;
							
						$in['foto'] = $data['file_name'];
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
					}
				}
				$this->app_model->insertData("t_user",$in);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil ditambahkan, silahkan ke pihak Manager untuk mengaktifkan data.</div>");
				header('location:'.base_url().'customer');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_edit()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('email', '*Email', 'trim|required');
			$this->form_validation->set_rules('username', '*Username', 'trim|required');
			$this->form_validation->set_rules('password', '*Password', 'trim|required');
			$this->form_validation->set_rules('nama_lengkap', '*Nama Lengkap', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['id_user'] = $this->input->post("id_user");
			
			if ($this->form_validation->run() == FALSE)
			{
				$d['pengguna']='Petugas 1 Kredit CBN';
				$d['title']='Edit Data Customer CBN';
				$d['judul']='Edit Customer';

				$id['id_user'] = $this->uri->segment(3);	
				$q = $this->app_model->getSelectedData("t_user",$id);
				foreach($q->result() as $dt)
				{
					$d['id_user'] = $dt->id_user;
					$d['username'] = $dt->username; 
					$d['password'] = $dt->password; 
					$d['nama_lengkap'] = $dt->nama_lengkap;
					$d['level'] = $dt->level; 
					$d['kode_grup'] = $dt->kode_grup;
					$d['stts'] = $dt->stts;
					$d['foto'] = $dt->foto; 
					$d['status'] = $dt->status; 
					$d['kode_negara'] = $dt->kode_negara;
					$d['kode_kota'] = $dt->kode_kota; 
					$d['penghasilan'] = $dt->penghasilan;
					$d['email'] = $dt->email;
				}
				$d['mst_kota'] = $this->app_model->getAllKota();
				$d['negara']=$this->app_model->get_negara();
				$this->load->view('customer/v_edit',$d);
			}
			else
			{
				$upd['username'] = $this->input->post("username");
				$upd['nama_lengkap'] = $this->input->post("nama_lengkap");
				$upd['password'] = md5($this->input->post("password"));
				$upd['email'] = $this->input->post("email");
				$upd['status'] = 0;
				$upd['stts'] = 0;
				$upd['kode_kota'] = $this->input->post("kode_kota");
				$upd['kode_negara'] = $this->input->post("kode_negara");
				$upd['penghasilan'] = $this->input->post("penghasilan");
				if(!empty($_FILES['foto']['name']))
				{
					$acak=rand(00000000000,99999999999);
					$bersih=$_FILES['foto']['name'];
					$nm=str_replace(" ","_","$bersih");
					$pisah=explode(".",$nm);
					$nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
					$nama_murni=date('Ymd-His');
					$ekstensi_kotor = $pisah[1];
						
					$file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
					$file_type_baru = strtolower($file_type);
						
					$ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
					$n_baru = $ubah.'.'.$file_type_baru;
						
					$config['upload_path']	= "./asset/images/user/";
					$config['allowed_types']= 'png|jpg|jpeg|gif';
					$config['file_name'] = $n_baru;
					$config['max_size']     = 0;
					$config['max_width']  	= 0;
					$config['max_height']  	= 0;
				 
					$this->load->library('upload', $config);
				 
					if ($this->upload->do_upload("foto")) {
						$data	 	= $this->upload->data();
				 
						/* PATH */
						$source             = "./asset/images/user/".$data['file_name'] ;
						$destination_thumb	= "./asset/images/user/thumb/" ;
						$destination_medium	= "./asset/images/user/medium/" ;
				 
						// Permission Configuration
						chmod($source, 0777) ;
				 
						/* Resizing Processing */
						// Configuration Of Image Manipulation :: Static
						$this->load->library('image_lib') ;
						$img['image_library'] = 'GD2';
						$img['create_thumb']  = TRUE;
						$img['maintain_ratio']= TRUE;
				 
						/// Limit Width Resize
						$limit_medium   = 280 ;
						$limit_thumb    = 120 ;
				 
						// Size Image Limit was using (LIMIT TOP)
						$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
				 
						// Percentase Resize
						if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
							$percent_medium = $limit_medium/$limit_use ;
							$percent_thumb  = $limit_thumb/$limit_use ;
						}
				 
						//// Making THUMBNAIL ///////
						$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
						$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_thumb ;
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
				 
						////// Making MEDIUM /////////////
						$img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
						$img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_medium ;
							
						$upd['foto'] = $data['file_name'];
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
					}
				}
				$this->app_model->updateData("t_user",$upd,$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil diubah, silahkan ke pihak Manager untuk mengaktifkan data.</div>");
				header('location:'.base_url().'customer');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function hapus()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$id['id_user'] = $this->uri->segment(3);
			$this->app_model->deleteData("t_user",$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button tipe=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil dihapus.</div>");
			header('location:'.base_url().'customer');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function kembali()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			header('location:'.base_url().'customer');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
}
