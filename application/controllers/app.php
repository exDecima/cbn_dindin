<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
               
	}

	public function index()
	{
		if($this->session->userdata('logged_in')=="")
		{
			$this->form_validation->set_rules('username', '*Username', 'trim|required');
			$this->form_validation->set_rules('password', '*Password', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			
			if ($this->form_validation->run() == FALSE)
			{
				$q = $this->app_model->manualQuery("SELECT * from t_background WHERE kode_bg=1");
				foreach($q->result() as $dt){
					$d['gambar']=$dt->gambar;
					$d['warna']=$dt->warna;
				}
				$this->load->view('app/login/v_login_customer',$d);
			}
			else
			{
				$dt['username'] = $this->input->post('username');
				$dt['password'] = $this->input->post('password');
				$dt['kode_grup'] = $this->input->post('kode_grup');
				$this->login_customer_model->getLoginData($dt);
			}
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')==1)
		{
			header('location:'.base_url().'home');
		}
	}

	public function login_petugas()
	{
		if($this->session->userdata('logged_in')=="")
		{
			$this->form_validation->set_rules('username', '*Username', 'trim|required');
			$this->form_validation->set_rules('password', '*Password', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			
			if ($this->form_validation->run() == FALSE)
			{
				$q = $this->db->query("SELECT * from t_background WHERE kode_bg=1");
				foreach($q->result() as $dt){
					$data['gambar']=$dt->gambar;
					$data['warna']=$dt->warna;
				}
				$data['grup']=$this->login_petugas_model->get_grup();
				$this->load->view('app/login/v_login_petugas',$data);
			}
			else
			{
				$dt['username'] = $this->input->post('username');
				$dt['password'] = $this->input->post('password');
				$dt['kode_grup'] = $this->input->post('kode_grup');
				$this->login_petugas_model->getLoginData($dt);
			}
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')==1)
		{
			header('location:'.base_url().'home');
		}
	}

	public function ubah_password()
	{
		if($this->session->userdata('logged_in')!="")
		{		
			if($this->session->userdata('level')=="customer")
			{
				$d['pengguna']='Customer Kredit CBN';
			}
			else if($this->session->userdata('level')=="petugas1")
			{
				$d['pengguna']='Petugas 1 Kredit CBN';
			}
			else if($this->session->userdata('level')=="petugas2")
			{
				$d['pengguna']='Petugas 2 Kredit CBN';
			}
			else if($this->session->userdata('level')=="manager")
			{
				$d['pengguna']='Manager Kredit CBN';
			}
			$d['title']='Pengaturan Akun';
			$d['judul']='Pengaturan Akun';
			$d['username'] = $this->session->userdata("username");
			$d['nama_lengkap'] = $this->session->userdata("nama_lengkap");
			$d['foto'] = $this->session->userdata("foto");
			$d['email'] = $this->session->userdata("email");
			$this->load->view('app/v_ubah_password',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
	
	public function simpan_password()
	{
		if($this->session->userdata('logged_in')!="")
		{
			$this->form_validation->set_rules('pass_lama', '*Password Lama', 'trim|required');
			$this->form_validation->set_rules('pass_baru', '*Password Baru', 'trim|required');
			$this->form_validation->set_rules('ulangi_pass_baru', '*Ulangi Password Baru', 'trim|required');

			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			
			$id['username'] = $this->input->post("username");
			$pass_lama = $this->input->post("pass_lama");
			$pass_baru = $this->input->post("pass_baru");
			$ulangi_pass_baru = $this->input->post("ulangi_pass_baru");
			
			$set['tab_a'] = "active";
			$set['tab_b'] = "";
			$set['tab_c'] = "";
			$set['tab_d'] = "";
			$this->session->set_userdata($set);
			
			if ($this->form_validation->run() == FALSE)
			{
				if($this->session->userdata('level')=="customer")
				{
					$d['pengguna']='Customer Kredit CBN';
				}
				else if($this->session->userdata('level')=="petugas1")
				{
					$d['pengguna']='Petugas 1 Kredit CBN';
				}
				else if($this->session->userdata('level')=="petugas2")
				{
					$d['pengguna']='Petugas 2 Kredit CBN';
				}
				else if($this->session->userdata('level')=="manager")
				{
					$d['pengguna']='Manager Kredit CBN';
				}
				$d['title']='Pengaturan Akun';
				$d['judul']='Pengaturan Akun';
				$d['username'] = $this->session->userdata("username");
				$d['nama_lengkap'] = $this->session->userdata("nama_lengkap");
				$d['foto'] = $this->session->userdata("foto");
				$d['email'] = $this->session->userdata("email");
				$this->load->view('app/v_ubah_password',$d);
			}
			else
			{
				$login['username'] = $id['username'];
				$login['password'] = md5($pass_lama);
				$cek = $this->app_model->getSelectedData('t_user', $login);
				if($cek->num_rows()>0)
				{
					if($pass_baru==$ulangi_pass_baru)
					{
						$upd['password'] = md5($pass_baru);
						$this->app_model->updateData("t_user",$upd,$id);
						$this->session->set_flashdata("pass", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Berhasil mengubah password.</div>");
						header('location:'.base_url().'app/ubah_password');
					}
					else
					{
					$this->session->set_flashdata("pass", "<div class=\"alert alert-block\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Maaf password baru tidak sama.</div>");
						header('location:'.base_url().'app/ubah_password');
					}
				}
				else
				{
					$this->session->set_flashdata("pass", "<div class=\"alert alert-block\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Maaf password lama salah.</div>");
					header('location:'.base_url().'app/ubah_password');
				}
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
	
	public function simpan_nama()
	{
		if($this->session->userdata('logged_in')!="")
		{
			$this->form_validation->set_rules('nama_lengkap', '*Nama Pengguna', 'trim|required');

			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			
			$id['username'] = $this->input->post("username");
			
			$set['tab_a'] = "";
			$set['tab_b'] = "active";
			$set['tab_c'] = "";
			$set['tab_d'] = "";
			$this->session->set_userdata($set);
			
			if ($this->form_validation->run() == FALSE)
			{
				if($this->session->userdata('level')=="customer")
				{
					$d['pengguna']='Customer Kredit CBN';
				}
				else if($this->session->userdata('level')=="petugas1")
				{
					$d['pengguna']='Petugas 1 Kredit CBN';
				}
				else if($this->session->userdata('level')=="petugas2")
				{
					$d['pengguna']='Petugas 2 Kredit CBN';
				}
				else if($this->session->userdata('level')=="manager")
				{
					$d['pengguna']='Manager Kredit CBN';
				}
				$d['title']='Pengaturan Akun';
				$d['judul']='Pengaturan Akun';
				$d['username'] = $this->session->userdata("username");
				$d['nama_lengkap'] = $this->session->userdata("nama_lengkap");
				$d['foto'] = $this->session->userdata("foto");
				$d['email'] = $this->session->userdata("email");
				$this->load->view('app/v_ubah_password',$d);
			}
			else
			{
				$upd['nama_lengkap'] = $this->input->post("nama_lengkap");
				$this->app_model->updateData("t_user",$upd,$id);
				$this->session->set_flashdata("pass", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Berhasil mengubah nama.</div>");
				$set_new['nama_lengkap'] = $upd['nama_lengkap'];
				$this->session->set_userdata($set_new);
				header('location:'.base_url().'app/ubah_password');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_foto()
	{
		if($this->session->userdata('logged_in')!="")
		{
			$id['username'] = $this->input->post("username");
						
			$set['tab_a'] = "";
			$set['tab_b'] = "";
			$set['tab_c'] = "active";
			$set['tab_d'] = "";
			$this->session->set_userdata($set);
			
				if(!empty($_FILES['foto']['name']))
				{
					$acak=rand(00000000000,99999999999);
					$bersih=$_FILES['foto']['name'];
					$nm=str_replace(" ","_","$bersih");
					$pisah=explode(".",$nm);
					$nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
					$nama_murni=date('Ymd-His');
					$ekstensi_kotor = $pisah[1];
						
					$file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
					$file_type_baru = strtolower($file_type);
						
					$ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
					$n_baru = $ubah.'.'.$file_type_baru;
						
					$config['upload_path']	= "./asset/images/user/";
					$config['allowed_types']= 'png|jpg|jpeg|gif';
					$config['file_name'] = $n_baru;
					$config['max_size']     = '25000';
					$config['max_width']  	= '30000';
					$config['max_height']  	= '30000';
				 
					$this->load->library('upload', $config);
				 
					if ($this->upload->do_upload("foto")) {
						$data	 	= $this->upload->data();
				 
						/* PATH */
						$source             = "./asset/images/user/".$data['file_name'] ;
						$destination_thumb	= "./asset/images/user/thumb/" ;
						$destination_medium	= "./asset/images/user/medium/" ;
				 
						// Permission Configuration
						chmod($source, 0777) ;
				 
						/* Resizing Processing */
						// Configuration Of Image Manipulation :: Static
						$this->load->library('image_lib') ;
						$img['image_library'] = 'GD2';
						$img['create_thumb']  = TRUE;
						$img['maintain_ratio']= TRUE;
				 
						/// Limit Width Resize
						$limit_medium   = 320 ;
						$limit_thumb    = 160 ;
				 
						// Size Image Limit was using (LIMIT TOP)
						$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
				 
						// Percentase Resize
						if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
							$percent_medium = $limit_medium/$limit_use ;
							$percent_thumb  = $limit_thumb/$limit_use ;
						}
				 
						//// Making THUMBNAIL ///////
						$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
						$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_thumb ;
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
				 
						////// Making MEDIUM /////////////
						$img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
						$img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;
				 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '100%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_medium ;
							
						$upd['foto'] = $data['file_name'];
				 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
					}
				}
			$this->app_model->updateData("t_user",$upd,$id);
			$this->session->set_flashdata("pass", "<div class=\"alert alert-info\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Berhasil mengubah foto.</div>");
			$set_new['foto'] = $upd['foto'];
			$this->session->set_userdata($set_new);
			header('location:'.base_url().'app/ubah_password');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_email()
	{
		if($this->session->userdata('logged_in')!="")
		{
			$this->form_validation->set_rules('email', '*Email', 'trim|required');

			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			
			$id['username'] = $this->input->post("username");
			
			$set['tab_a'] = "";
			$set['tab_b'] = "";
			$set['tab_c'] = "";
			$set['tab_d'] = "active";
			$this->session->set_userdata($set);
			
			if ($this->form_validation->run() == FALSE)
			{
				if($this->session->userdata('level')=="customer")
				{
					$d['pengguna']='Customer Kredit CBN';
				}
				else if($this->session->userdata('level')=="petugas1")
				{
					$d['pengguna']='Petugas 1 Kredit CBN';
				}
				else if($this->session->userdata('level')=="petugas2")
				{
					$d['pengguna']='Petugas 2 Kredit CBN';
				}
				else if($this->session->userdata('level')=="manager")
				{
					$d['pengguna']='Manager Kredit CBN';
				}
				$d['title']='Pengaturan Akun';
				$d['judul']='Pengaturan Akun';
				$d['username'] = $this->session->userdata("username");
				$d['nama_lengkap'] = $this->session->userdata("nama_lengkap");
				$d['foto'] = $this->session->userdata("foto");
				$d['email'] = $this->session->userdata("email");
				$this->load->view('app/v_ubah_password',$d);
			}
			else
			{
				$upd['email'] = $this->input->post("email");
				$this->app_model->updateData("t_user",$upd,$id);
				$this->session->set_flashdata("pass", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Berhasil mengubah email.</div>");
				$set_new['email'] = $upd['email'];
				$this->session->set_userdata($set_new);
				header('location:'.base_url().'app/ubah_password');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function setting()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$d['error'] = '';
			$d['title']='Pengaturan Sistem';
			$d['judul']='Pengaturan Sistem';
			$d['pengguna']='Manager CBN';
			$q = $this->db->query("SELECT * from t_background WHERE kode_bg=1");
				foreach($q->result() as $dt){
					$d['kode_bg']=$dt->kode_bg;
					$d['gambar']=$dt->gambar;
					$d['warna']=$dt->warna;
				}
			$this->load->view('app/v_setting', $d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function backup()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$now = date('dmY-His');
			$this->load->dbutil();
			$config = array(
				'tables'=>array(
					't_grup',
					't_jeniskendaraan',
					't_kendaraan',
					't_kota',
					't_negara',
					't_pengajuan',
					't_user'),
				'ignore'=>array(
					't_background'),
				'format'=>'txt',
				'filename'=>'backup.sql'
			);
			$backup =& $this->dbutil->backup($config);
			$file_name = 'backup-'.$now.'.sql';
			force_download($file_name, $backup);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function restore()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="administrator")
		{
			//upload file
			$fupload = $_FILES['resfile'];
			$nama = $_FILES['resfile']['name'];
			if(isset($fupload)){
				$lokasi_file = $fupload['tmp_name'];
				$direktori="./upload/setting/".$nama;
				move_uploaded_file($lokasi_file,"$direktori");
			}
			//restore database
			$isi_file=file_get_contents($direktori);
			$string_query=rtrim($isi_file, "\n;" );
			$array_query=explode(";", $string_query);
			foreach($array_query as $query){
				$this->db->query("SET FOREIGN_KEY_CHECKS = 0");
				$this->db->query($query);
				$this->db->query("SET FOREIGN_KEY_CHECKS = 1");
			}
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Restore Database Selesai dan Berhasil.</div>");
			redirect('app/setting');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function ubah_bg(){
					if(!empty($_FILES['gambar']['name']))
					{
						$acak=rand(00000000000,99999999999);
						$bersih=$_FILES['gambar']['name'];
						$nm=str_replace(" ","_","$bersih");
						$pisah=explode(".",$nm);
						$nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
						$nama_murni=date('Ymd-His');
						$ekstensi_kotor = $pisah[1];
						
						$file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
						$file_type_baru = strtolower($file_type);
						
						$ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
						$n_baru = $ubah.'.'.$file_type_baru;
						
						$config['upload_path']	= "./asset/foto_barang/";
						$config['allowed_types']= 'gif|jpg|png|jpeg';
						$config['file_name'] = $n_baru;
						$config['max_size']     = '50000';
						$config['max_width']  	= '30000';
						$config['max_height']  	= '30000';
				 
						$this->load->library('upload', $config);
				 
						if ($this->upload->do_upload("gambar")) {
							$data	 	= $this->upload->data();
				 
							/* PATH */
							$source             = "./asset/foto_barang/".$data['file_name'] ;
							$destination_thumb	= "./asset/foto_barang/thumb/" ;
							$destination_medium	= "./asset/foto_barang/medium/" ;
				 
							// Permission Configuration
							chmod($source, 0777) ;
				 
							/* Resizing Processing */
							// Configuration Of Image Manipulation :: Static
							$this->load->library('image_lib') ;
							$img['image_library'] = 'GD2';
							$img['create_thumb']  = TRUE;
							$img['maintain_ratio']= TRUE;
				 
							/// Limit Width Resize
							$limit_medium   = 425 ;
							$limit_thumb    = 220 ;
				 
							// Size Image Limit was using (LIMIT TOP)
							$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
				 
							// Percentase Resize
							if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
								$percent_medium = $limit_medium/$limit_use ;
								$percent_thumb  = $limit_thumb/$limit_use ;
							}
				 
							//// Making THUMBNAIL ///////
							$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
							$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
				 
							// Configuration Of Image Manipulation :: Dynamic
							$img['thumb_marker'] = '';
							$img['quality']      = '100%' ;
							$img['source_image'] = $source ;
							$img['new_image']    = $destination_thumb ;
				 
							// Do Resizing
							$this->image_lib->initialize($img);
							$this->image_lib->resize();
							$this->image_lib->clear() ;
				 
							////// Making MEDIUM /////////////
							$img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
							$img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;
				 
							// Configuration Of Image Manipulation :: Dynamic
							$img['thumb_marker'] = '';
							$img['quality']      = '100%' ;
							$img['source_image'] = $source ;
							$img['new_image']    = $destination_medium ;
							
							$in['gambar'] = $data['file_name'];
				 
							// Do Resizing
							$this->image_lib->initialize($img);
							$this->image_lib->resize();
							$this->image_lib->clear() ;
						}
					}

				$id['kode_bg'] = 1;
				if($this->input->post('warna')!=""||$in['gambar']!=""){
					$in['warna'] = $this->input->post('warna');
					$this->app_model->updateData("t_background",$in,$id);
				}
				
				header('location:'.base_url().'app/setting');
	}

	function logout()
	{
		$id['id_user']=$this->session->userdata('id_user');
		$upd['status']=0;
		$this->app_model->updateData("t_user",$upd,$id);
		$this->session->sess_destroy();
		header('location:'.base_url().'');
	}
}

/* End of file app.php */
/* Location: ./application/controllers/app.php */