<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kendaraan extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$d['title']='Kelola Kendaraan';
			$d['judul']='Kelola Kendaraan';
			$d['pengguna']='Petugas 1 Kredit CBN';
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->getAllMerek();
			$config['base_url'] = base_url() . 'kendaraan/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$d['kode_kendaraan'] = "";
			$d['kendaraan'] = "";
			$d['kode_merek'] = "";
			$d['merek'] = "";
			$d['mst_kendaraan'] = $this->app_model->getAllKendaraan();
			$d['data_kendaraan'] = $this->app_model->manualQuery("SELECT * from t_kendaraan");
			$d['data_merek'] = $this->app_model->manualQuery("SELECT * from t_merek a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan limit ".$offset.",".$limit."");
			
			$this->load->view('kendaraan/v_list',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function cari()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$d['title']='Kelola Kendaraan';
			$d['judul']='Kelola Kendaraan';
			$d['pengguna']='Petugas 1 Kredit CBN';
			
			$kata = $this->input->post("kode_kendaraan");
			
			$page=$this->uri->segment(3);
			$limit=30;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->manualQuery("select * from t_merek a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan where a.kode_kendaraan='$kata'");
			$config['base_url'] = base_url() . 'kendaraan/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();

			$d['kode_kendaraan'] = "";
			$d['kendaraan'] = "";
			$d['kode_merek'] = "";
			$d['merek'] = "";
			$d['mst_kendaraan'] = $this->app_model->getAllkendaraan();
			$d['data_kendaraan'] = $this->app_model->manualQuery("SELECT * from t_kendaraan");
			$d['data_merek'] = $this->app_model->manualQuery("SELECT * from t_merek a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan where a.kode_kendaraan='$kata' limit ".$offset.",".$limit."");
			$this->load->view('kendaraan/v_list',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

//kendaraan
	public function simpan()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('kendaraan', '*Nama kendaraan', 'trim|required|is_unique[t_kendaraan.kendaraan]');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['kode_kendaraan'] = $this->input->post("kode_kendaraan");
			
			if ($this->form_validation->run() == FALSE)
			{
				$page=$this->uri->segment(3);
				$limit=10;
				if(!$page):
				$offset = 0;
				else:
				$offset = $page;
				endif;

				$d['title']='Kelola Kendaraan';
				$d['judul']='Kelola Kendaraan';
				$d['pengguna']='Petugas 1 Kredit CBN';
				$d['tot'] = $offset;
				$tot_hal = $this->app_model->getAllmerek();
				$config['base_url'] = base_url() . 'kendaraan/index/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
				$d["paginator"] =$this->pagination->create_links();
				
				$d['kode_kendaraan'] = "";
				$d['kendaraan'] = "";
				$d['kode_merek'] = "";
				$d['merek'] = "";
				$d['mst_kendaraan'] = $this->app_model->getAllkendaraan();
				$d['data_kendaraan'] = $this->app_model->manualQuery("SELECT * from t_kendaraan");
				$d['data_merek'] = $this->app_model->manualQuery("SELECT * from t_merek a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan limit ".$offset.",".$limit."");
				
				$this->load->view('kendaraan/v_list',$d);
			}
			else
			{
				$in['kode_kendaraan'] = $this->input->post('kode_kendaraan');
				$in['kendaraan'] = $this->input->post('kendaraan');
				$this->app_model->insertData("t_kendaraan",$in);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil ditambahkan.</div>");
				header('location:'.base_url().'kendaraan');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_edit()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('kendaraan', '*Nama kendaraan', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['kode_kendaraan'] = $this->input->post("kode_kendaraan");
			
			if ($this->form_validation->run() == FALSE)
			{
				$page=$this->uri->segment(3);
				$limit=10;
				if(!$page):
				$offset = 0;
				else:
				$offset = $page;
				endif;

				$d['title']='Kelola Kendaraan';
				$d['judul']='Kelola Kendaraan';
				$d['pengguna']='petugas1 Kredit CBN';
				$d['tot'] = $offset;
				$tot_hal = $this->app_model->getAllmerek();
				$config['base_url'] = base_url() . 'kendaraan/index/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
				$d["paginator"] =$this->pagination->create_links();

				$d['kode_kendaraan'] = "";
				$d['kendaraan'] = "";
				$d['kode_merek'] = "";
				$d['merek'] = "";
				$d['mst_kendaraan'] = $this->app_model->getAllkendaraan();
				$d['data_kendaraan'] = $this->app_model->manualQuery("SELECT * from t_kendaraan");
				$d['data_merek'] = $this->app_model->manualQuery("SELECT * from t_merek a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan limit ".$offset.",".$limit."");
				
				$this->load->view('kendaraan/v_list',$d);
			}
			else
			{
				$upd['kode_kendaraan'] = $this->input->post('kode_kendaraan');
				$upd['kendaraan'] = $this->input->post('kendaraan');
    			$this->app_model->updateData("t_kendaraan",$upd,$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil diubah.</div>");
    			header('location:'.base_url().'kendaraan');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function hapus()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$id['kode_kendaraan'] = $this->uri->segment(3);
			$kd_hps = $this->uri->segment(3);
			$hps_b = $this->app_model->manualQuery("SELECT * from t_merek WHERE kode_kendaraan='$kd_hps'");
			if($hps_b->num_rows()>0)
			{
				$this->session->set_flashdata("p", "<div class=\"alert alert-block\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data gagal dihapus, data berhubungan dengan data lain.</div>");
				header('location:'.base_url().'kendaraan');
			}
			else
			{
				$this->app_model->deleteData("t_kendaraan",$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil dihapus.</div>");
				header('location:'.base_url().'kendaraan');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
//merek
	public function simpan_merek()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('merek', '*Nama merek', 'trim|required|is_unique[t_merek.merek]');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['kode_merek'] = $this->input->post("kode_merek");
			
			if ($this->form_validation->run() == FALSE)
			{
				$page=$this->uri->segment(3);
				$limit=10;
				if(!$page):
				$offset = 0;
				else:
				$offset = $page;
				endif;

				$d['title']='Kelola Kendaraan';
				$d['judul']='Kelola Kendaraan';
				$d['pengguna']='petugas1 Kredit CBN';
				$d['tot'] = $offset;
				$tot_hal = $this->app_model->getAllmerek();
				$config['base_url'] = base_url() . 'kendaraan/index/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
				$d["paginator"] =$this->pagination->create_links();
				
				$d['kode_kendaraan'] = "";
				$d['kendaraan'] = "";
				$d['kode_merek'] = "";
				$d['merek'] = "";
				$d['mst_kendaraan'] = $this->app_model->getAllkendaraan();
				$d['data_kendaraan'] = $this->app_model->manualQuery("SELECT * from t_kendaraan");
				$d['data_merek'] = $this->app_model->manualQuery("SELECT * from t_merek a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan limit ".$offset.",".$limit."");
				
				$this->load->view('kendaraan/v_list',$d);
			}
			else
			{
				$in['kode_merek'] = $this->input->post('kode_merek');
				$in['merek'] = $this->input->post('merek');
				$in['kode_kendaraan'] = $this->input->post('kode_kendaraan');
				$this->app_model->insertData("t_merek",$in);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil ditambahkan.</div>");
				header('location:'.base_url().'kendaraan');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_edit_merek()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('merek', '*Nama merek', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['kode_merek'] = $this->input->post("kode_merek");
			
			if ($this->form_validation->run() == FALSE)
			{
				$page=$this->uri->segment(3);
				$limit=10;
				if(!$page):
				$offset = 0;
				else:
				$offset = $page;
				endif;

				$d['title']='Kelola Kendaraan';
				$d['judul']='Kelola Kendaraan';
				$d['pengguna']='petugas1 Kredit CBN';
				$d['tot'] = $offset;
				$tot_hal = $this->app_model->getAllmerek();
				$config['base_url'] = base_url() . 'kendaraan/index/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
				$d["paginator"] =$this->pagination->create_links();

				$d['kode_kendaraan'] = "";
				$d['kendaraan'] = "";
				$d['kode_merek'] = "";
				$d['merek'] = "";
				$d['mst_kendaraan'] = $this->app_model->getAllkendaraan();
				$d['data_kendaraan'] = $this->app_model->manualQuery("SELECT * from t_kendaraan");
				$d['data_merek'] = $this->app_model->manualQuery("SELECT * from t_merek a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan limit ".$offset.",".$limit."");
				
				$this->load->view('kendaraan/v_list',$d);
			}
			else
			{
				$upd['kode_merek'] = $this->input->post('kode_merek');
				$upd['merek'] = $this->input->post('merek');
				$upd['kode_kendaraan'] = $this->input->post('kode_kendaraan');
    			$this->app_model->updateData("t_merek",$upd,$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil diubah.</div>");
    			header('location:'.base_url().'kendaraan');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function hapus_merek()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$id['kode_merek'] = $this->uri->segment(3);
			$this->app_model->deleteData("t_merek",$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil dihapus.</div>");
			header('location:'.base_url().'kendaraan');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
}
