<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Negara extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$d['title']='Kelola Negara dan Kota';
			$d['judul']='Kelola Negara dan Kota';
			$d['pengguna']='Petugas 1 Kredit CBN';
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->getAllkota();
			$config['base_url'] = base_url() . 'negara/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$d['kode_negara'] = "";
			$d['negara'] = "";
			$d['kode_kota'] = "";
			$d['kota'] = "";
			$d['mst_negara'] = $this->app_model->getAllnegara();
			$d['data_negara'] = $this->app_model->manualQuery("SELECT * from t_negara");
			$d['data_kota'] = $this->app_model->manualQuery("SELECT * from t_kota a left join t_negara b on a.kode_negara=b.kode_negara limit ".$offset.",".$limit."");
			
			$this->load->view('negara/v_list',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function cari()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$d['title']='Kelola Negara dan Kota';
			$d['judul']='Kelola Negara dan Kota';
			$d['pengguna']='Petugas 1 Kredit CBN';
			
			$kata = $this->input->post("kode_negara");
			
			$page=$this->uri->segment(3);
			$limit=30;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->manualQuery("select * from t_kota a left join t_negara b on a.kode_negara=b.kode_negara where a.kode_negara='$kata'");
			$config['base_url'] = base_url() . 'negara/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();

			$d['kode_negara'] = "";
			$d['negara'] = "";
			$d['kode_kota'] = "";
			$d['kota'] = "";
			$d['mst_negara'] = $this->app_model->getAllnegara();
			$d['data_negara'] = $this->app_model->manualQuery("SELECT * from t_negara");
			$d['data_kota'] = $this->app_model->manualQuery("SELECT * from t_kota a left join t_negara b on a.kode_negara=b.kode_negara where a.kode_negara='$kata' limit ".$offset.",".$limit."");
			$this->load->view('negara/v_list',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

//negara
	public function simpan()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('negara', '*Nama negara', 'trim|required|is_unique[t_negara.negara]');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['kode_negara'] = $this->input->post("kode_negara");
			
			if ($this->form_validation->run() == FALSE)
			{
				$page=$this->uri->segment(3);
				$limit=10;
				if(!$page):
				$offset = 0;
				else:
				$offset = $page;
				endif;

				$d['title']='Kelola Negara dan Kota';
				$d['judul']='Kelola Negara dan Kota';
				$d['pengguna']='Petugas 1 Kredit CBN';
				$d['tot'] = $offset;
				$tot_hal = $this->app_model->getAllkota();
				$config['base_url'] = base_url() . 'negara/index/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
				$d["paginator"] =$this->pagination->create_links();
				
				$d['kode_negara'] = "";
				$d['negara'] = "";
				$d['kode_kota'] = "";
				$d['kota'] = "";
				$d['mst_negara'] = $this->app_model->getAllnegara();
				$d['data_negara'] = $this->app_model->manualQuery("SELECT * from t_negara");
				$d['data_kota'] = $this->app_model->manualQuery("SELECT * from t_kota a left join t_negara b on a.kode_negara=b.kode_negara limit ".$offset.",".$limit."");
				
				$this->load->view('negara/v_list',$d);
			}
			else
			{
				$in['kode_negara'] = $this->input->post('kode_negara');
				$in['negara'] = $this->input->post('negara');
				$this->app_model->insertData("t_negara",$in);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil ditambahkan.</div>");
				header('location:'.base_url().'negara');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_edit()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('negara', '*Nama negara', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['kode_negara'] = $this->input->post("kode_negara");
			
			if ($this->form_validation->run() == FALSE)
			{
				$page=$this->uri->segment(3);
				$limit=10;
				if(!$page):
				$offset = 0;
				else:
				$offset = $page;
				endif;

				$d['title']='Kelola Negara dan Kota';
				$d['judul']='Kelola Negara dan Kota';
				$d['pengguna']='petugas1 Kredit CBN';
				$d['tot'] = $offset;
				$tot_hal = $this->app_model->getAllkota();
				$config['base_url'] = base_url() . 'negara/index/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
				$d["paginator"] =$this->pagination->create_links();

				$d['kode_negara'] = "";
				$d['negara'] = "";
				$d['kode_kota'] = "";
				$d['kota'] = "";
				$d['mst_negara'] = $this->app_model->getAllnegara();
				$d['data_negara'] = $this->app_model->manualQuery("SELECT * from t_negara");
				$d['data_kota'] = $this->app_model->manualQuery("SELECT * from t_kota a left join t_negara b on a.kode_negara=b.kode_negara limit ".$offset.",".$limit."");
				
				$this->load->view('negara/v_list',$d);
			}
			else
			{
				$upd['kode_negara'] = $this->input->post('kode_negara');
				$upd['negara'] = $this->input->post('negara');
    			$this->app_model->updateData("t_negara",$upd,$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil diubah.</div>");
    			header('location:'.base_url().'negara');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function hapus()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$id['kode_negara'] = $this->uri->segment(3);
			$kd_hps = $this->uri->segment(3);
			$hps_b = $this->app_model->manualQuery("SELECT * from t_kota WHERE kode_negara='$kd_hps'");
			if($hps_b->num_rows()>0)
			{
				$this->session->set_flashdata("p", "<div class=\"alert alert-block\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data gagal dihapus, data berhubungan dengan data lain.</div>");
				header('location:'.base_url().'negara');
			}
			else
			{
				$this->app_model->deleteData("t_negara",$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil dihapus.</div>");
				header('location:'.base_url().'negara');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
//kota
	public function simpan_kota()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('kota', '*Nama kota', 'trim|required|is_unique[t_kota.kota]');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['kode_kota'] = $this->input->post("kode_kota");
			
			if ($this->form_validation->run() == FALSE)
			{
				$page=$this->uri->segment(3);
				$limit=10;
				if(!$page):
				$offset = 0;
				else:
				$offset = $page;
				endif;

				$d['title']='Kelola Negara dan Kota';
				$d['judul']='Kelola Negara dan Kota';
				$d['pengguna']='petugas1 Kredit CBN';
				$d['tot'] = $offset;
				$tot_hal = $this->app_model->getAllkota();
				$config['base_url'] = base_url() . 'negara/index/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
				$d["paginator"] =$this->pagination->create_links();
				
				$d['kode_negara'] = "";
				$d['negara'] = "";
				$d['kode_kota'] = "";
				$d['kota'] = "";
				$d['mst_negara'] = $this->app_model->getAllnegara();
				$d['data_negara'] = $this->app_model->manualQuery("SELECT * from t_negara");
				$d['data_kota'] = $this->app_model->manualQuery("SELECT * from t_kota a left join t_negara b on a.kode_negara=b.kode_negara limit ".$offset.",".$limit."");
				
				$this->load->view('negara/v_list',$d);
			}
			else
			{
				$in['kode_kota'] = $this->input->post('kode_kota');
				$in['kota'] = $this->input->post('kota');
				$in['kode_negara'] = $this->input->post('kode_negara');
				$this->app_model->insertData("t_kota",$in);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil ditambahkan.</div>");
				header('location:'.base_url().'negara');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_edit_kota()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$this->form_validation->set_rules('kota', '*Nama kota', 'trim|required');
			$this->form_validation->set_message('required', '%s Mohon Diisi.');
			$this->form_validation->set_message('is_unique', '%s Telah Tersedia.');

			$id['kode_kota'] = $this->input->post("kode_kota");
			
			if ($this->form_validation->run() == FALSE)
			{
				$page=$this->uri->segment(3);
				$limit=10;
				if(!$page):
				$offset = 0;
				else:
				$offset = $page;
				endif;

				$d['title']='Kelola Negara dan Kota';
				$d['judul']='Kelola Negara dan Kota';
				$d['pengguna']='petugas1 Kredit CBN';
				$d['tot'] = $offset;
				$tot_hal = $this->app_model->getAllkota();
				$config['base_url'] = base_url() . 'negara/index/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
				$d["paginator"] =$this->pagination->create_links();

				$d['kode_negara'] = "";
				$d['negara'] = "";
				$d['kode_kota'] = "";
				$d['kota'] = "";
				$d['mst_negara'] = $this->app_model->getAllnegara();
				$d['data_negara'] = $this->app_model->manualQuery("SELECT * from t_negara");
				$d['data_kota'] = $this->app_model->manualQuery("SELECT * from t_kota a left join t_negara b on a.kode_negara=b.kode_negara limit ".$offset.",".$limit."");
				
				$this->load->view('negara/v_list',$d);
			}
			else
			{
				$upd['kode_kota'] = $this->input->post('kode_kota');
				$upd['kota'] = $this->input->post('kota');
				$upd['kode_negara'] = $this->input->post('kode_negara');
    			$this->app_model->updateData("t_kota",$upd,$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil diubah.</div>");
    			header('location:'.base_url().'negara');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function hapus_kota()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$id['kode_kota'] = $this->uri->segment(3);
			$this->app_model->deleteData("t_kota",$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil dihapus.</div>");
			header('location:'.base_url().'negara');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
}
