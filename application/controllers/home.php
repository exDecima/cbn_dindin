<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="customer")
		{
			$d['title']='Beranda';
			$d['judul']='Beranda';
			$d['pengguna']='Customer Kredit CBN';
			$this->load->view('customer/v_home',$d);
		}
		else if ($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$d['title']='Beranda';
			$d['judul']='Beranda';
			$d['pengguna']='Petugas 1 CBN';
			$this->load->view('petugas1/v_home',$d);
		}
		else if ($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas2")
		{
			$d['title']='Beranda';
			$d['judul']='Beranda';
			$d['pengguna']='Petugas 2 CBN';
			$this->load->view('petugas2/v_home',$d);
		}
		else if ($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$d['title']='Beranda';
			$d['judul']='Beranda';
			$d['pengguna']='Manager CBN';
			$this->load->view('manager/v_home',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
}
