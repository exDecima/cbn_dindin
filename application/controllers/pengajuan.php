<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengajuan extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="customer")
		{
			$page=$this->uri->segment(3);
			$limit=$this->config->item('limit_data');
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$d['title']='Transaksi Pengajuan Kredit Kendaraan';
			$d['judul']='Transaksi Pengajuan Kredit Kendaraan';
			$d['pengguna'] = 'Customer CBN - '.$this->session->userdata('nama_lengkap');
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->getAllPengajuan();
			$config['base_url'] = base_url() . 'pengajuan/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();

			$ud = $this->session->userdata('id_user');
			
			$d['data_pengajuan'] = $this->app_model->manualQuery("SELECT * from t_pengajuan a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan left join t_merek c on a.kode_merek=c.kode_merek left join t_user d on a.id_user=d.id_user WHERE a.id_user = '$ud' ORDER BY a.tgl_pengajuan DESC limit ".$offset.",".$limit."");
			
			$this->load->view('pengajuan/v_list',$d);
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$page=$this->uri->segment(3);
			$limit=$this->config->item('limit_data');
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$d['title']='Verifikasi Pengajuan Kredit Kendaraan';
			$d['judul']='Verifikasi Pengajuan Kredit Kendaraan Tahap 1';
			$d['pengguna'] = 'Petugas 1 CBN ';
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->getAllPengajuan();
			$config['base_url'] = base_url() . 'pengajuan/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'pengajuannya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$d['data_verifikasi'] = $this->app_model->manualQuery("SELECT * from t_pengajuan a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan left join t_merek c on a.kode_merek=c.kode_merek left join t_user d on a.id_user=d.id_user ORDER BY stts_petugas1 DESC, kode_pengajuan ASC limit ".$offset.",".$limit."");

			$this->load->view('pengajuan/v_list_verifikasi',$d);
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas2")
		{
			$page=$this->uri->segment(3);
			$limit=$this->config->item('limit_data');
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$d['title']='Verifikasi Pengajuan Kredit Kendaraan';
			$d['judul']='Verifikasi Pengajuan Kredit Kendaraan Tahap 1';
			$d['pengguna'] = 'Petugas 2 CBN ';
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->getAllPengajuan();
			$config['base_url'] = base_url() . 'pengajuan/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'pengajuannya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$d['data_verifikasi'] = $this->app_model->manualQuery("SELECT * from t_pengajuan a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan left join t_merek c on a.kode_merek=c.kode_merek left join t_user d on a.id_user=d.id_user WHERE stts_petugas1='Terima' ORDER BY stts_petugas2 DESC, kode_pengajuan ASC limit ".$offset.",".$limit."");

			$this->load->view('pengajuan/v_list_verifikasi',$d);
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$page=$this->uri->segment(3);
			$limit=$this->config->item('limit_data');
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$d['title']='Verifikasi Pengajuan Kredit Kendaraan';
			$d['judul']='Verifikasi Pengajuan Kredit Kendaraan Tahap 1';
			$d['pengguna'] = 'Manager CBN ';
			$d['tot'] = $offset;
			$tot_hal = $this->app_model->getAllPengajuan();
			$config['base_url'] = base_url() . 'pengajuan/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'pengajuannya';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$d['data_verifikasi'] = $this->app_model->manualQuery("SELECT * from t_pengajuan a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan left join t_merek c on a.kode_merek=c.kode_merek left join t_user d on a.id_user=d.id_user WHERE stts_petugas2='Terima' ORDER BY stts_manager DESC, kode_pengajuan ASC limit ".$offset.",".$limit."");

			$this->load->view('pengajuan/v_list_verifikasi',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function tambah()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="customer")
		{
			$d['title']='Tambah Transaksi Pengajuan Kredit Kendaraan';
			$d['judul']='Tambah Transaksi Pengajuan Kredit Kendaraan';
			$d['pengguna'] = 'Customer CBN - '.$this->session->userdata('nama_lengkap');

			$d['kode_pengajuan'] = "";
			$d['kd_pengajuan'] = $this->app_model->getMaxKodePengajuan();
			$d['mst_merek'] = $this->app_model->getAllMerek();
			$d['kendaraan']=$this->app_model->get_kendaraan();	
			$this->load->view('pengajuan/v_input',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function get_merek()
	{
		$data['merek']=$this->app_model->get_merek($this->uri->segment(3));
		$this->load->view('pengajuan/ajax_get_merek',$data);
	}

	public function simpan()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="customer")
		{
			$in['kd_pengajuan'] = $this->input->post('kd_pengajuan');
			$in['tgl_pengajuan'] = date('Y-m-d H:i:s');
			$in['kode_kendaraan'] = $this->input->post('kode_kendaraan');
			$in['kode_merek'] = $this->input->post('kode_merek');
			$in['id_user'] = $this->session->userdata('id_user');
			$in['stts_petugas1'] = "Tunggu";
			$in['stts_petugas2'] = "Tunggu";
			$in['stts_manager'] = "Tunggu";
			
			$this->app_model->insertData("t_pengajuan",$in);

			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Pengajuan berhasil dipengajuan.</div>");

			header('location:'.base_url().'pengajuan');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function edit()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="customer")
		{
			$ksedt = $this->uri->segment(3);
			$sedt = $this->app_model->manualQuery("SELECT * from t_pengajuan WHERE (stts_petugas1!='Tolak' and stts_petugas2!='Tolak' and stts_manager!='Tolak') and kode_pengajuan='$ksedt'");
			if($sedt->num_rows()>0)
			{
				$this->session->set_flashdata("p", "<div class=\"alert alert-block\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Mohon maaf data sudah diproses, data tidak dapat diubah.</div>");
				header('location:'.base_url().'pengajuan');
			}
			else
			{
				$d['title']='Edit Transaksi Pengajuan Kredit Kendaraan';
				$d['judul']='Edit Transaksi Pengajuan Kredit Kendaraan';
				$d['pengguna'] = 'Customer CBN - '.$this->session->userdata('nama_lengkap');

				$id['kode_pengajuan'] = $this->uri->segment(3);
				$data_pengajuan = $this->app_model->getSelectedData("t_pengajuan",$id);	

				if($data_pengajuan->num_rows()>0)
				{
					$q = $this->app_model->getSelectedData("t_pengajuan",$id);
					foreach($q->result() as $data)
					{
						$d['kode_pengajuan'] = $data->kode_pengajuan;
						$d['kd_pengajuan'] = $data->kd_pengajuan;
						$d['kode_kendaraan'] = $data->kode_kendaraan;
						$d['kode_merek'] = $data->kode_merek;
						$d['id_user'] = $data->id_user;
					}
					$d['mst_merek'] = $this->app_model->getAllMerek();
					$d['kendaraan']=$this->app_model->get_kendaraan();
					$this->load->view('pengajuan/v_edit',$d);
				}
				else
				{
					header('location:'.base_url().'pengajuan');
				}
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_edit()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="customer")
		{
			$upd['kd_pengajuan'] = $this->input->post('kd_pengajuan');
			$upd['tgl_pengajuan'] = date('Y-m-d H:i:s');
			$upd['kode_kendaraan'] = $this->input->post('kode_kendaraan');
			$upd['kode_merek'] = $this->input->post('kode_merek');
			$upd['id_user'] = $this->session->userdata('id_user');
			$upd['stts_petugas1'] = "Tunggu";
			$upd['stts_petugas2'] = "Tunggu";
			$upd['stts_manager'] = "Tunggu";
			
			$this->app_model->updateData("t_pengajuan",$upd,$id);
    		header('location:'.base_url().'pengajuan');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function detail()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="customer")
		{
			$d['title']='Detail Transaksi Pengajuan Kredit Kendaraan';
			$d['judul']='Detail Transaksi Pengajuan Kredit Kendaraan';
			$d['pengguna'] = 'Customer CBN - '.$this->session->userdata('nama_lengkap');

			$id = $this->uri->segment(3);
			$data_pengajuan = $this->app_model->manualQuery("SELECT * from t_pengajuan a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan left join t_merek c on a.kode_merek=c.kode_merek left join t_user d on a.id_user=d.id_user WHERE a.kode_pengajuan='$id'");	
			foreach($data_pengajuan->result() as $data)
			{
				$d['kode_pengajuan'] = $data->kode_pengajuan;
				$d['kd_pengajuan'] = $data->kd_pengajuan;
				$d['kode_kendaraan'] = $data->kode_kendaraan;
				$d['kode_merek'] = $data->kode_merek;
				$d['id_user'] = $data->id_user;
				$d['kendaraan'] = $data->kendaraan;
				$d['merek'] = $data->merek;
			}
			$this->load->view('pengajuan/v_detail',$d);
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')!="customer")
		{
			$d['title']='Detail Transaksi Pengajuan Kredit Kendaraan';
			$d['judul']='Detail Transaksi Pengajuan Kredit Kendaraan';
			if($this->session->userdata('level')=="petugas1")
			{
				$d['pengguna']='Petugas 1 Kredit CBN';
			}
			else if($this->session->userdata('level')=="petugas2")
			{
				$d['pengguna']='Petugas 2 Kredit CBN';
			}
			else if($this->session->userdata('level')=="manager")
			{
				$d['pengguna']='Manager Kredit CBN';
			}
			$id = $this->uri->segment(3);
			$data_pengajuan = $this->app_model->manualQuery("SELECT * from t_pengajuan a left join t_kendaraan b on a.kode_kendaraan=b.kode_kendaraan left join t_merek c on a.kode_merek=c.kode_merek left join t_user d on a.id_user=d.id_user left join t_negara e on d.kode_negara=e.kode_negara left join t_kota f on d.kode_kota=f.kode_kota WHERE a.kode_pengajuan='$id'");	
			foreach($data_pengajuan->result() as $data)
			{
				$d['kode_pengajuan'] = $data->kode_pengajuan;
				$d['kd_pengajuan'] = $data->kd_pengajuan;
				$d['kode_kendaraan'] = $data->kode_kendaraan;
				$d['kode_merek'] = $data->kode_merek;
				$d['id_user'] = $data->id_user;
				$d['kendaraan'] = $data->kendaraan;
				$d['merek'] = $data->merek;
				$d['kota'] = $data->kota;
				$d['negara'] = $data->negara;
				$d['tgl_pengajuan'] = $data->kendaraan;
				$d['nama_lengkap'] = $data->nama_lengkap;
				$d['penghasilan'] = $data->penghasilan;
				$d['email'] = $data->email;
			}
			$this->load->view('pengajuan/v_detail_verifikasi',$d);
		}
		else
		{
			header('location:'.base_url().'');
		}
	}
	
	public function simpan_tolak()
	{
		$id['kode_pengajuan'] = $this->input->post('kode_pengajuan');
		$xd = $this->input->post('kode_pengajuan');
		$data_pengajuan = $this->app_model->manualQuery("SELECT * from t_pengajuan a left join t_user d on a.id_user=d.id_user WHERE a.kode_pengajuan='$xd'");	
		foreach($data_pengajuan->result() as $data)
		{
			$datamail['email'] = $data->email;
		}
		$emailto = $datamail['email'];
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$upd['stts_petugas1'] = "Tolak";
			$upd['alasan_petugas1'] = $this->input->post('alasan');
			$upd['tgl_petugas1'] = date('Y-m-d H:i:s');
			$upd['petugas1'] = $this->session->userdata('nama_lengkap');
   				
    		$this->app_model->updateData("t_pengajuan",$upd,$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Verifikasi berhasil, pengajuan ditolak.</div>");
    		header('location:'.base_url().'pengajuan');
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas2")
		{
			$upd['stts_petugas2'] = "Tolak";
			$upd['alasan_petugas2'] = $this->input->post('alasan');
			$upd['tgl_petugas2'] = date('Y-m-d H:i:s');
			$upd['petugas2'] = $this->session->userdata('nama_lengkap');
   				
    		$this->app_model->updateData("t_pengajuan",$upd,$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Verifikasi berhasil, pengajuan ditolak.</div>");
    		header('location:'.base_url().'pengajuan');
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$upd['stts_manager'] = "Tolak";
			$upd['alasan_manager'] = $this->input->post('alasan');
			$upd['tgl_manager'] = date('Y-m-d H:i:s');
			$upd['manager'] = $this->session->userdata('nama_lengkap');
			$manager = $this->session->userdata('email');
			$als = $this->input->post('alasan');
   			
	        $this->load->config('email');
	        $conf = $this->config->item('conf','email');
	        $this->load->library('email',$conf);
	        
	        $this->email->from($manager, 'Manager CBN');
	        $this->email->to($emailto);
	        $this->email->subject('Pengajuan Kredit ditolak');
	        $this->email->message($als);
	        $this->email->send();
	        
	        if (!$this->email->send()){
	            show_error($this->email->print_debugger());
	        }

	        $this->app_model->updateData("t_pengajuan",$upd,$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Verifikasi berhasil, pengajuan ditolak.</div>");

    		header('location:'.base_url().'pengajuan');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function simpan_setuju()
	{
		$xd = $this->input->post('kode_pengajuan');
		$data_pengajuan = $this->app_model->manualQuery("SELECT * from t_pengajuan a left join t_user d on a.id_user=d.id_user WHERE a.kode_pengajuan='$xd'");	
		foreach($data_pengajuan->result() as $data)
		{
			$emailto = $data->email;
		}
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas1")
		{
			$id['kode_pengajuan'] = $this->uri->segment(3);
			$upd['tgl_petugas1'] = date('Y-m-d H:i:s');
			$upd['stts_petugas1'] = "Terima";
			$upd['petugas1'] = $this->session->userdata('nama_lengkap');
			$this->app_model->updateData("t_pengajuan",$upd,$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Verifikasi berhasil, data diterima.</div>");
			header('location:'.base_url().'pengajuan');
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="petugas2")
		{
			$id['kode_pengajuan'] = $this->uri->segment(3);
			$upd['tgl_petugas2'] = date('Y-m-d H:i:s');
			$upd['stts_petugas2'] = "Terima";
			$upd['petugas2'] = $this->session->userdata('nama_lengkap');
			$this->app_model->updateData("t_pengajuan",$upd,$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Verifikasi berhasil, data diterima.</div>");
			header('location:'.base_url().'pengajuan');
		}
		else if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="manager")
		{
			$id['kode_pengajuan'] = $this->uri->segment(3);
			$upd['tgl_manager'] = date('Y-m-d H:i:s');
			$upd['stts_manager'] = "Terima";
			$upd['manager'] = $this->session->userdata('nama_lengkap');

			$manager = $this->session->userdata('email');
   			
	        $this->load->config('email');
	        $conf = $this->config->item('conf','email');
	        $this->load->library('email',$conf);
	        
	        $this->email->from($manager, 'Manager CBN');
	        $this->email->to($emailto);
	        $this->email->subject('Pengajuan Kredit disetujui');
	        $this->email->message('Pengajuan Kredit disetujui');
	        $this->email->send();
	        
	        if (!$this->email->send()){
	            show_error($this->email->print_debugger());
	        }

			$this->app_model->updateData("t_pengajuan",$upd,$id);
			$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Verifikasi berhasil, data diterima.</div>");
			header('location:'.base_url().'pengajuan');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	
	public function hapus()
	{
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="customer")
		{
			$id['kode_pengajuan'] = $this->uri->segment(3);
			$ks = $this->uri->segment(3);
			$hps = $this->app_model->manualQuery("SELECT * from t_pengajuan WHERE kode_pengajuan='$ks' and (stts_petugas1='Terima' or stts_petugas1='Tunggu')");
			if($hps->num_rows()>0)
			{
				$this->session->set_flashdata("p", "<div class=\"alert alert-block\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data gagal dihapus, data berhubungan dengan data lain.</div>");
				header('location:'.base_url().'pengajuan');
			}
			else
			{
				$this->app_model->deleteData("t_pengajuan",$id);
				$this->session->set_flashdata("p", "<div class=\"alert alert-success\" id=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>Data berhasil dihapus.</div>");
				header('location:'.base_url().'pengajuan');
			}
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

	public function back()
	{
		if($this->session->userdata('logged_in')!="")
		{
			header('location:'.base_url().'pengajuan');
		}
		else
		{
			header('location:'.base_url().'');
		}
	}

}