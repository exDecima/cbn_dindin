<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    var $t_grup='t_grup';
    var $t_negara='t_negara';
    var $t_kota='t_kota';
    var $t_kendaraan='t_kendaraan';
    var $t_merek='t_merek';

    public function getMaxKodePengajuan()
    {
        $q = $this->db->query("select MAX(RIGHT(kd_pengajuan,8)) as kd_max from t_pengajuan");
        $kd = "";
        if($q->num_rows()>0)
        {
            foreach($q->result() as $k)
            {
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%08s", $tmp);
            }
        }
        else
        {
            $kd = "00000001";
        }   
        return "PK".$kd;
    }


    public function get_grup() {
    $sql_grup=$this->db->get($this->t_grup);    
    if($sql_grup->num_rows()>0){
        foreach ($sql_grup->result_array() as $row)
            {
                $result['-']= 'Pilih Grup';
                $result[$row['kode_grup']]= ucwords(strtolower($row['grup']));
            }
            return $result;
        }
    }

    public function get_negara() {
    $this->db->order_by('negara','asc');
    $sql_negara=$this->db->get($this->t_negara);    
    if($sql_negara->num_rows()>0){
        foreach ($sql_negara->result_array() as $row)
            {
                $result['-']= 'Pilih Negara';
                $result[$row['kode_negara']]= ucwords(strtolower($row['negara']));
            }
            return $result;
        }
    }
    
    public function get_kota($kode_negara){
    $this->db->where('kode_negara',$kode_negara);
    $this->db->order_by('kota','asc');
    $sql_kota=$this->db->get($this->t_kota);
    if($sql_kota->num_rows()>0){

        foreach ($sql_kota->result_array() as $row)
        {
            $result[$row['kode_kota']]= ucwords(strtolower($row['kota']));
        }
        } else {
           $result['-']= 'Pilih Kota';
        }
        return $result;
    }

    public function get_kendaraan() {
    $this->db->order_by('kendaraan','asc');
    $sql_kendaraan=$this->db->get($this->t_kendaraan);    
    if($sql_kendaraan->num_rows()>0){
        foreach ($sql_kendaraan->result_array() as $row)
            {
                $result['-']= 'Pilih Jenis Kendaraan';
                $result[$row['kode_kendaraan']]= ucwords(strtolower($row['kendaraan']));
            }
            return $result;
        }
    }
    
    public function get_merek($kode_kendaraan){
    $this->db->where('kode_kendaraan',$kode_kendaraan);
    $this->db->order_by('merek','asc');
    $sql_merek=$this->db->get($this->t_merek);
    if($sql_merek->num_rows()>0){

        foreach ($sql_merek->result_array() as $row)
        {
            $result[$row['kode_merek']]= ucwords(strtolower($row['merek']));
        }
        } else {
           $result['-']= 'Pilih Merek';
        }
        return $result;
    }

    function getAllPengajuan(){
        return $this->db->get("t_pengajuan");
    }
    function getAllGrup(){
        return $this->db->get("t_grup");
    }
    function getAllUser(){
        return $this->db->get("t_user");
    }
    function getAllNegara(){
        return $this->db->get("t_negara");
    }
    function getAllKota(){
        return $this->db->get("t_kota");
    }
    function getAllKendaraan(){
        return $this->db->get("t_kendaraan");
    }
    function getAllMerek(){
        return $this->db->get("t_merek");
    }
    public function getAllData($table)
    {
        return $this->db->get($table);
    }

    public function getAllDataLimited($table,$limit,$offset)
    {
        return $this->db->get($table, $limit, $offset);
    }

    public function getSelectedDataLimited($table,$data,$limit,$offset)
    {
        return $this->db->get_where($table, $data, $limit, $offset);
    }

    public function getSelectedData($table,$data)
    {
        return $this->db->get_where($table, $data);
    }

    function updateData($table,$data,$field_key)
    {
        $this->db->update($table,$data,$field_key);
    }

    function deleteData($table,$data)
    {
        $this->db->delete($table,$data);
    }

    function insertData($table,$data)
    {
        $this->db->insert($table,$data);
    }

    function manualQuery($q)
    {
        return $this->db->query($q);
    }
}