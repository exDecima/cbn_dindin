<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_customer_model extends CI_Model {

	public function getLoginData($data)
	{
		$login['username'] = $data['username'];
		$login['password'] = md5($data['password']);
		$login['kode_grup'] = $data['kode_grup'];
		$cek = $this->db->get_where('t_user', $login);
		if($cek->num_rows()>0)
		{
			foreach($cek->result() as $qad)
			{
				$sess_data['logged_in'] = 'cbnDindin';
				$sess_data['id_user'] = $qad->id_user;
				$sess_data['username'] = $qad->username;
				$sess_data['nama_lengkap'] = $qad->nama_lengkap;
				$sess_data['level'] = $qad->level;
				$sess_data['kode_grup'] = $qad->kode_grup;
				$sess_data['foto'] = $qad->foto;
				$sess_data['stts'] = $qad->stts;
				$sess_data['status'] = $qad->status;
				$sess_data['kode_negara'] = $qad->kode_negara;
				$sess_data['kode_kota'] = $qad->kode_kota;
				$sess_data['penghasilan'] = $qad->penghasilan;
				$sess_data['email'] = $qad->email;
				if($sess_data['stts']==1){
					$this->session->set_userdata($sess_data);
				}
				else{
					$this->session->set_flashdata('result_login', "*Akun Tidak Aktif.");
				}
				
			}
			$id['id_user']=$this->session->userdata('id_user');
			$upd['status']=1;
			$this->app_model->updateData("t_user",$upd,$id);
			header('location:'.base_url().'');
		}
		else
		{
			$this->session->set_flashdata('result_login', "*Username dan Password Salah.");
			header('location:'.base_url().'');
		}
	}
}
