<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_petugas_model extends CI_Model {

	var $t_grup='t_grup';

	public function get_grup() {
	$sql_grup=$this->db->query("SELECT * from t_grup WHERE kode_grup!=1");	
	if($sql_grup->num_rows()>0){
		foreach ($sql_grup->result_array() as $row)
			{
				$result['-']= 'Pilih Grup';
				$result[$row['kode_grup']]= ucwords(strtolower($row['grup']));
			}
			return $result;
		}
	}
	
	public function getLoginData($data)
	{
		$login['username'] = $data['username'];
		$login['password'] = md5($data['password']);
		$login['kode_grup'] = $data['kode_grup'];
		$cek = $this->db->get_where('t_user', $login);
		if($cek->num_rows()>0)
		{
			foreach($cek->result() as $qad)
			{
				$sess_data['logged_in'] = 'yesGetMeLoginBaby';
				$sess_data['id_user'] = $qad->id_user_login;
				$sess_data['username'] = $qad->username;
				$sess_data['nama_lengkap'] = $qad->nama_lengkap;
				$sess_data['level'] = $qad->level;
				$sess_data['kode_grup'] = $qad->kode_grup;
				$sess_data['stts'] = $qad->stts;
				$sess_data['kode_negara'] = $qad->kode_negara;
				$sess_data['kode_kota'] = $qad->kode_kota;
				$sess_data['penghasilan'] = $qad->penghasilan;
				$sess_data['email'] = $qad->email;
				if($sess_data['stts']==1){
					$this->session->set_userdata($sess_data);
				}
				else{
					$this->session->set_flashdata('result_login', "*Akun Tidak Aktif.");
				}
			}
			header('location:'.base_url().'app/login_petugas');
		}
		else
		{
			$this->session->set_flashdata('result_login', "*Data Login Salah.");
			header('location:'.base_url().'app/login_petugas');
		}
	}
}
