#
# TABLE STRUCTURE FOR: t_background
#

DROP TABLE IF EXISTS t_background;

CREATE TABLE `t_background` (
  `kode_bg` int(6) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(100) NOT NULL,
  `warna` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_bg`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO t_background (`kode_bg`, `gambar`, `warna`) VALUES (1, '177569274-20150806-071331.jpg', '#000000');


#
# TABLE STRUCTURE FOR: t_barang
#

DROP TABLE IF EXISTS t_barang;

CREATE TABLE `t_barang` (
  `kode_barang` int(6) NOT NULL AUTO_INCREMENT,
  `no_reg` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `gol` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `ket` varchar(30) NOT NULL,
  `qty` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `kd_brg` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (1, 'BB1/A/10', 'coba', 'Berbahaya', 'Kertas', 2, '820951805-20151212-155629.jpg', 'rupbasan', 2, 'dindin', 'PS00000001');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (2, 'BB1/a/11', 'afafa', 'Berharga', 'Elektronik', 3, '', 'rupbasan', 3, 'dindin', 'PS00000002');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (3, 'BB2/afa/af', 'fakjgbjgwhg', 'Umum Terbuka', 'Kertas', 4, '', 'rupbasan', 4, 'dindin', 'PS00000003');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (4, 'BB3/afa/11', 'aafafa', 'Berharga', 'Kertas', 3, '', 'rupbasan', 3, 'dindin', 'PS00000004');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (5, 'BB3/qggw/11 E', 'aafagagg', 'Berbahaya', 'Kertas', 7, '', 'rupbasan', 7, 'dindin', 'PS00000005');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (6, 'BB3/afafafafaa/11 E', 'aaafa', 'Umum Tertutup', 'Kertas', 8, '', 'rupbasan', 8, 'dindin', 'PS00000006');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (7, 'BB5/WFW/11 E', 'aaa', 'Berharga', 'Kertas', 4, '', 'rupbasan', 4, 'dindin', 'PS00000007');


#
# TABLE STRUCTURE FOR: t_barang_keluar
#

DROP TABLE IF EXISTS t_barang_keluar;

CREATE TABLE `t_barang_keluar` (
  `kode_barang_keluar` int(6) NOT NULL AUTO_INCREMENT,
  `no_reg` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `gol` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `qty` int(5) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `kode_status` int(6) NOT NULL,
  `ket` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `kd_brg` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_barang_keluar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_berita
#

DROP TABLE IF EXISTS t_berita;

CREATE TABLE `t_berita` (
  `kode_berita` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(500) NOT NULL,
  `subjudul` varchar(1000) NOT NULL,
  `isi` varchar(10000) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `tgl_berita` datetime NOT NULL,
  PRIMARY KEY (`kode_berita`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO t_berita (`kode_berita`, `judul`, `subjudul`, `isi`, `foto`, `nama_lengkap`, `status`, `tgl_berita`) VALUES (3, 'Called it Love', 'Love is Our Grace, Love is Our Downfall', '<p style=\"text-align:justify\">Apabila engkau mendamba seseorang yang berbudi tanpa cela, mungkinkah kiranya gaharu menebarkan wanginya tanpa asap?&nbsp;</p>\r\n\r\n<p>Hunger, a poet once said,&nbsp;is the most important thing we know,&nbsp;the first lesson we learn.&nbsp;But hunger can be easily&nbsp;quieted down, easily satiated.&nbsp;There is another force, a&nbsp;different type of hunger,&nbsp;an&nbsp;unquenchable thirst&nbsp;that cannot be extinguished.&nbsp;It&#39;s very existence is what&nbsp;defines us, what makes us human.&nbsp;That force is love.</p>\r\n\r\n<blockquote>\r\n<p style=\"text-align:justify\">Love is our grace.&nbsp;Love is our downfall.</p>\r\n</blockquote>\r\n\r\n<p><img alt=\"\" src=\"/fileckeditor/lee-ji-eun-love-is-red-jejak-hati-162133(1).jpg\" style=\"float:right; height:200px; width:320px\" />Love, you see, is the&nbsp;one force that cannot be explained&nbsp;cannot be broken down&nbsp;into a chemical process.&nbsp;It is the beacon that guides us back home&nbsp;when no one is there&nbsp;and the light that illuminates our loss.&nbsp;Its absence robs us of all pleasure&nbsp;of our capacity for joy.&nbsp;It makes our nights darker&nbsp;and our days gloomier.&nbsp;But when we&nbsp;find love&nbsp;no matter how wrong,&nbsp;how sad or how terrible&nbsp;we cling to it.&nbsp;It gives us our strength.&nbsp;It holds us upright.&nbsp;It feeds on us and we feed on it.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>', '834939188-20151205-172438.jpg', 'Dindin Zaenudin', 1, '2015-12-02 14:28:24');
INSERT INTO t_berita (`kode_berita`, `judul`, `subjudul`, `isi`, `foto`, `nama_lengkap`, `status`, `tgl_berita`) VALUES (4, 'Berita Membahagiakan Akhir Tahun Ini', 'Alhamdulillah berhasil lolos test wawancara', '<p>Akhirnya saya diterima bekerja di PT WINGS Corp dengan gaji yang cukup besar, dan posisi yang menjanjikan.</p>', '299522932-20151205-172540.jpg', 'Dindin Zaenudin', 1, '2015-12-01 17:23:36');
INSERT INTO t_berita (`kode_berita`, `judul`, `subjudul`, `isi`, `foto`, `nama_lengkap`, `status`, `tgl_berita`) VALUES (5, 'Make it Different', 'I can change this world, beautifully', '<p>this world is very wonderfull, amazing, icikiwis, wanti-wanti, on the dance flour, tepung atuh eta mah.</p>', '733057611-20151206-165615.jpg', 'Dindin Zaenudin', 1, '2015-12-06 16:36:57');


#
# TABLE STRUCTURE FOR: t_chat
#

DROP TABLE IF EXISTS t_chat;

CREATE TABLE `t_chat` (
  `kode_chat` int(10) NOT NULL AUTO_INCREMENT,
  `chat` varchar(500) NOT NULL,
  `id_user` int(6) NOT NULL,
  `tgl_chat` datetime NOT NULL,
  PRIMARY KEY (`kode_chat`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO t_chat (`kode_chat`, `chat`, `id_user`, `tgl_chat`) VALUES (1, 'hallo all', 2, '2015-12-14 09:24:17');
INSERT INTO t_chat (`kode_chat`, `chat`, `id_user`, `tgl_chat`) VALUES (2, 'hallo juga', 3, '2015-12-14 09:26:16');
INSERT INTO t_chat (`kode_chat`, `chat`, `id_user`, `tgl_chat`) VALUES (3, 'apa kabar nih', 3, '2015-12-14 15:55:57');
INSERT INTO t_chat (`kode_chat`, `chat`, `id_user`, `tgl_chat`) VALUES (4, 'alhamdulillah baik, kamu gimana?', 2, '2015-12-14 16:00:28');


#
# TABLE STRUCTURE FOR: t_galeri
#

DROP TABLE IF EXISTS t_galeri;

CREATE TABLE `t_galeri` (
  `kode_galeri` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(500) NOT NULL,
  `isi` varchar(10000) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `status` int(2) NOT NULL,
  `kode_kategori` int(10) NOT NULL,
  PRIMARY KEY (`kode_galeri`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO t_galeri (`kode_galeri`, `judul`, `isi`, `foto`, `status`, `kode_kategori`) VALUES (1, 'Kegiatan Rutin Saya', '<p>Berdansa, mungkin itulah kegiatan rutin yang biasa dilakukan saya setiap pagi menjelang petang. Tak ada yang dapat menggantikan nikmatnya kentut pagi hari, yang durutdut-durutdut berkali-kali, hingga akhirnya diakhiri dengan pret kecil yang melepus menebarkan wewangian yang sangat-sangat bau busuk.</p>\r\n\r\n<blockquote>\r\n<p>kentut tud tudiya padem.</p>\r\n</blockquote>\r\n\r\n<p>mohon untuk tidak ditiru. Berbahaya!!!</p>\r\n\r\n<p>Berdansa, mungkin itulah kegiatan rutin yang biasa dilakukan saya setiap pagi menjelang petang. Tak ada yang dapat menggantikan nikmatnya kentut pagi hari, yang durutdut-durutdut berkali-kali, hingga akhirnya diakhiri dengan pret kecil yang melepus menebarkan wewangian yang sangat-sangat bau busuk. Berdansa, mungkin itulah kegiatan rutin yang biasa dilakukan saya setiap pagi menjelang petang. Tak ada yang dapat menggantikan nikmatnya kentut pagi hari, yang durutdut-durutdut berkali-kali, hingga akhirnya diakhiri dengan pret kecil yang melepus menebarkan wewangian yang sangat-sangat bau busuk.&nbsp;Berdansa, mungkin itulah kegiatan rutin yang biasa dilakukan saya setiap pagi menjelang petang. Tak ada yang dapat menggantikan nikmatnya kentut pagi hari, yang durutdut-durutdut berkali-kali, hingga akhirnya diakhiri dengan pret kecil yang melepus menebarkan wewangian yang sangat-sangat bau busuk.&nbsp;Mohon untuk tidak ditiru. Berbahaya!!!</p>', '508406136-20151209-151029.jpg', 1, 2);
INSERT INTO t_galeri (`kode_galeri`, `judul`, `isi`, `foto`, `status`, `kode_kategori`) VALUES (2, 'Kegiatan Percobaan', '<p>Perjalanan hidup memang melelahkan, tetapi jika semua diniatkan karena Alloh, insya Alloh akan membuahkan hasil yang menakjubkan.</p>\r\n\r\n<p>Percayalah, itulah&nbsp;yang terbaik bagi kita semua.</p>', '272624118-20151209-151057.png', 1, 4);


#
# TABLE STRUCTURE FOR: t_gol
#

DROP TABLE IF EXISTS t_gol;

CREATE TABLE `t_gol` (
  `kode_gol` int(6) NOT NULL AUTO_INCREMENT,
  `gol` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_gol`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (1, 'Umum Terbuka');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (2, 'Umum Tertutup');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (3, 'Berharga');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (4, 'Berbahaya');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (5, 'Hewan');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (6, 'Tanaman');


#
# TABLE STRUCTURE FOR: t_grup
#

DROP TABLE IF EXISTS t_grup;

CREATE TABLE `t_grup` (
  `kode_grup` int(6) NOT NULL AUTO_INCREMENT,
  `grup` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_grup`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO t_grup (`kode_grup`, `grup`) VALUES (1, 'Rupbasan');
INSERT INTO t_grup (`kode_grup`, `grup`) VALUES (2, 'Pengadilan');
INSERT INTO t_grup (`kode_grup`, `grup`) VALUES (3, 'Kejaksaan');
INSERT INTO t_grup (`kode_grup`, `grup`) VALUES (4, 'Kepolisian');


#
# TABLE STRUCTURE FOR: t_instansi
#

DROP TABLE IF EXISTS t_instansi;

CREATE TABLE `t_instansi` (
  `kode_instansi` int(6) NOT NULL AUTO_INCREMENT,
  `instansi` varchar(100) NOT NULL,
  `kode_grup` int(11) NOT NULL,
  PRIMARY KEY (`kode_instansi`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (1, 'RUPBASAN', 1);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (2, 'Pengadilan Negeri Banyumas', 2);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (3, 'Pengadilan Negeri Purwokerto', 2);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (4, 'Kejaksaan Negeri Banyumas', 3);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (5, 'Kejaksaan Negeri Purwokerto', 3);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (6, 'Polres Banyumas', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (7, 'Polsek Banyumas', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (8, 'Polsek Baturraden', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (9, 'Polsek Cilongok', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (10, 'Polsek Kalibagor', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (11, 'Polsek Karanglewas', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (12, 'Polsek Kebasen', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (13, 'Polsek Patikraja', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (14, 'Polsek Pekuncen', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (15, 'Polsek Purwokerto Barat', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (16, 'Polsek Purwokerto Selatan', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (17, 'Polsek Purwokerto Timur', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (18, 'Polsek Purwokerto Utara', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (19, 'Polsek Sokaraja', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (20, 'Polsek Somagede', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (21, 'Polsek Sumbang', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (22, 'Polsek Sumpiuh', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (23, 'Polsek Wangon', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (24, 'Polsek Kembaran', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (25, 'Polsek Tambak', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (26, 'Polsek Kemranjen', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (27, 'Polsek Rawalo', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (28, 'Polsek Jatilawang', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (29, 'Polsek Purwojati', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (30, 'Polsek Lumbir', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (31, 'Polsek Ajibarang', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (32, 'Polsek Gumelar', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (33, 'Polsek Kedungbanteng', 4);


#
# TABLE STRUCTURE FOR: t_jenis
#

DROP TABLE IF EXISTS t_jenis;

CREATE TABLE `t_jenis` (
  `kode_jenis` int(6) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (1, 'Kertas');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (2, 'Logam');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (3, 'Non-Logam');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (4, 'Kimia');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (5, 'Elektronik');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (6, 'Mekanik');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (7, 'Gas');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (8, 'Alat Rumah Tangga');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (9, 'Bahan Makanan/Minuman');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (10, 'Tumbuhan/Tanaman');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (11, 'Hewan Ternak');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (12, 'Rumah Bangunan/Gedung');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (13, 'Tanah');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (14, 'Kapal Laut dan Udara');


#
# TABLE STRUCTURE FOR: t_kategori
#

DROP TABLE IF EXISTS t_kategori;

CREATE TABLE `t_kategori` (
  `kode_kategori` int(10) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO t_kategori (`kode_kategori`, `kategori`) VALUES (1, 'Umroh Plus');
INSERT INTO t_kategori (`kode_kategori`, `kategori`) VALUES (2, 'Umroh Reguler');
INSERT INTO t_kategori (`kode_kategori`, `kategori`) VALUES (4, 'Umroh Paket');


#
# TABLE STRUCTURE FOR: t_kegiatan
#

DROP TABLE IF EXISTS t_kegiatan;

CREATE TABLE `t_kegiatan` (
  `kode_kegiatan` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(500) NOT NULL,
  `tgl_kegiatan` date NOT NULL,
  PRIMARY KEY (`kode_kegiatan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO t_kegiatan (`kode_kegiatan`, `judul`, `tgl_kegiatan`) VALUES (2, 'Penyerahan Uang', '2015-12-08');
INSERT INTO t_kegiatan (`kode_kegiatan`, `judul`, `tgl_kegiatan`) VALUES (3, 'Penyerahan Aplikasi Kepada RUPBASAN Klas II Purwokerto', '2015-12-07');
INSERT INTO t_kegiatan (`kode_kegiatan`, `judul`, `tgl_kegiatan`) VALUES (4, 'Selesai', '2015-12-05');


#
# TABLE STRUCTURE FOR: t_keluar_sebelum
#

DROP TABLE IF EXISTS t_keluar_sebelum;

CREATE TABLE `t_keluar_sebelum` (
  `kode_keluar_sebelum` int(6) NOT NULL AUTO_INCREMENT,
  `no_keluar_sebelum` varchar(100) NOT NULL,
  `file_keluar_sebelum` varchar(100) NOT NULL,
  `no_tetapan_sebelum` varchar(50) NOT NULL,
  `tgl_tetapan_sebelum` date NOT NULL,
  `file_tetapan_sebelum` varchar(100) NOT NULL,
  `no_ba_sebelum` varchar(50) NOT NULL,
  `tgl_ba_sebelum` date NOT NULL,
  `file_ba_sebelum` varchar(100) NOT NULL,
  `tgl_keluar_sebelum` date NOT NULL,
  `kode_masuk` int(6) NOT NULL,
  `instansi_pemohon` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nama_petugas_a` varchar(100) NOT NULL,
  `nip_petugas_a` varchar(50) NOT NULL,
  `pangkat_petugas_a` varchar(100) NOT NULL,
  `jabatan_petugas_a` varchar(100) NOT NULL,
  `nama_petugas_b` varchar(100) NOT NULL,
  `nip_petugas_b` varchar(50) NOT NULL,
  `pangkat_petugas_b` varchar(100) NOT NULL,
  `jabatan_petugas_b` varchar(100) NOT NULL,
  `nama_saksi_a` varchar(100) NOT NULL,
  `ktp_saksi_a` varchar(100) NOT NULL,
  `alamat_saksi_a` varchar(500) NOT NULL,
  `nama_saksi_b` varchar(100) NOT NULL,
  `ktp_saksi_b` varchar(100) NOT NULL,
  `alamat_saksi_b` varchar(500) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  PRIMARY KEY (`kode_keluar_sebelum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_keluar_setelah
#

DROP TABLE IF EXISTS t_keluar_setelah;

CREATE TABLE `t_keluar_setelah` (
  `kode_keluar_setelah` int(6) NOT NULL AUTO_INCREMENT,
  `no_keluar_setelah` varchar(100) NOT NULL,
  `file_keluar_setelah` varchar(100) NOT NULL,
  `no_putusan_setelah` varchar(50) NOT NULL,
  `tgl_putusan_setelah` date NOT NULL,
  `file_putusan_setelah` varchar(100) NOT NULL,
  `no_tetapan_setelah` varchar(50) NOT NULL,
  `tgl_tetapan_setelah` date NOT NULL,
  `file_tetapan_setelah` varchar(100) NOT NULL,
  `no_ba_setelah` varchar(50) NOT NULL,
  `tgl_ba_setelah` date NOT NULL,
  `file_ba_setelah` varchar(100) NOT NULL,
  `tgl_keluar_setelah` date NOT NULL,
  `kode_masuk` int(6) NOT NULL,
  `instansi_pemohon` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nama_petugas_a` varchar(100) NOT NULL,
  `nip_petugas_a` varchar(50) NOT NULL,
  `pangkat_petugas_a` varchar(100) NOT NULL,
  `jabatan_petugas_a` varchar(100) NOT NULL,
  `nama_petugas_b` varchar(100) NOT NULL,
  `nip_petugas_b` varchar(50) NOT NULL,
  `pangkat_petugas_b` varchar(100) NOT NULL,
  `jabatan_petugas_b` varchar(100) NOT NULL,
  `nama_saksi_a` varchar(100) NOT NULL,
  `ktp_saksi_a` varchar(100) NOT NULL,
  `alamat_saksi_a` varchar(500) NOT NULL,
  `nama_saksi_b` varchar(100) NOT NULL,
  `ktp_saksi_b` varchar(100) NOT NULL,
  `alamat_saksi_b` varchar(500) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  PRIMARY KEY (`kode_keluar_setelah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_kirim
#

DROP TABLE IF EXISTS t_kirim;

CREATE TABLE `t_kirim` (
  `kode_kirim` int(6) NOT NULL AUTO_INCREMENT,
  `no_kirim` varchar(100) NOT NULL,
  `tgl_kirim` datetime NOT NULL,
  `file_kirim` varchar(100) NOT NULL,
  `no_perintah_kirim` varchar(50) NOT NULL,
  `tgl_perintah_kirim` date NOT NULL,
  `file_perintah_kirim` varchar(100) NOT NULL,
  `no_tetapan_kirim` varchar(50) NOT NULL,
  `tgl_tetapan_kirim` date NOT NULL,
  `file_tetapan_kirim` varchar(100) NOT NULL,
  `no_ba_kirim` varchar(50) NOT NULL,
  `tgl_ba_kirim` date NOT NULL,
  `file_ba_kirim` varchar(100) NOT NULL,
  `pasal` varchar(20) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `nip_petugas` varchar(50) NOT NULL,
  `pangkat_petugas` varchar(100) NOT NULL,
  `jabatan_petugas` varchar(100) NOT NULL,
  `nama_saksi` varchar(100) NOT NULL,
  `nip_saksi` varchar(50) NOT NULL,
  `pangkat_saksi` varchar(100) NOT NULL,
  `jabatan_saksi` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `kode_instansi` int(6) NOT NULL,
  `status_kirim` varchar(20) NOT NULL,
  `alasan` varchar(500) NOT NULL,
  `verifikasioleh` varchar(100) NOT NULL,
  `tgl_verifikasi` datetime NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  PRIMARY KEY (`kode_kirim`),
  KEY `id_user` (`nama_lengkap`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_kirim_barang
#

DROP TABLE IF EXISTS t_kirim_barang;

CREATE TABLE `t_kirim_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_kirim` varchar(100) NOT NULL,
  `no_reg` varchar(100) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_komenberita
#

DROP TABLE IF EXISTS t_komenberita;

CREATE TABLE `t_komenberita` (
  `kode_komen` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `komen` varchar(500) NOT NULL,
  `tgl_komen` datetime NOT NULL,
  `status` int(2) NOT NULL,
  `kode_berita` int(10) NOT NULL,
  PRIMARY KEY (`kode_komen`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO t_komenberita (`kode_komen`, `nama`, `email`, `komen`, `tgl_komen`, `status`, `kode_berita`) VALUES (6, 'Broth', 'brotbrot@it.com', 'iya bener il', '2015-12-06 13:14:07', 1, 3);
INSERT INTO t_komenberita (`kode_komen`, `nama`, `email`, `komen`, `tgl_komen`, `status`, `kode_berita`) VALUES (7, 'Fikar Singh Haniya', 'fikfik@singatel.com', 'Kata siapa bagus? jelek gini, yang jelas lah kalo bikin kata-kata tuh. \r\njangan asal-asalan lah, jadinya kan aneh.\r\n \r\napa gara-gara saya ga bisa baca bahasa inggris ya? \r\nhehe', '2015-12-06 15:07:57', 0, 3);


#
# TABLE STRUCTURE FOR: t_komenpengumuman
#

DROP TABLE IF EXISTS t_komenpengumuman;

CREATE TABLE `t_komenpengumuman` (
  `kode_komen` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `komen` varchar(500) NOT NULL,
  `tgl_komen` datetime NOT NULL,
  `status` int(2) NOT NULL,
  `kode_pengumuman` int(10) NOT NULL,
  PRIMARY KEY (`kode_komen`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO t_komenpengumuman (`kode_komen`, `nama`, `email`, `komen`, `tgl_komen`, `status`, `kode_pengumuman`) VALUES (2, 'Purno', 'zaen.dindin@wings.co.id', 'semoga saya selalu sabar dan mendapat rezeki yang halal banyak dan berkah amiin', '2015-12-09 15:06:43', 0, 2);


#
# TABLE STRUCTURE FOR: t_masuk
#

DROP TABLE IF EXISTS t_masuk;

CREATE TABLE `t_masuk` (
  `kode_masuk` int(6) NOT NULL AUTO_INCREMENT,
  `no_penitipan` varchar(100) NOT NULL,
  `tgl_penitipan` date NOT NULL,
  `file_penitipan` varchar(100) NOT NULL,
  `no_perintah` varchar(50) NOT NULL,
  `tgl_perintah` date NOT NULL,
  `file_perintah` varchar(100) NOT NULL,
  `no_tetapan_hakim` varchar(50) NOT NULL,
  `tgl_tetapan_hakim` date NOT NULL,
  `file_tetapan_hakim` varchar(100) NOT NULL,
  `no_ba_penitipan` varchar(50) NOT NULL,
  `tgl_ba_penitipan` date NOT NULL,
  `file_ba_penitipan` varchar(100) NOT NULL,
  `pasal` varchar(20) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `nip_petugas` varchar(50) NOT NULL,
  `pangkat_petugas` varchar(100) NOT NULL,
  `jabatan_petugas` varchar(100) NOT NULL,
  `nama_saksi` varchar(100) NOT NULL,
  `nip_saksi` varchar(50) NOT NULL,
  `pangkat_saksi` varchar(100) NOT NULL,
  `jabatan_saksi` varchar(100) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  PRIMARY KEY (`kode_masuk`),
  KEY `kode_instansi` (`instansi`),
  KEY `id_user` (`nama_lengkap`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO t_masuk (`kode_masuk`, `no_penitipan`, `tgl_penitipan`, `file_penitipan`, `no_perintah`, `tgl_perintah`, `file_perintah`, `no_tetapan_hakim`, `tgl_tetapan_hakim`, `file_tetapan_hakim`, `no_ba_penitipan`, `tgl_ba_penitipan`, `file_ba_penitipan`, `pasal`, `tersangka`, `nama_petugas`, `nip_petugas`, `pangkat_petugas`, `jabatan_petugas`, `nama_saksi`, `nip_saksi`, `pangkat_saksi`, `jabatan_saksi`, `instansi`, `nama_lengkap`, `keterangan`) VALUES (1, 'coba', '2015-10-29', '', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00', '', 'avvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv', '0000-00-00', '', 'vvbbbbbbbbbbbbbbbbbbbbbbbbbbb', '0000-00-00', '', 'aaa', 'dodo', '', '', '', '', '', '', '', '', 'Kejaksaan Negeri Banyumas', 'Dindin Zaenudin', '');
INSERT INTO t_masuk (`kode_masuk`, `no_penitipan`, `tgl_penitipan`, `file_penitipan`, `no_perintah`, `tgl_perintah`, `file_perintah`, `no_tetapan_hakim`, `tgl_tetapan_hakim`, `file_tetapan_hakim`, `no_ba_penitipan`, `tgl_ba_penitipan`, `file_ba_penitipan`, `pasal`, `tersangka`, `nama_petugas`, `nip_petugas`, `pangkat_petugas`, `jabatan_petugas`, `nama_saksi`, `nip_saksi`, `pangkat_saksi`, `jabatan_saksi`, `instansi`, `nama_lengkap`, `keterangan`) VALUES (2, 'aaaaa', '2015-12-04', '', '', '0000-00-00', '', '', '0000-00-00', '', '', '0000-00-00', '', 'aaa', 'aa', '', '', '', '', '', '', '', '', 'Pengadilan Negeri Banyumas', 'Dindin Zaenudin', '');


#
# TABLE STRUCTURE FOR: t_masuk_barang
#

DROP TABLE IF EXISTS t_masuk_barang;

CREATE TABLE `t_masuk_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_penitipan` varchar(100) NOT NULL,
  `no_reg` varchar(100) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (24, 'coba', 'BB1/a/11');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (25, 'coba', 'BB2/afa/af');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (26, 'coba', 'BB3/afa/11');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (27, 'coba', 'BB3/qggw/11 E');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (28, 'coba', 'BB3/afafafafaa/11 E');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (29, 'coba', 'BB5/WFW/11 E');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (30, 'coba', 'BB1/a/10');


#
# TABLE STRUCTURE FOR: t_mohon
#

DROP TABLE IF EXISTS t_mohon;

CREATE TABLE `t_mohon` (
  `kode_mohon` int(6) NOT NULL AUTO_INCREMENT,
  `no_mohon` varchar(100) NOT NULL,
  `tgl_mohon` datetime NOT NULL,
  `no_tetapan_mohon` varchar(50) NOT NULL,
  `tgl_tetapan_mohon` date NOT NULL,
  `file_tetapan_mohon` varchar(100) NOT NULL,
  `no_ba_mohon` varchar(50) NOT NULL,
  `tgl_ba_mohon` date NOT NULL,
  `file_ba_mohon` varchar(100) NOT NULL,
  `kode_kirim` int(6) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `kode_instansi` int(6) NOT NULL,
  `nama_petugas_a` varchar(100) NOT NULL,
  `nip_petugas_a` varchar(50) NOT NULL,
  `pangkat_petugas_a` varchar(100) NOT NULL,
  `jabatan_petugas_a` varchar(100) NOT NULL,
  `nama_petugas_b` varchar(100) NOT NULL,
  `nip_petugas_b` varchar(50) NOT NULL,
  `pangkat_petugas_b` varchar(100) NOT NULL,
  `jabatan_petugas_b` varchar(100) NOT NULL,
  `nama_saksi_a` varchar(100) NOT NULL,
  `ktp_saksi_a` varchar(100) NOT NULL,
  `alamat_saksi_a` varchar(500) NOT NULL,
  `nama_saksi_b` varchar(100) NOT NULL,
  `ktp_saksi_b` varchar(100) NOT NULL,
  `alamat_saksi_b` varchar(500) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `status_mohon` varchar(20) NOT NULL,
  `alasan` varchar(500) NOT NULL,
  `verifikasioleh` varchar(100) NOT NULL,
  `tgl_verifikasi` datetime NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `file_mohon` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_mohon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_mohon_barang
#

DROP TABLE IF EXISTS t_mohon_barang;

CREATE TABLE `t_mohon_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_mohon` varchar(100) NOT NULL,
  `kode_barang_keluar` int(6) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_pegawai
#

DROP TABLE IF EXISTS t_pegawai;

CREATE TABLE `t_pegawai` (
  `id_pegawai` int(6) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `pangkat` varchar(100) NOT NULL,
  `gol_jabatan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_penelitian
#

DROP TABLE IF EXISTS t_penelitian;

CREATE TABLE `t_penelitian` (
  `kode_penelitian` int(6) NOT NULL AUTO_INCREMENT,
  `no_penelitian` varchar(100) NOT NULL,
  `tgl_penelitian` date NOT NULL,
  `kode_barang` int(6) NOT NULL,
  `segel` varchar(100) NOT NULL,
  `kondisi` varchar(100) NOT NULL,
  `sifat` varchar(100) NOT NULL,
  `macam` varchar(100) NOT NULL,
  `bentuk` varchar(100) NOT NULL,
  `berat` varchar(20) NOT NULL,
  `volume` varchar(20) NOT NULL,
  `lebar` varchar(20) NOT NULL,
  `tinggi` varchar(20) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `laras` varchar(100) NOT NULL,
  `pabrik` varchar(100) NOT NULL,
  `no_pabrik` varchar(100) NOT NULL,
  `peluru` varchar(100) NOT NULL,
  `peledak` varchar(100) NOT NULL,
  `pas` varchar(100) NOT NULL,
  `tgl_pas` date NOT NULL,
  `no_pas` varchar(100) NOT NULL,
  `berlaku_pas` varchar(100) NOT NULL,
  `no_senjata` varchar(100) NOT NULL,
  `bahan` varchar(100) NOT NULL,
  `thn_buat` varchar(20) NOT NULL,
  `thn_terbit` varchar(20) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `kaliber` varchar(100) NOT NULL,
  `no_mesin` varchar(100) NOT NULL,
  `no_chasis` varchar(100) NOT NULL,
  `daya` varchar(20) NOT NULL,
  `pengarang` varchar(100) NOT NULL,
  `tulisan` varchar(300) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `usia` varchar(20) NOT NULL,
  `karat` varchar(20) NOT NULL,
  `warna_kemas` varchar(30) NOT NULL,
  `batas` varchar(500) NOT NULL,
  `imb` varchar(100) NOT NULL,
  `isi` varchar(500) NOT NULL,
  `bukti` varchar(100) NOT NULL,
  `bendera` varchar(100) NOT NULL,
  `cacat` varchar(500) NOT NULL,
  `lain` varchar(500) NOT NULL,
  `petugas_a` varchar(100) NOT NULL,
  `petugas_b` varchar(100) NOT NULL,
  `kepala` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_penelitian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_pengumuman
#

DROP TABLE IF EXISTS t_pengumuman;

CREATE TABLE `t_pengumuman` (
  `kode_pengumuman` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(500) NOT NULL,
  `subjudul` varchar(1000) NOT NULL,
  `isi` varchar(10000) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `tgl_pengumuman` datetime NOT NULL,
  PRIMARY KEY (`kode_pengumuman`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO t_pengumuman (`kode_pengumuman`, `judul`, `subjudul`, `isi`, `foto`, `nama_lengkap`, `status`, `tgl_pengumuman`) VALUES (2, 'Bismillah, Artikel Pertama Membuat Booming', 'Indahnya artikel ini memberi arti yang begitu berarti.', '<p>Alhamdulillah, artikel pertama ini membuat hatiku&nbsp;<s>cenat-cenut</s>&nbsp;bergelimang dengan kebahagiaan.</p>\r\n\r\n<blockquote>\r\n<p>begitu indah, sampai-sampai sulit diungkapkan</p>\r\n</blockquote>\r\n\r\n<p>ini adalah <span class=\"marker\"><ins><span style=\"background-color:#FFFF00\">anugerah</span></ins></span> yang Maha Kuasa, hidupku begitu indah. Semoga berkah, amiin.</p>', '216563279-20151206-200451.jpg', 'Dindin Zaenudin', 1, '2015-11-29 10:54:20');


#
# TABLE STRUCTURE FOR: t_pesan
#

DROP TABLE IF EXISTS t_pesan;

CREATE TABLE `t_pesan` (
  `kode_pesan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `judul` varchar(500) NOT NULL,
  `isi` varchar(1000) NOT NULL,
  `tgl_pesan` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`kode_pesan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO t_pesan (`kode_pesan`, `nama_lengkap`, `email`, `judul`, `isi`, `tgl_pesan`, `status`) VALUES (1, 'Al Khawarizmi', 'drac@gorgon.com', 'Aplikasi SIPBB', 'saya mau menanyakan mengenai aplikasi SIPBB, bisa tolong hubungi nomor 082226590800?', '2015-12-09 18:30:10', 0);
INSERT INTO t_pesan (`kode_pesan`, `nama_lengkap`, `email`, `judul`, `isi`, `tgl_pesan`, `status`) VALUES (2, 'Fikar Maulana', 'djibril@wingcorp.com', 'Lamaran Kerja', 'saya mau menanyakan mengenai informasi lamaran kerja yang ada di WINGS corporation.', '2015-12-09 18:32:55', 1);
INSERT INTO t_pesan (`kode_pesan`, `nama_lengkap`, `email`, `judul`, `isi`, `tgl_pesan`, `status`) VALUES (3, 'Echa Lube', 'lulubecha@gamil.com', 'Cuma Test', 'ini test', '2015-12-09 20:22:34', 1);


#
# TABLE STRUCTURE FOR: t_profil
#

DROP TABLE IF EXISTS t_profil;

CREATE TABLE `t_profil` (
  `kode_profil` int(5) NOT NULL AUTO_INCREMENT,
  `profil` varchar(10000) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `struktur` varchar(200) NOT NULL,
  PRIMARY KEY (`kode_profil`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO t_profil (`kode_profil`, `profil`, `foto`, `struktur`) VALUES (1, '<p>Sistem Informasi Pengelolaan Basan Baran (SIPBB) merupakan salah satu inovasi dari RUPBASAN Klas II Purwokerto dalam melaksanakan pelayanan administrasi teknis basan baran menggunakan TI (Teknologi Informasi) dengan inovasi sendiri dengan harapan pelayanan berbasis TI ini memudahkan pelayanan, keakuratan basan baran, membuat kepercayaan dan memuaskan bagi publik dan instansi terkait.</p>\r\n\r\n<p>SIPBB merupakan sebuah aplikasi untuk menunjang pengelolaan benda sitaan negara dan barang rampasan negara di wilayah Kabupaten Banyumas demi terwujudnya motto, visi, dan misi RUPBASAN Klas II Purwokerto. SIPBB bertujuan untuk membuat pengelolaan Basan Baran di wilayah Kabupaten Banyumas menjadi terintegrasi, terpadu dan reliable dengan cara memberikan potret kondisi Basan Baran terakhir.</p>\r\n\r\n<p>Pembuatan aplikasi ini ditujukan untuk membantu proses pelayanan pada RUPBASAN Klas II Purwokerto khususnya dalam hal proses penerimaan penyimpanan benda sitaan negara dalam setiap tingkat pemeriksaan, mengeluarkan untuk kepentingan pemeriksaan, dan proses pengeluran / penghapusan basan baran.</p>\r\n\r\n<p>Peran aplikasi ini adalah membantu proses input dan pengelolaan data secara mudah melalui web database sehingga setiap user dapat menginput dan mengakses informasi data tanpa batasan lokasi mengingat stakeholder berada pada wilayah yang berjauhan (tersebar di wilayah hukum kabupaten Banyumas) sedangkan RUPBASAN Klas II Purwokerto sebagai user pelayanan dituntut untuk melakukan fungsinya dengan efektif, efesien, cepat, terbuka, dan mudah.</p>\r\n', '691985904-20151209-124004.jpg', '924391658-20151209-131224.jpg');


#
# TABLE STRUCTURE FOR: t_sebelum_barang
#

DROP TABLE IF EXISTS t_sebelum_barang;

CREATE TABLE `t_sebelum_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_keluar_sebelum` varchar(100) NOT NULL,
  `kode_barang_keluar` int(6) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_setelah_barang
#

DROP TABLE IF EXISTS t_setelah_barang;

CREATE TABLE `t_setelah_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_keluar_setelah` varchar(100) NOT NULL,
  `kode_barang_keluar` int(6) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_slide
#

DROP TABLE IF EXISTS t_slide;

CREATE TABLE `t_slide` (
  `kode_slide` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(500) NOT NULL,
  `subjudul` varchar(1000) NOT NULL,
  `isi` varchar(10000) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`kode_slide`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO t_slide (`kode_slide`, `judul`, `subjudul`, `isi`, `foto`, `status`) VALUES (2, 'Bismillah', 'Alhamdulillah berhasil, terus sukseskan.', 'Subhanalloh, akhirnya, inilah awal kesuksesan aplikasi ini, mudah-mudahan semuanya berjalan semakin baik. Amin.', '779620386-20151128-212639.png', 1);
INSERT INTO t_slide (`kode_slide`, `judul`, `subjudul`, `isi`, `foto`, `status`) VALUES (3, 'Dindin Umroh', 'Dindin Zaenudin Pergi UmrohTahun 2017', 'Inilah slide mengenai kesuksesan dindin zaenudin pergi ke baitullah, melalui uangnya sendiri. Dengan bangganya, dan khusunya.', '489558415-20151201-134413.png', 1);


#
# TABLE STRUCTURE FOR: t_status
#

DROP TABLE IF EXISTS t_status;

CREATE TABLE `t_status` (
  `kode_status` int(6) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO t_status (`kode_status`, `status`) VALUES (1, 'Bon Pinjam');
INSERT INTO t_status (`kode_status`, `status`) VALUES (2, 'Permintaan Instansi Terkait');


#
# TABLE STRUCTURE FOR: t_status_setelah
#

DROP TABLE IF EXISTS t_status_setelah;

CREATE TABLE `t_status_setelah` (
  `kode_status` int(6) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_status`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (3, 'Dikembalikan');
INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (4, 'Rampas Negara / Lelang');
INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (5, 'Dimusnahkan');
INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (6, 'Dikembalikan Ke Instansi');
INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (7, 'Perkara Lain');


#
# TABLE STRUCTURE FOR: t_tim
#

DROP TABLE IF EXISTS t_tim;

CREATE TABLE `t_tim` (
  `kode_tim` int(10) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(500) NOT NULL,
  `nip` varchar(200) NOT NULL,
  `gol` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `foto` varchar(200) NOT NULL,
  PRIMARY KEY (`kode_tim`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO t_tim (`kode_tim`, `nama_lengkap`, `nip`, `gol`, `jabatan`, `deskripsi`, `foto`) VALUES (1, 'Dindin Zaenudin', '3278052410920004', 'IV A', 'Kepala Divisi IT', 'single, ganteng, kaya, dan baik banget', '1172046256-20151209-142657.jpg');
INSERT INTO t_tim (`kode_tim`, `nama_lengkap`, `nip`, `gol`, `jabatan`, `deskripsi`, `foto`) VALUES (2, 'Martha Irfanto', '110934 131 4410', 'III B', 'Staf IT', 'Status: Menikah\r\nAnak: 2 berencana 3', '747044994-20151209-142737.jpg');
INSERT INTO t_tim (`kode_tim`, `nama_lengkap`, `nip`, `gol`, `jabatan`, `deskripsi`, `foto`) VALUES (3, 'Ilyasviel Von Einzbern', '0124 0804 1445 3156', 'II D', 'Sekertaris', 'Ini hanyalah pegawai fiktif tidak serius ', '145587512-20151209-142822.jpg');


#
# TABLE STRUCTURE FOR: t_user
#

DROP TABLE IF EXISTS t_user;

CREATE TABLE `t_user` (
  `id_user` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL,
  `kode_grup` int(6) NOT NULL,
  `kode_instansi` int(6) NOT NULL,
  `stts` int(2) NOT NULL,
  `foto` varchar(200) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `kode_instansi` (`kode_instansi`),
  KEY `kode_grup` (`kode_grup`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (2, 'admin', 'bc916cec2c4da86bf2d7a48ce4cbca28', 'Irfan Martha', 'administrator', 1, 1, 1, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (3, 'dindin', '788ad7091c4e500855fcf4e3cb1bda29', 'Dindin Zaenudin', 'operator', 1, 1, 1, '999188912-20151214-153205.jpg');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (4, 'yani', '080840925a7e2087673145d83918c658', 'Yani Yuliani', 'instansi', 4, 6, 1, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (5, 'ilya', '67e21d45f7b1fab0a3c43dd4339ee8e9', 'Ilyasviel Von Einzbern', 'instansi', 2, 2, 1, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (6, 'master', 'eb0a191797624dd3a48fa681d3061212', 'Master Fang', 'instansi', 3, 4, 0, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (8, 'titis', '5fe250f6fc5fb39ed7258111e6b86791', 'Laksita Titis E', 'operator', 1, 1, 1, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (9, 'ARI', 'fc292bd7df071858c2d0f955545673c1', 'Ari Prakoso', 'instansi', 2, 2, 0, '');


