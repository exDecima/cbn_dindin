#
# TABLE STRUCTURE FOR: t_background
#

DROP TABLE IF EXISTS t_background;

CREATE TABLE `t_background` (
  `kode_bg` int(6) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(100) NOT NULL,
  `warna` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_bg`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO t_background (`kode_bg`, `gambar`, `warna`) VALUES (1, '177569274-20150806-071331.jpg', '#000000');


#
# TABLE STRUCTURE FOR: t_barang
#

DROP TABLE IF EXISTS t_barang;

CREATE TABLE `t_barang` (
  `kode_barang` int(6) NOT NULL AUTO_INCREMENT,
  `no_reg` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `gol` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `ket` varchar(30) NOT NULL,
  `qty` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `kd_brg` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (1, 'BB1/A/10', 'coba', 'Berbahaya', 'Kertas', 2, '820951805-20151212-155629.jpg', 'rupbasan', 2, 'dindin', 'PS00000001');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (2, 'BB1/a/11', 'afafa', 'Berharga', 'Elektronik', 3, '', 'rupbasan', 3, 'dindin', 'PS00000002');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (3, 'BB2/afa/af', 'fakjgbjgwhg', 'Umum Terbuka', 'Kertas', 4, '', 'rupbasan', 4, 'dindin', 'PS00000003');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (4, 'BB3/afa/11', 'aafafa', 'Berharga', 'Kertas', 3, '', 'rupbasan', 3, 'dindin', 'PS00000004');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (5, 'BB3/qggw/11 E', 'aafagagg', 'Berbahaya', 'Kertas', 7, '', 'rupbasan', 7, 'dindin', 'PS00000005');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (6, 'BB3/afafafafaa/11 E', 'aaafa', 'Umum Tertutup', 'Kertas', 8, '', 'rupbasan', 8, 'dindin', 'PS00000006');
INSERT INTO t_barang (`kode_barang`, `no_reg`, `nama_barang`, `gol`, `jenis`, `jumlah`, `foto`, `ket`, `qty`, `username`, `kd_brg`) VALUES (7, 'BB5/WFW/11 E', 'aaa', 'Berharga', 'Kertas', 4, '', 'rupbasan', 4, 'dindin', 'PS00000007');


#
# TABLE STRUCTURE FOR: t_barang_keluar
#

DROP TABLE IF EXISTS t_barang_keluar;

CREATE TABLE `t_barang_keluar` (
  `kode_barang_keluar` int(6) NOT NULL AUTO_INCREMENT,
  `no_reg` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `gol` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `qty` int(5) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `kode_status` int(6) NOT NULL,
  `ket` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `kd_brg` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_barang_keluar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_gol
#

DROP TABLE IF EXISTS t_gol;

CREATE TABLE `t_gol` (
  `kode_gol` int(6) NOT NULL AUTO_INCREMENT,
  `gol` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_gol`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (1, 'Umum Terbuka');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (2, 'Umum Tertutup');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (3, 'Berharga');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (4, 'Berbahaya');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (5, 'Hewan');
INSERT INTO t_gol (`kode_gol`, `gol`) VALUES (6, 'Tanaman');


#
# TABLE STRUCTURE FOR: t_grup
#

DROP TABLE IF EXISTS t_grup;

CREATE TABLE `t_grup` (
  `kode_grup` int(6) NOT NULL AUTO_INCREMENT,
  `grup` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_grup`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO t_grup (`kode_grup`, `grup`) VALUES (1, 'Rupbasan');
INSERT INTO t_grup (`kode_grup`, `grup`) VALUES (2, 'Pengadilan');
INSERT INTO t_grup (`kode_grup`, `grup`) VALUES (3, 'Kejaksaan');
INSERT INTO t_grup (`kode_grup`, `grup`) VALUES (4, 'Kepolisian');


#
# TABLE STRUCTURE FOR: t_instansi
#

DROP TABLE IF EXISTS t_instansi;

CREATE TABLE `t_instansi` (
  `kode_instansi` int(6) NOT NULL AUTO_INCREMENT,
  `instansi` varchar(100) NOT NULL,
  `kode_grup` int(11) NOT NULL,
  PRIMARY KEY (`kode_instansi`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (1, 'RUPBASAN', 1);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (2, 'Pengadilan Negeri Banyumas', 2);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (3, 'Pengadilan Negeri Purwokerto', 2);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (4, 'Kejaksaan Negeri Banyumas', 3);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (5, 'Kejaksaan Negeri Purwokerto', 3);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (6, 'Polres Banyumas', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (7, 'Polsek Banyumas', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (8, 'Polsek Baturraden', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (9, 'Polsek Cilongok', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (10, 'Polsek Kalibagor', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (11, 'Polsek Karanglewas', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (12, 'Polsek Kebasen', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (13, 'Polsek Patikraja', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (14, 'Polsek Pekuncen', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (15, 'Polsek Purwokerto Barat', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (16, 'Polsek Purwokerto Selatan', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (17, 'Polsek Purwokerto Timur', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (18, 'Polsek Purwokerto Utara', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (19, 'Polsek Sokaraja', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (20, 'Polsek Somagede', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (21, 'Polsek Sumbang', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (22, 'Polsek Sumpiuh', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (23, 'Polsek Wangon', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (24, 'Polsek Kembaran', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (25, 'Polsek Tambak', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (26, 'Polsek Kemranjen', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (27, 'Polsek Rawalo', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (28, 'Polsek Jatilawang', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (29, 'Polsek Purwojati', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (30, 'Polsek Lumbir', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (31, 'Polsek Ajibarang', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (32, 'Polsek Gumelar', 4);
INSERT INTO t_instansi (`kode_instansi`, `instansi`, `kode_grup`) VALUES (33, 'Polsek Kedungbanteng', 4);


#
# TABLE STRUCTURE FOR: t_jenis
#

DROP TABLE IF EXISTS t_jenis;

CREATE TABLE `t_jenis` (
  `kode_jenis` int(6) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (1, 'Kertas');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (2, 'Logam');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (3, 'Non-Logam');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (4, 'Kimia');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (5, 'Elektronik');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (6, 'Mekanik');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (7, 'Gas');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (8, 'Alat Rumah Tangga');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (9, 'Bahan Makanan/Minuman');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (10, 'Tumbuhan/Tanaman');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (11, 'Hewan Ternak');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (12, 'Rumah Bangunan/Gedung');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (13, 'Tanah');
INSERT INTO t_jenis (`kode_jenis`, `jenis`) VALUES (14, 'Kapal Laut dan Udara');


#
# TABLE STRUCTURE FOR: t_keluar_sebelum
#

DROP TABLE IF EXISTS t_keluar_sebelum;

CREATE TABLE `t_keluar_sebelum` (
  `kode_keluar_sebelum` int(6) NOT NULL AUTO_INCREMENT,
  `no_keluar_sebelum` varchar(100) NOT NULL,
  `file_keluar_sebelum` varchar(100) NOT NULL,
  `no_tetapan_sebelum` varchar(50) NOT NULL,
  `tgl_tetapan_sebelum` date NOT NULL,
  `file_tetapan_sebelum` varchar(100) NOT NULL,
  `no_ba_sebelum` varchar(50) NOT NULL,
  `tgl_ba_sebelum` date NOT NULL,
  `file_ba_sebelum` varchar(100) NOT NULL,
  `tgl_keluar_sebelum` date NOT NULL,
  `kode_masuk` int(6) NOT NULL,
  `instansi_pemohon` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nama_petugas_a` varchar(100) NOT NULL,
  `nip_petugas_a` varchar(50) NOT NULL,
  `pangkat_petugas_a` varchar(100) NOT NULL,
  `jabatan_petugas_a` varchar(100) NOT NULL,
  `nama_petugas_b` varchar(100) NOT NULL,
  `nip_petugas_b` varchar(50) NOT NULL,
  `pangkat_petugas_b` varchar(100) NOT NULL,
  `jabatan_petugas_b` varchar(100) NOT NULL,
  `nama_saksi_a` varchar(100) NOT NULL,
  `ktp_saksi_a` varchar(100) NOT NULL,
  `alamat_saksi_a` varchar(500) NOT NULL,
  `nama_saksi_b` varchar(100) NOT NULL,
  `ktp_saksi_b` varchar(100) NOT NULL,
  `alamat_saksi_b` varchar(500) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  PRIMARY KEY (`kode_keluar_sebelum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_keluar_setelah
#

DROP TABLE IF EXISTS t_keluar_setelah;

CREATE TABLE `t_keluar_setelah` (
  `kode_keluar_setelah` int(6) NOT NULL AUTO_INCREMENT,
  `no_keluar_setelah` varchar(100) NOT NULL,
  `file_keluar_setelah` varchar(100) NOT NULL,
  `no_putusan_setelah` varchar(50) NOT NULL,
  `tgl_putusan_setelah` date NOT NULL,
  `file_putusan_setelah` varchar(100) NOT NULL,
  `no_tetapan_setelah` varchar(50) NOT NULL,
  `tgl_tetapan_setelah` date NOT NULL,
  `file_tetapan_setelah` varchar(100) NOT NULL,
  `no_ba_setelah` varchar(50) NOT NULL,
  `tgl_ba_setelah` date NOT NULL,
  `file_ba_setelah` varchar(100) NOT NULL,
  `tgl_keluar_setelah` date NOT NULL,
  `kode_masuk` int(6) NOT NULL,
  `instansi_pemohon` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nama_petugas_a` varchar(100) NOT NULL,
  `nip_petugas_a` varchar(50) NOT NULL,
  `pangkat_petugas_a` varchar(100) NOT NULL,
  `jabatan_petugas_a` varchar(100) NOT NULL,
  `nama_petugas_b` varchar(100) NOT NULL,
  `nip_petugas_b` varchar(50) NOT NULL,
  `pangkat_petugas_b` varchar(100) NOT NULL,
  `jabatan_petugas_b` varchar(100) NOT NULL,
  `nama_saksi_a` varchar(100) NOT NULL,
  `ktp_saksi_a` varchar(100) NOT NULL,
  `alamat_saksi_a` varchar(500) NOT NULL,
  `nama_saksi_b` varchar(100) NOT NULL,
  `ktp_saksi_b` varchar(100) NOT NULL,
  `alamat_saksi_b` varchar(500) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  PRIMARY KEY (`kode_keluar_setelah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_kirim
#

DROP TABLE IF EXISTS t_kirim;

CREATE TABLE `t_kirim` (
  `kode_kirim` int(6) NOT NULL AUTO_INCREMENT,
  `no_kirim` varchar(100) NOT NULL,
  `tgl_kirim` datetime NOT NULL,
  `file_kirim` varchar(100) NOT NULL,
  `no_perintah_kirim` varchar(50) NOT NULL,
  `tgl_perintah_kirim` date NOT NULL,
  `file_perintah_kirim` varchar(100) NOT NULL,
  `no_tetapan_kirim` varchar(50) NOT NULL,
  `tgl_tetapan_kirim` date NOT NULL,
  `file_tetapan_kirim` varchar(100) NOT NULL,
  `no_ba_kirim` varchar(50) NOT NULL,
  `tgl_ba_kirim` date NOT NULL,
  `file_ba_kirim` varchar(100) NOT NULL,
  `pasal` varchar(20) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `nip_petugas` varchar(50) NOT NULL,
  `pangkat_petugas` varchar(100) NOT NULL,
  `jabatan_petugas` varchar(100) NOT NULL,
  `nama_saksi` varchar(100) NOT NULL,
  `nip_saksi` varchar(50) NOT NULL,
  `pangkat_saksi` varchar(100) NOT NULL,
  `jabatan_saksi` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `kode_instansi` int(6) NOT NULL,
  `status_kirim` varchar(20) NOT NULL,
  `alasan` varchar(500) NOT NULL,
  `verifikasioleh` varchar(100) NOT NULL,
  `tgl_verifikasi` datetime NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  PRIMARY KEY (`kode_kirim`),
  KEY `id_user` (`nama_lengkap`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_kirim_barang
#

DROP TABLE IF EXISTS t_kirim_barang;

CREATE TABLE `t_kirim_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_kirim` varchar(100) NOT NULL,
  `no_reg` varchar(100) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_masuk
#

DROP TABLE IF EXISTS t_masuk;

CREATE TABLE `t_masuk` (
  `kode_masuk` int(6) NOT NULL AUTO_INCREMENT,
  `no_penitipan` varchar(100) NOT NULL,
  `tgl_penitipan` date NOT NULL,
  `file_penitipan` varchar(100) NOT NULL,
  `no_perintah` varchar(50) NOT NULL,
  `tgl_perintah` date NOT NULL,
  `file_perintah` varchar(100) NOT NULL,
  `no_tetapan_hakim` varchar(50) NOT NULL,
  `tgl_tetapan_hakim` date NOT NULL,
  `file_tetapan_hakim` varchar(100) NOT NULL,
  `no_ba_penitipan` varchar(50) NOT NULL,
  `tgl_ba_penitipan` date NOT NULL,
  `file_ba_penitipan` varchar(100) NOT NULL,
  `pasal` varchar(20) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `nip_petugas` varchar(50) NOT NULL,
  `pangkat_petugas` varchar(100) NOT NULL,
  `jabatan_petugas` varchar(100) NOT NULL,
  `nama_saksi` varchar(100) NOT NULL,
  `nip_saksi` varchar(50) NOT NULL,
  `pangkat_saksi` varchar(100) NOT NULL,
  `jabatan_saksi` varchar(100) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  PRIMARY KEY (`kode_masuk`),
  KEY `kode_instansi` (`instansi`),
  KEY `id_user` (`nama_lengkap`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO t_masuk (`kode_masuk`, `no_penitipan`, `tgl_penitipan`, `file_penitipan`, `no_perintah`, `tgl_perintah`, `file_perintah`, `no_tetapan_hakim`, `tgl_tetapan_hakim`, `file_tetapan_hakim`, `no_ba_penitipan`, `tgl_ba_penitipan`, `file_ba_penitipan`, `pasal`, `tersangka`, `nama_petugas`, `nip_petugas`, `pangkat_petugas`, `jabatan_petugas`, `nama_saksi`, `nip_saksi`, `pangkat_saksi`, `jabatan_saksi`, `instansi`, `nama_lengkap`, `keterangan`) VALUES (1, 'coba', '2015-10-29', '', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00', '', 'avvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv', '0000-00-00', '', 'vvbbbbbbbbbbbbbbbbbbbbbbbbbbb', '0000-00-00', '', 'aaa', 'dodo', '', '', '', '', '', '', '', '', 'Kejaksaan Negeri Banyumas', 'Dindin Zaenudin', '');
INSERT INTO t_masuk (`kode_masuk`, `no_penitipan`, `tgl_penitipan`, `file_penitipan`, `no_perintah`, `tgl_perintah`, `file_perintah`, `no_tetapan_hakim`, `tgl_tetapan_hakim`, `file_tetapan_hakim`, `no_ba_penitipan`, `tgl_ba_penitipan`, `file_ba_penitipan`, `pasal`, `tersangka`, `nama_petugas`, `nip_petugas`, `pangkat_petugas`, `jabatan_petugas`, `nama_saksi`, `nip_saksi`, `pangkat_saksi`, `jabatan_saksi`, `instansi`, `nama_lengkap`, `keterangan`) VALUES (2, 'aaaaa', '2015-12-04', '', '', '0000-00-00', '', '', '0000-00-00', '', '', '0000-00-00', '', 'aaa', 'aa', '', '', '', '', '', '', '', '', 'Pengadilan Negeri Banyumas', 'Dindin Zaenudin', '');


#
# TABLE STRUCTURE FOR: t_masuk_barang
#

DROP TABLE IF EXISTS t_masuk_barang;

CREATE TABLE `t_masuk_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_penitipan` varchar(100) NOT NULL,
  `no_reg` varchar(100) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (24, 'coba', 'BB1/a/11');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (25, 'coba', 'BB2/afa/af');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (26, 'coba', 'BB3/afa/11');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (27, 'coba', 'BB3/qggw/11 E');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (28, 'coba', 'BB3/afafafafaa/11 E');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (29, 'coba', 'BB5/WFW/11 E');
INSERT INTO t_masuk_barang (`kode`, `no_penitipan`, `no_reg`) VALUES (30, 'coba', 'BB1/a/10');


#
# TABLE STRUCTURE FOR: t_mohon
#

DROP TABLE IF EXISTS t_mohon;

CREATE TABLE `t_mohon` (
  `kode_mohon` int(6) NOT NULL AUTO_INCREMENT,
  `no_mohon` varchar(100) NOT NULL,
  `tgl_mohon` datetime NOT NULL,
  `no_tetapan_mohon` varchar(50) NOT NULL,
  `tgl_tetapan_mohon` date NOT NULL,
  `file_tetapan_mohon` varchar(100) NOT NULL,
  `no_ba_mohon` varchar(50) NOT NULL,
  `tgl_ba_mohon` date NOT NULL,
  `file_ba_mohon` varchar(100) NOT NULL,
  `kode_kirim` int(6) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `kode_instansi` int(6) NOT NULL,
  `nama_petugas_a` varchar(100) NOT NULL,
  `nip_petugas_a` varchar(50) NOT NULL,
  `pangkat_petugas_a` varchar(100) NOT NULL,
  `jabatan_petugas_a` varchar(100) NOT NULL,
  `nama_petugas_b` varchar(100) NOT NULL,
  `nip_petugas_b` varchar(50) NOT NULL,
  `pangkat_petugas_b` varchar(100) NOT NULL,
  `jabatan_petugas_b` varchar(100) NOT NULL,
  `nama_saksi_a` varchar(100) NOT NULL,
  `ktp_saksi_a` varchar(100) NOT NULL,
  `alamat_saksi_a` varchar(500) NOT NULL,
  `nama_saksi_b` varchar(100) NOT NULL,
  `ktp_saksi_b` varchar(100) NOT NULL,
  `alamat_saksi_b` varchar(500) NOT NULL,
  `tersangka` varchar(500) NOT NULL,
  `status_mohon` varchar(20) NOT NULL,
  `alasan` varchar(500) NOT NULL,
  `verifikasioleh` varchar(100) NOT NULL,
  `tgl_verifikasi` datetime NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `file_mohon` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_mohon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_mohon_barang
#

DROP TABLE IF EXISTS t_mohon_barang;

CREATE TABLE `t_mohon_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_mohon` varchar(100) NOT NULL,
  `kode_barang_keluar` int(6) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_pegawai
#

DROP TABLE IF EXISTS t_pegawai;

CREATE TABLE `t_pegawai` (
  `id_pegawai` int(6) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `pangkat` varchar(100) NOT NULL,
  `gol_jabatan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_penelitian
#

DROP TABLE IF EXISTS t_penelitian;

CREATE TABLE `t_penelitian` (
  `kode_penelitian` int(6) NOT NULL AUTO_INCREMENT,
  `no_penelitian` varchar(100) NOT NULL,
  `tgl_penelitian` date NOT NULL,
  `kode_barang` int(6) NOT NULL,
  `segel` varchar(100) NOT NULL,
  `kondisi` varchar(100) NOT NULL,
  `sifat` varchar(100) NOT NULL,
  `macam` varchar(100) NOT NULL,
  `bentuk` varchar(100) NOT NULL,
  `berat` varchar(20) NOT NULL,
  `volume` varchar(20) NOT NULL,
  `lebar` varchar(20) NOT NULL,
  `tinggi` varchar(20) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `laras` varchar(100) NOT NULL,
  `pabrik` varchar(100) NOT NULL,
  `no_pabrik` varchar(100) NOT NULL,
  `peluru` varchar(100) NOT NULL,
  `peledak` varchar(100) NOT NULL,
  `pas` varchar(100) NOT NULL,
  `tgl_pas` date NOT NULL,
  `no_pas` varchar(100) NOT NULL,
  `berlaku_pas` varchar(100) NOT NULL,
  `no_senjata` varchar(100) NOT NULL,
  `bahan` varchar(100) NOT NULL,
  `thn_buat` varchar(20) NOT NULL,
  `thn_terbit` varchar(20) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `kaliber` varchar(100) NOT NULL,
  `no_mesin` varchar(100) NOT NULL,
  `no_chasis` varchar(100) NOT NULL,
  `daya` varchar(20) NOT NULL,
  `pengarang` varchar(100) NOT NULL,
  `tulisan` varchar(300) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `usia` varchar(20) NOT NULL,
  `karat` varchar(20) NOT NULL,
  `warna_kemas` varchar(30) NOT NULL,
  `batas` varchar(500) NOT NULL,
  `imb` varchar(100) NOT NULL,
  `isi` varchar(500) NOT NULL,
  `bukti` varchar(100) NOT NULL,
  `bendera` varchar(100) NOT NULL,
  `cacat` varchar(500) NOT NULL,
  `lain` varchar(500) NOT NULL,
  `petugas_a` varchar(100) NOT NULL,
  `petugas_b` varchar(100) NOT NULL,
  `kepala` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_penelitian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_sebelum_barang
#

DROP TABLE IF EXISTS t_sebelum_barang;

CREATE TABLE `t_sebelum_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_keluar_sebelum` varchar(100) NOT NULL,
  `kode_barang_keluar` int(6) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_setelah_barang
#

DROP TABLE IF EXISTS t_setelah_barang;

CREATE TABLE `t_setelah_barang` (
  `kode` int(6) NOT NULL AUTO_INCREMENT,
  `no_keluar_setelah` varchar(100) NOT NULL,
  `kode_barang_keluar` int(6) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: t_status
#

DROP TABLE IF EXISTS t_status;

CREATE TABLE `t_status` (
  `kode_status` int(6) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO t_status (`kode_status`, `status`) VALUES (1, 'Bon Pinjam');
INSERT INTO t_status (`kode_status`, `status`) VALUES (2, 'Permintaan Instansi Terkait');


#
# TABLE STRUCTURE FOR: t_status_setelah
#

DROP TABLE IF EXISTS t_status_setelah;

CREATE TABLE `t_status_setelah` (
  `kode_status` int(6) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_status`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (3, 'Dikembalikan');
INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (4, 'Rampas Negara / Lelang');
INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (5, 'Dimusnahkan');
INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (6, 'Dikembalikan Ke Instansi');
INSERT INTO t_status_setelah (`kode_status`, `status`) VALUES (7, 'Perkara Lain');


#
# TABLE STRUCTURE FOR: t_user
#

DROP TABLE IF EXISTS t_user;

CREATE TABLE `t_user` (
  `id_user` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL,
  `kode_grup` int(6) NOT NULL,
  `kode_instansi` int(6) NOT NULL,
  `stts` int(2) NOT NULL,
  `foto` varchar(200) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `kode_instansi` (`kode_instansi`),
  KEY `kode_grup` (`kode_grup`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (2, 'admin', 'bc916cec2c4da86bf2d7a48ce4cbca28', 'Irfan Martha', 'administrator', 1, 1, 1, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (3, 'dindin', '788ad7091c4e500855fcf4e3cb1bda29', 'Dindin Zaenudin', 'operator', 1, 1, 1, '999188912-20151214-153205.jpg');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (4, 'yani', '080840925a7e2087673145d83918c658', 'Yani Yuliani', 'instansi', 4, 6, 1, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (5, 'ilya', '67e21d45f7b1fab0a3c43dd4339ee8e9', 'Ilyasviel Von Einzbern', 'instansi', 2, 2, 1, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (6, 'master', 'eb0a191797624dd3a48fa681d3061212', 'Master Fang', 'instansi', 3, 4, 0, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (8, 'titis', '5fe250f6fc5fb39ed7258111e6b86791', 'Laksita Titis E', 'operator', 1, 1, 1, '');
INSERT INTO t_user (`id_user`, `username`, `password`, `nama_lengkap`, `level`, `kode_grup`, `kode_instansi`, `stts`, `foto`) VALUES (9, 'ARI', 'fc292bd7df071858c2d0f955545673c1', 'Ari Prakoso', 'instansi', 2, 2, 0, '');


